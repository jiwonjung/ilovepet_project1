package com.jiwon.ilovepet;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

public class MariaDBConnection {
	
	private static final String DRIVER =
			"org.mariadb.jdbc.Driver";
	private static final String URL =
			//"jdbc:mariadb://localhost:3306/ilovepet";  /*기존 localhost에서의 URL */
			"jdbc:mariadb://mydbinstance.cvpqlscqau4c.ap-northeast-2.rds.amazonaws.com:3306/myrds";
	//private static final String USER ="springUser";
	private static final String USER ="myRdsDB";
	
	//private static final String PWD = "0000";
	private static final String PWD = "wldnjs123!";
	
	@Test
	public void testConn() throws Exception{
		Class.forName(DRIVER);
		
		try(Connection conn = DriverManager.getConnection(URL, USER, PWD)){
			System.out.println(conn);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
