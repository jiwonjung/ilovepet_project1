package com.jiwon.ilovepet.daoTest;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiwon.ilovepet.adoption.dao.AdoptionDAO;
import com.jiwon.ilovepet.adoption.vo.AdoptionVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})
public class AdoptionDAOtest {
	
	@Inject
	private AdoptionDAO dao;
	
	private static Logger logger = LoggerFactory.getLogger(AdoptionDAOtest.class);
	@Test
	public void writeTest() throws Exception {
		for(int i=15; i <= 19; i++) {
			AdoptionVO avo = new AdoptionVO();
			
			avo.setUserId("test1");
			avo.setTitle("햄스터 무료분양 글!" + i);
			avo.setState("분양중");
			avo.setAdoptorName("김성수" + i);
			avo.setAdoptorPhone("010-3543-999" +i);
			avo.setAdoptorEmail(i + "test1@naver.com");
			avo.setPetLocation("경기도" + i);
			avo.setPetCategory("햄스터");
			avo.setPetAmount("1만원");
			avo.setPetSex("여아");
			avo.setPetContent("햄스터 1만원에 분양합니다. 연락주세요!" + i);
			
			dao.writeAdoption(avo);
		}
	}
	/*

	@Test
	public void readTest() throws Exception {
		logger.info(dao.readAdoption(1).toString());
	}
	@Test
	public void updateTest() throws Exception {
		AdoptionVO avo = new AdoptionVO();
		avo.setAdoptionNo(295);
		avo.setTitle("고슴도치 분양글!!");
		avo.setAdoptorName("홍길동수정");
		avo.setState("분양완료");
		avo.setAdoptorPhone("010-7788-9991");
		avo.setAdoptorEmail("hong9@naver.com");
		avo.setPetLocation("경기도 시흥9");
		avo.setPetCategory("고슴도치");
		avo.setPetAmount("무료");
		avo.setPetSex("여아");
		avo.setPetContent("일주일 전 박스 안에서 발견했어요. 예방접종 맞췄구요. 데려가실 분!(수정)");
		
		dao.updateAdoption(avo);
	}
	 */
	/*
	
	
	@Test
	public void deleteTest() throws Exception {
		dao.deleteAdoption(2);
	}
	
	@Test
	public void listTest() throws Exception {
		logger.info(dao.listAdoption().toString());
	}
	
	// 리스트 페이징 + 검색
	@Test
	public void listPagingTest() throws Exception {
		
	}
	 */
}
