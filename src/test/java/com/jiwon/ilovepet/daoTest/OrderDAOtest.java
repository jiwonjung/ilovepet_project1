package com.jiwon.ilovepet.daoTest;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiwon.ilovepet.petmall.dao.OrderDAO;
import com.jiwon.ilovepet.petmall.vo.OrderDetailVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})

public class OrderDAOtest {
	
	@Inject
	private OrderDAO dao;
	
	private static Logger logger = LoggerFactory.getLogger(OrderDAOtest.class);
	/*
	@Test
	public void orderInfoTest() throws Exception {
		OrderVO vo = new OrderVO();
		vo.setOrderNo("20191216012345");
		vo.setUserId("test1");
		vo.setOrderReceiver("나에게");
		vo.setUserAddcode("01-123");
		vo.setUserAddress1("서울호고루");
		vo.setUserAddress2("개봉동");
		vo.setOrderPhone("010-1111-9999");
		vo.setOrderAmount(90000);
		
		dao.orderInfo(vo);
	}
	*/
	
	@Test
	public void orderDetailsTest() throws Exception {
		OrderDetailVO vo = new OrderDetailVO();
		vo.setOrderNo("20191218_529350");
		vo.setProductNo(113);
		vo.setAmount(9);
		
		dao.orderInfo_Details(vo);
	}
}
