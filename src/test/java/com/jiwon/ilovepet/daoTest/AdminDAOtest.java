package com.jiwon.ilovepet.daoTest;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiwon.ilovepet.admin.dao.AdminDAO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.petmall.vo.OrderListVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;
import com.jiwon.ilovepet.user.vo.UserVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})

public class AdminDAOtest {
	@Inject
	private AdminDAO dao;
	
	private static Logger logger = LoggerFactory.getLogger(AdminDAOtest.class);
	/*
	@Test
	public void listTest() throws Exception {
		logger.info(dao.usersListAdmin().toString());
	}
	
	@Test
	public void listPagingTest() throws Exception {
		PagingCriteria pCriteria = new PagingCriteria();
		pCriteria.setPage(0);
		pCriteria.setNumPerPage(15);
		
		List<UserVO> list = dao.listUsersPaging(pCriteria);
		
		for (UserVO userVO : list) {
			logger.info(userVO.getUserId() + ": " + userVO.getUserName());
		}
	}
	 */

}
