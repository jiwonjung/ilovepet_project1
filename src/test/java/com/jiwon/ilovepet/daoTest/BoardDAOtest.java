package com.jiwon.ilovepet.daoTest;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.jiwon.ilovepet.board.dao.BoardDAO;
import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})
public class BoardDAOtest {
	
	@Inject
	private BoardDAO dao;
	
	private static Logger logger = LoggerFactory.getLogger(BoardDAOtest.class);
	@Test
	public void writeTest() throws Exception {
		for(int i=1; i <= 100; i++) {
			BoardVO bvo = new BoardVO();
			bvo.setBoardTitle("안녕하세요!(" + i + ")");
			bvo.setBoardContent("테스트33 내용용.");
			bvo.setBoardCategory("Q&A");
			bvo.setUserId("test1");
			
			dao.writeBoard(bvo);
			
		}
	}
	/*	
	@Test
	public void readBoardTest() throws Exception {
		
		logger.info(dao.readBoard(1).toString());
	}
	@Test
	public void updateBoardTest() throws Exception {
		BoardVO bvo = new BoardVO();
		bvo.setBoardNo(3);
		bvo.setBoardTitle("안녕하세요!!수정");
		bvo.setBoardContent("안녕하세요 수정내용입니다.");
		
		dao.updateBoard(bvo);
	}
	@Test
	public void deleteBoardTest() throws Exception {
		
		dao.deleteBoard(4);
	}

	@Test
	public void listTest() throws Exception {
		
		logger.info(dao.list().toString());
	}
	
	@Test
	public void listPagingTest() throws Exception {
		int page = 5;
		List<BoardVO> list = dao.listPaging(page);
		
		for (BoardVO boardVO : list) { 
			logger.info(boardVO.getBoardNo()+ ": " + boardVO.getBoardTitle()); 
		}
	}

//public List<BoardVO> listPagingCriteria(PagingCriteria pCriteria) throws Exception;
	@Test
	public void listPagingCriteriaTest() throws Exception {
		PagingCriteria pCriteria = new PagingCriteria();
		pCriteria.setPage(0);
		pCriteria.setNumPerPage(15);
		
		List<BoardVO> list = dao.listPagingCriteria(pCriteria);
		
		for (BoardVO boardVO : list) {
			logger.info(boardVO.getBoardNo() + ": " + boardVO.getBoardTitle());
		}
	}
	 */
	
	/*
	// UriComponentsBuilder를 이용한 페이징 처리 기법
	@Test
	public void uriTest() throws Exception {
		UriComponents uriComponents = 
				UriComponentsBuilder.newInstance()
				.path("/bbs/read")
				.queryParam("boardNo", 100)
				.queryParam("numPerPage", 20)
				.build();
		
		logger.info("/bbs/read?boardNo=100&numPerPage=20");
		logger.info(uriComponents.toString());
	}

	@Test
	public void uriTest2() throws Exception {
		UriComponents uriComponents = 
				UriComponentsBuilder.newInstance()
				.path("/{modeule}/{page}")
				.queryParam("boardNo", 100)
				.queryParam("numPerPage", 20)
				.build()
				.expand("bbs", "read")
				.encode();
		
		logger.info("/bbs/read?boardNo=100&numPerPage=20");
		logger.info(uriComponents.toString());
	}
	
	// 검색기능 테스트
	@Test
	public void searchTest() throws Exception {
		SearchingCriteria sCriteria = new SearchingCriteria();
		sCriteria.setPage(1);
		sCriteria.setKeyword("안녕하세요!!");
		sCriteria.setSearchingCategory("T");
		
		logger.info("===============");
		
		List<BoardVO> board = dao.listSearchingCriteria(sCriteria);
		
		for (BoardVO boardVO : board) {
			logger.info(boardVO.getBoardNo() + " : " + boardVO.getBoardTitle());
		}
		
		logger.info("===============");
		logger.info("검색된 게시판 글 갯수: " + dao.countSearchingData(sCriteria));
	}
	 */
	
	/*
	 * @Test public void listByCategoryTest() throws Exception { SearchingCriteria
	 * sCriteria = new SearchingCriteria(); sCriteria.setPage(1);
	 * //sCriteria.setKeyword("이 고양이 종이 뭔가요?"); sCriteria.setKeyword("안녕하세요!!");
	 * sCriteria.setSearchingCategory("T");
	 * 
	 * logger.info("================");
	 * 
	 * //List<BoardVO> board = dao.listByQnA(sCriteria); List<BoardVO> board =
	 * dao.listByFree(sCriteria);
	 * 
	 * for(BoardVO boardVO : board) { logger.info(boardVO.getBoardNo() + " : " +
	 * boardVO.getBoardTitle()); }
	 * 
	 * logger.info("================"); //logger.info("검색된 게시판 글 갯수: " +
	 * dao.countQnAData(sCriteria)); logger.info("검색된 게시판 글 갯수: " +
	 * dao.countFreeData(sCriteria)); }
	 */
}
