package com.jiwon.ilovepet.daoTest;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiwon.ilovepet.petmall.dao.CartDAO;
import com.jiwon.ilovepet.petmall.vo.CartListVO;
import com.jiwon.ilovepet.petmall.vo.CartVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})
public class CartDAOtest {
	@Inject
	private CartDAO dao;
	
	private static Logger logger = LoggerFactory.getLogger(CartDAOtest.class);
	
	@Test
	public void listTest() throws Exception {
		logger.info(dao.listCart("test1").toString());
	}
	
	@Test
	public void insertTest() throws Exception {
		CartListVO vo = new CartListVO();
		vo.setUserId("test66");
		vo.setProductNo(94);
		vo.setAmount(6);
		
		dao.insertCart(vo);
	}
	/*
	 */
}
