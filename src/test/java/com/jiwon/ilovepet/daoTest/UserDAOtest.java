package com.jiwon.ilovepet.daoTest;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiwon.ilovepet.board.dao.BoardDAO;
import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.user.dao.UserDAO;
import com.jiwon.ilovepet.user.vo.UserVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})
public class UserDAOtest {
	
	@Inject
	private UserDAO dao;
	
	@Inject
	private BoardDAO bdao;
	
	private static Logger logger = LoggerFactory.getLogger(UserDAOtest.class);
	
	@Test
	public void testTime() throws Exception {
		System.out.println(dao.getTime());  // getTime()과 insertMember()는 MemberDAOImpl 메소드와 동일하게
	}

	/*
	@Test
	public void insertUserTest() throws Exception {
		UserVO uvo = new UserVO();
		uvo.setUserId("test66");
		uvo.setUserPw("1234");
		uvo.setUserName("김은지");
		uvo.setUserEmail("eunji@naver.com");
		uvo.setUserPhone("010-6876-5125");
		uvo.setUserAddCode("01234");
		uvo.setUserAddress1("서울 구로구");
		uvo.setUserAddress2("개봉동 현대");
		
		dao.insertUser(uvo);
	}
	@Test
	public void readUserTest() throws Exception {
		logger.info(dao.readUser("seg615").toString());
	}	
	@Test
	public void updateUserTest() throws Exception {
		UserVO uvo = new UserVO();
		uvo.setUserId("test5");
		uvo.setUserName("김은학");
		uvo.setUserEmail("ttt555@naver.com");
		uvo.setUserPhone("01055551111");
		uvo.setUserAddCode("0123");
		uvo.setUserAddress1("서울구로구");
		uvo.setUserAddress2("개봉덕");
		
		dao.updateUser(uvo);
	}
	
	 */
	
	/*
	
	@Test
	public void userDeleteTest() throws Exception {
		dao.deleteUser("test5");
	}
	
	@Test
	public void userListTest() throws Exception {
		logger.info(dao.listUsers().toString());
	
	}
	
	@Test
	public void listPagingCriteriaTest() throws Exception {
		PagingCriteria pCriteria = new PagingCriteria();
		pCriteria.setPage(0);
		pCriteria.setNumPerPage(15);
		
		UserVO uvo = new UserVO();
		uvo.setUserId("test1");
		
		List<BoardVO> list = dao.listBbsById(pCriteria, uvo.getUserId());
		
		for (BoardVO boardVO : list) {
			logger.info(boardVO.getBoardNo() + ": " + boardVO.getBoardTitle());
		}
	}
	 */
	
}
