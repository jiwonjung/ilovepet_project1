package com.jiwon.ilovepet.daoTest;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.jiwon.ilovepet.boardReply.dao.BoardReplyDAO;
import com.jiwon.ilovepet.boardReply.vo.BoardReplyVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})
public class BoardReplyDAOtest {
	
	@Inject
	private BoardReplyDAO dao;
	
	private static Logger logger = LoggerFactory.getLogger(BoardReplyDAOtest.class);
	/*
	@Test
	public void writeTest() throws Exception {
		//for (int i = 1; i <= 100; i++) {
			BoardReplyVO rvo = new BoardReplyVO();
			rvo.setBoardNo(1020);
			rvo.setReBoardContent("댓글 남깁니다!!");
			rvo.setUserId("test3");
			
			dao.writeReply(rvo);
		//}
	}
	
	 */
	/*
	@Test 
	public void updateTest() throws Exception {
		BoardReplyVO rvo = new BoardReplyVO();
		rvo.setReBoardNo(6);
		rvo.setReBoardContent("다시 댓글 수정수정");
		
		dao.updateReply(rvo);
	}
	@Test 
	public void deleteTest() throws Exception {
		dao.deleteReply(99);
	}
	@Test 
	public void listTest() throws Exception {
		logger.info(dao.replyList(2).toString());
	}
	
	//public List<BoardReplyVO> replyListPaging(Integer boardNo, PagingCriteria pCriteria) throws Exception;
	 */
	@Test
	public void listPagingTest() throws Exception {
		int boardNo = 2;
		
		PagingCriteria pCriteria = new PagingCriteria();
		pCriteria.setPage(0);
		pCriteria.setNumPerPage(10);
		
		List<BoardReplyVO> list = dao.replyListPaging(boardNo, pCriteria);
	
		for (BoardReplyVO boardReplyVO : list) {
			logger.info(boardReplyVO.getBoardNo() + ": " + boardReplyVO.getReBoardContent());
		}
	}
	
}
