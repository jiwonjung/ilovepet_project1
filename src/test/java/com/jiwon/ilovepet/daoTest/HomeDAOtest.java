package com.jiwon.ilovepet.daoTest;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiwon.ilovepet.admin.dao.HomeDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})

public class HomeDAOtest {
	@Inject
	private HomeDAO dao;
	
	private static Logger logger = LoggerFactory.getLogger(HomeDAOtest.class);
	
	/*
	 * @Test public void listAdoptionTest() throws Exception {
	 * logger.info(dao.getMainAdoption().toString()); }
	 */

	@Test
	public void listProductTest() throws Exception {
		logger.info(dao.getMainProduct().toString());
	}
}
