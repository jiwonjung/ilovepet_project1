package com.jiwon.ilovepet.daoTest;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiwon.ilovepet.boardReply.vo.BoardReplyVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.petmall.dao.ProductReviewDAO;
import com.jiwon.ilovepet.petmall.vo.ProductReviewVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})
public class ProductReviewDAOtest {
	@Inject
	private ProductReviewDAO dao;
	
	private static Logger logger = LoggerFactory.getLogger(ProductReviewDAOtest.class);
	/*
	@Test
	public void writeTest() throws Exception {
		for (int i = 31; i <= 50; i++) {
			ProductReviewVO vo = new ProductReviewVO();
			vo.setProductNo(100);
			vo.setReviewContent("배송빨라요!!굿굿 + " + i);
			vo.setUserId("seg615");
			vo.setReviewScore(4);
			
			dao.writeProductReview(vo);
		}
	}
	
	@Test
	public void updateTest() throws Exception {
		ProductReviewVO vo = new ProductReviewVO();
		vo.setReviewNo(1);
		vo.setReviewContent("수정합니다!!");
		vo.setReviewScore(3);
		
		dao.updateProductReview(vo);
	}
	
	@Test
	public void deleteTest() throws Exception {
		dao.deleteProductReview(4);
	}
	@Test
	public void listTest() throws Exception {
		logger.info(dao.listProductReview(11).toString());
	}
	 */
	@Test
	public void listPagingTest() throws Exception {
		int productNo = 100;
		
		PagingCriteria pCriteria = new PagingCriteria();
		pCriteria.setPage(0);
		pCriteria.setNumPerPage(10);
		
		List<ProductReviewVO> list = dao.listPagingProductReview(productNo, pCriteria);
				
	
		for (ProductReviewVO vo : list) {
			logger.info(vo.getProductNo() + ": " + vo.getReviewContent());
		}
	}
	/*	
 */
	
	
}
