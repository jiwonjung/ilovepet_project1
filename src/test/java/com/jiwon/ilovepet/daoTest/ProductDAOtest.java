package com.jiwon.ilovepet.daoTest;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.petmall.dao.ProductDAO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={"file:src/main/webapp/WEB-INF/spring/**/*.xml"})
public class ProductDAOtest {
	
	@Inject
	private ProductDAO dao;
	
	private static Logger logger = LoggerFactory.getLogger(ProductDAOtest.class);
	
	/*
	@Test
	public void insertTest() throws Exception {
		
			
			ProductVO pvo = new ProductVO();
			pvo.setProductName("강아지 간식");
			pvo.setCateCode("101");
			pvo.setProductPrice(20000);
			pvo.setProductStock(100);
			pvo.setProductDetails("맛있어요!! 베스트 셀러!");
			pvo.setProductUrl("food.jpg");
			
			dao.insertProduct(pvo);
	}
	@Test
	public void readTest() throws Exception {
		logger.info(dao.readProductDetails(1).toString());
	}
	*/
	
	@Test
	public void updateTest() throws Exception {
		ProductVO pvo = new ProductVO();
		pvo.setProductName("햄스터 사료");
		pvo.setCateCode("301");
		pvo.setProductNo(106);
		pvo.setProductPrice(10000);
		pvo.setProductStock(88);
		pvo.setProductDetails("프리미엄 사료입니다.");
		pvo.setProductUrl("clothing.jpg");
		
		dao.updateProduct(pvo);
	}
	/*
	@Test
	public void deleteTest() throws Exception {
		dao.deleteProduct(2);
	}
	
	@Test
	public void listTest() throws Exception {
		logger.info(dao.listProduct().toString());
	}
	
	@Test
	public void listPagingTest() throws Exception {
		SearchingCriteria sCriteria = new SearchingCriteria();
		sCriteria.setPage(1);
		sCriteria.setKeyword("강아지사료");
		sCriteria.setSearchingCategory("N");
		
		logger.info("================");
		
		List<ProductVO> product = dao.listProductCriteria(sCriteria);
		
		for(ProductVO productVO : product) {
			logger.info(productVO.getProductNo() + " : " + productVO.getProductName());
		}
		
		logger.info("================");
		//logger.info("검색된 게시판 글 갯수: " + dao.countQnAData(sCriteria));
		logger.info("검색된 게시판 글 갯수: " + dao.countProductData(sCriteria));
	}
	
	@Test
	public void productViewTest() throws Exception {
		logger.info(dao.productView(109).toString());
	}
	 */	
}
