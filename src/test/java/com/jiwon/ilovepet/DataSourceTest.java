package com.jiwon.ilovepet;

import java.sql.Connection;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/**/*.xml") // servlet-context.xml, root-context.xml 두 개의 위치를 지정해야 불러올 수 있음
public class DataSourceTest {
	
	@Inject
	private DataSource dataSource;
	
	@Test
	public void testConnection() throws Exception{
		try(Connection conn = dataSource.getConnection()){
			System.out.println("conn 출력:  " + conn);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
