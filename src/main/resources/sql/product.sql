/* 상품 */

CREATE TABLE `product` (
	`product_no` INT(11) NOT NULL AUTO_INCREMENT,
	`product_name` VARCHAR(60) NULL DEFAULT NULL,
	`cateCode` VARCHAR(30) NOT NULL,
	`product_price` INT(11) NULL DEFAULT '0',
	`product_stock` INT(11) NULL DEFAULT NULL,
	`product_details` VARCHAR(500) NULL DEFAULT NULL,
	`product_url` VARCHAR(500) NULL DEFAULT NULL,
	`product_thumbImg` VARCHAR(200) NULL DEFAULT NULL,
	`review_cnt` INT(11) NULL DEFAULT '0',
	`regdate` TIMESTAMP NOT NULL DEFAULT '',
	`moddate` TIMESTAMP NOT NULL DEFAULT '',
	PRIMARY KEY (`product_no`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=151
;

insert into product (product_name, product_price, product_details, product_url) 
values('강아지샴푸', 13000, '아이러브펫 매장의 스테디 셀러!', 'shampoo.jpg');

alter table product add product_category varchar(50) null after product_no;
alter table product add product_stock int not null after product_category;