/* 분양페이지 */
CREATE TABLE `adoption` (
	`adoption_no` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` VARCHAR(50) NOT NULL,
	`title` VARCHAR(200) NOT NULL,
	`state` VARCHAR(10) NULL DEFAULT '분양중',
	`adoptor_name` VARCHAR(50) NOT NULL,
	`adoptor_phone` VARCHAR(50) NOT NULL,
	`adoptor_email` VARCHAR(100) NOT NULL,
	`pet_location` VARCHAR(100) NOT NULL,
	`pet_category` VARCHAR(50) NOT NULL,
	`pet_amount` VARCHAR(50) NULL DEFAULT '무료',
	`pet_sex` VARCHAR(10) NULL DEFAULT '모름',
	`pet_content` TEXT NULL DEFAULT NULL,
	`pet_img` VARCHAR(500) NULL DEFAULT NULL,
	`pet_thumbImg` VARCHAR(200) NULL DEFAULT NULL,
	`regdate` TIMESTAMP NOT NULL DEFAULT '',
	`moddate` TIMESTAMP NOT NULL DEFAULT '',
	PRIMARY KEY (`adoption_no`),
	INDEX `fk_user_id2` (`user_id`),
	CONSTRAINT `fk_user_id2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=36
;


alter table adoption add constraint fk_user_id2
foreign key(user_id) references users(user_id);
