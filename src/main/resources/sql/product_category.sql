/* category table */

CREATE TABLE `product_category` (
	`cateName` VARCHAR(20) NOT NULL,
	`cateCode` VARCHAR(30) NOT NULL,
	`cateCodeRef` VARCHAR(30) NULL DEFAULT NULL,
	PRIMARY KEY (`cateCode`),
	INDEX `cate_code_ref` (`cateCodeRef`),
	CONSTRAINT `product_category_ibfk_1` FOREIGN KEY (`cateCodeRef`) REFERENCES `product_category` (`cateCode`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

insert into product_category (cateName, cateCode) values('강아지', '100');
insert into product_category (cateName, cateCode, cateCodeRef) values('강아지사료', '101', '100');
insert into product_category (cateName, cateCode, cateCodeRef) values('간식', '102', '100');
insert into product_category (cateName, cateCode, cateCodeRef) values('강아지 옷', '103', '100');

insert into product_category (cateName, cateCode) values('고양이', '200');
insert into product_category (cateName, cateCode, cateCodeRef) values('고양이사료', '201', '200');
insert into product_category (cateName, cateCode, cateCodeRef) values('모래/탈취제', '202', '200');
insert into product_category (cateName, cateCode, cateCodeRef) values('스크래쳐/캣타워', '203', '200');

insert into product_category (cateName, cateCode) values('고슴도치', '300');
insert into product_category (cateName, cateCode, cateCodeRef) values('고슴도치사료', '301', '300');
insert into product_category (cateName, cateCode, cateCodeRef) values('은신처', '302', '300');
insert into product_category (cateName, cateCode, cateCodeRef) values('위생/청결/화장실', '303', '300');

insert into product_category (cateName, cateCode) values('햄스터', '400');
insert into product_category (cateName, cateCode, cateCodeRef) values('햄스터시료', '401', '400');
insert into product_category (cateName, cateCode, cateCodeRef) values('장난감/쳇바퀴', '402', '400');
insert into product_category (cateName, cateCode, cateCodeRef) values('배딩/모레', '403', '400');


SELECT 1 AS LEVEL, cateName, cateCode, cateCodeRef FROM product_category WHERE cateCodeRef is NULL
UNION ALL
SELECT 2 AS LEVEL, cateCode, cateCode, cateCodeRef FROM product_category WHERE cateCodeRef IS NOT null
ORDER BY cateCode;