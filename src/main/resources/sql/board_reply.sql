/*개시판 댓글*/

CREATE TABLE `board_reply` (
	`reboard_no` INT(11) NOT NULL AUTO_INCREMENT,
	`board_no` INT(11) NOT NULL DEFAULT '0',
	`user_id` VARCHAR(50) NOT NULL,
	`reboard_content` TEXT NULL DEFAULT NULL,
	`regdate` TIMESTAMP NOT NULL DEFAULT '',
	`moddate` TIMESTAMP NOT NULL DEFAULT '',
	PRIMARY KEY (`reboard_no`),
	INDEX `fk_board_no` (`board_no`),
	INDEX `fk_user_id` (`user_id`),
	CONSTRAINT `fk_board_no` FOREIGN KEY (`board_no`) REFERENCES `board` (`board_no`),
	CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=138
;


alter table board_reply add constraint fk_board_no
foreign key(board_no) references board(board_no);

alter table board_reply add constraint fk_board_no
foreign key(board_no) references board(board_no);

alter table board_reply add constraint fk_user_id
foreign key(user_id) references users(user_id);
