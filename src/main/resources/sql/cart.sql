/* 장바구니 */

CREATE TABLE `cart` (
	`cart_no` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` VARCHAR(50) NOT NULL,
	`product_no` INT(11) NOT NULL,
	`amount` INT(11) NULL DEFAULT '0',
	`add_date` TIMESTAMP NOT NULL DEFAULT '',
	PRIMARY KEY (`cart_no`, `user_id`),
	INDEX `fk_cart_userid` (`user_id`),
	INDEX `fk_cart_productno` (`product_no`),
	CONSTRAINT `fk_cart_productno` FOREIGN KEY (`product_no`) REFERENCES `product` (`product_no`),
	CONSTRAINT `fk_cart_userid` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=131
;




alter table cart add constraint fk_cart_userid
foreign key (user_id) references users (user_id);

alter table cart add constraint fk_cart_productno
foreign key (product_no) references product (product_no);

insert into cart(user_id, product_no, amount) 
values('seg615', 100, 99);