/* 주문 테이블 */

CREATE TABLE `tbl_order` (
	`order_no` VARCHAR(50) NOT NULL,
	`user_id` VARCHAR(50) NOT NULL,
	`order_receiver` VARCHAR(50) NOT NULL,
	`user_addcode` VARCHAR(100) NOT NULL,
	`user_address1` VARCHAR(100) NOT NULL,
	`user_address2` VARCHAR(100) NOT NULL,
	`order_phone` VARCHAR(30) NOT NULL,
	`order_amount` INT(11) NOT NULL,
	`orderdate` TIMESTAMP NOT NULL DEFAULT '',
	`delivery` VARCHAR(20) NULL DEFAULT '배송준비',
	PRIMARY KEY (`order_no`),
	INDEX `tbl_order_userId` (`user_id`),
	CONSTRAINT `tbl_order_userId` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

alter table tbl_order add constraint tbl_order_userId foreign key(user_id)
references users(user_id);
