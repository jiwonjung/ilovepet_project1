CREATE TABLE `product_review` (
	`review_no` INT(11) NOT NULL AUTO_INCREMENT,
	`product_no` INT(11) NOT NULL,
	`user_id` VARCHAR(50) NOT NULL,
	`review_content` VARCHAR(1000) NOT NULL,
	`review_score` INT(11) NULL DEFAULT '0',
	`regdate` TIMESTAMP NOT NULL DEFAULT '',
	`moddate` TIMESTAMP NOT NULL DEFAULT '',
	PRIMARY KEY (`review_no`, `product_no`),
	INDEX `fk_review_userid` (`user_id`),
	INDEX `fk_review_productno` (`product_no`),
	CONSTRAINT `fk_review_productno` FOREIGN KEY (`product_no`) REFERENCES `product` (`product_no`),
	CONSTRAINT `fk_review_userid` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=100
;


alter table product_review add constraint fk_review_userid
foreign key (user_id) references users (user_id);

alter table product_review add constraint fk_review_productno
foreign key (product_no) references product (product_no);