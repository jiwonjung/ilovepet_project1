/*자유게시판*/

CREATE TABLE `board` (
	`board_no` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` VARCHAR(50) NOT NULL,
	`board_title` VARCHAR(200) NOT NULL,
	`board_content` TEXT NULL DEFAULT NULL,
	`board_category` VARCHAR(30) NOT NULL,
	`hit` INT(11) NULL DEFAULT '0',
	`replycnt` INT(11) NULL DEFAULT '0',
	`filecnt` INT(11) NULL DEFAULT '0',
	`board_img` VARCHAR(500) NULL DEFAULT NULL,
	`board_thumbImg` VARCHAR(200) NULL DEFAULT NULL,
	`regdate` TIMESTAMP NOT NULL DEFAULT '',
	`moddate` TIMESTAMP NOT NULL DEFAULT '',
	PRIMARY KEY (`board_no`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1037
;
