/* 주문 상세 테이블 */

CREATE TABLE `tbl_order_details` (
	`orderDetails_no` INT(11) NOT NULL AUTO_INCREMENT,
	`order_no` VARCHAR(50) NOT NULL,
	`product_no` INT(11) NOT NULL,
	`amount` INT(11) NOT NULL,
	PRIMARY KEY (`orderDetails_no`),
	INDEX `orderDetails_orderNo` (`order_no`),
	INDEX `orderDetails_productNo` (`product_no`),
	CONSTRAINT `orderDetails_orderNo` FOREIGN KEY (`order_no`) REFERENCES `tbl_order` (`order_no`),
	CONSTRAINT `orderDetails_productNo` FOREIGN KEY (`product_no`) REFERENCES `product` (`product_no`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=246
;
