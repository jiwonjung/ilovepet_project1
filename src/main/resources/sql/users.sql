/* 회원 */

CREATE TABLE `users` (
	`user_id` VARCHAR(50) NOT NULL,
	`user_pw` VARCHAR(100) NOT NULL,
	`user_name` VARCHAR(50) NOT NULL,
	`user_email` VARCHAR(100) NOT NULL,
	`user_phone` VARCHAR(13) NOT NULL,
	`isadmin` TINYINT(4) NULL DEFAULT '0',
	`user_addcode` VARCHAR(50) NULL DEFAULT NULL,
	`user_address1` VARCHAR(100) NULL DEFAULT NULL,
	`user_address2` VARCHAR(50) NULL DEFAULT NULL,
	`session_key` VARCHAR(50) NOT NULL DEFAULT 'none',
	`session_limit` TIMESTAMP NOT NULL DEFAULT '',
	`regdate` TIMESTAMP NOT NULL DEFAULT '',
	`moddate` TIMESTAMP NOT NULL DEFAULT '',
	PRIMARY KEY (`user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
