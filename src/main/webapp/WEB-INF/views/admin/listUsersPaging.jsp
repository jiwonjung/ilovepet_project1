<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/left_sidebar.jsp"%>
<section id="main-content">
<!--main content start-->
	<section class="wrapper" style="margin-top: 0;">
		<div class="row mt">
			<div class="col-lg-12">
				<div class="content-panel"  style="padding-top: 0;">
					<br/>
					<h3 style="text-align:center;"><b>전체 회원 목록 </b></h3>
						<br/><br/>
					<section id="unseen">
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>아이디</th>
									<th>이름</th>
									<th>이메일</th>
									<th>연락처</th>
									<th>주소</th>
									<th class="numeric">가입날짜</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${list }" var="user">  
								<tr>
									<td>${user.userId }</td>
									<td>${user.userName }</td>  <!-- "/bbs/read?bid"는 controller 맵핑경로로 bid값을 함꼐 넘김. ex. 글 선택하면 url은 http://localhost:8080/bbs/read?bid=2  -->
									<td>${user.userEmail }</td>
									<td>${user.userPhone }</td>
									<td>[${user.userAddCode}]${user.userAddress1 } ${user.userAddress2 }</td>
									<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${user.regdate }"/></td> <!-- D: 365일로 표현, d: 월에 해당하는 일로 표현, H: 24시간기준, h: 12시간기준 -->
								</tr>
							</c:forEach>
							</tbody>
						</table>
					</section>

				</div>
				<!-- /content-panel -->
				
						<div class="showback" align="center">
						<div class="btn-group">
						  <c:if test="${pagingMaker.prev }">
						  	<%-- <a href="pagingList?page=${pagingMaker.startPage-1}" > --%>
						  	<a href="listUsersPaging${pagingMake.makeUriComponents(pagingMaker.startPage-1)}" >
						  	<button type="button" class="btn btn-default"><i class="fa fa-angle-double-left" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  <c:forEach begin="${pagingMaker.startPage }" end="${pagingMaker.endPage }" var="pageNum">
						  <a href="listUsersPaging${pagingMaker.makeUriComponents(pageNum)}">
						 	 <button type="button" class="<c:out value="${pagingMaker.cri.page == pageNum?'btn btn-primary':'btn btn-default'}"/>">${pageNum}</button> <!--${pagingMaker.cri.page}은 pagingMaker.getCri()과 PageCriteria의 getPage()와 같음  -->
						  </a>
						  </c:forEach>
						  <c:if test="${pagingMaker.next && pagingMaker.endPage > 0 }">
						  	<a href ="listUsersPaging${pagingMaker.makeUriComponents(pagingMaker.endPage+1) }">
						  		<button type="button" class="btn btn-default"><i class="fa fa-angle-double-right" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  
						</div>      					
      				</div><!-- /showback -->
			</div>
			<!-- /col-lg-4 -->
		</div>
		<!-- /row -->
	</section>
	<!-- /wrapper -->

</section>
<%@ include file="../include/footer.jsp"%>
</body></html>