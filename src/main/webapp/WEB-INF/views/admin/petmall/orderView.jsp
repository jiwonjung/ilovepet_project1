<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/left_sidebar.jsp"%>
<style>
 .orderInfo { border:5px solid #eee; padding:10px 20px; margin:20px 0;}
 .orderInfo span { font-size:20px; font-weight:bold; display:inline-block; width:90px; }
 
 .orderView li { margin-bottom:20px; padding-bottom:20px; border-bottom:1px solid #999; }
 .orderView li::after { content:""; display:block; clear:both; }
 
 .thumb { float:left; width:200px; }
 .thumb img { width:200px; height:200px; }
 .gdsInfo { float:right; width:calc(100% - 220px); line-height:2; }
 .gdsInfo span { font-size:20px; font-weight:bold; display:inline-block; width:100px; margin-right:10px; }
</style>
<body>
<section id="main-content">
<section id="content">
	<div class="orderInfo">
					<c:forEach items="${orderView}" var="orderView" varStatus="status">
						
						<c:if test="${status.first}">
							<p><span>주문자</span>${orderView.userId}</p>
							<p><span>수령인</span>${orderView.orderReceiver}</p>
							<p><span>주소</span>(${orderView.userAddcode}) ${orderView.userAddress1} ${orderView.userAddress2}</p>
							<p><span>가격</span><fmt:formatNumber pattern="###,###,###" value="${orderView.orderAmount}" /> 원</p>
							<p><span>상태</span>${orderView.delivery}</p>
							<div class="deliveryChange">
								<form role="form" method="post">
								
								<%-- 	<input type="hidden" name="orderNo" value="${orderView.orderNo}" /> --%>
									<input type="hidden" name="delivery" class="delivery" value="" />
									
									<button type="button" class="delivery1_btn">배송 중</button>
									<button type="button" class="delivery2_btn">배송 완료</button>
									
									<script>
										var formObj = $("form[role='form']");
										
										$(".delivery1_btn").click(function(){
											var result = confirm("해당 주문상태를 '배송 중'으로 변경하시겠습니까?");
											if(result){
												$(".delivery").val("배송 중");
												formObj.submit(); 
											}
										});
										
										$(".delivery2_btn").click(function(){
											var result = confirm("해당 주문상태를 '배송 완료'으로 변경하시겠습니까?");
											if(result){
												$(".delivery").val("배송 완료");
												formObj.submit(); 
											}
										});
										
									
									</script>
								</form>
							</div>
						</c:if>
						
					</c:forEach>
				</div>
				
				<ul class="orderView">
					<c:forEach items="${orderView}" var="orderView">					
					<li>
						<div class="thumb">
							<img src="${orderView.productThumbImg}" />
						</div>
						<div class="gdsInfo">
							<p>
								<span>상품명</span>${orderView.productName}<br />
								<span>개당 가격</span><fmt:formatNumber pattern="###,###,###" value="${orderView.productPrice}" /> 원<br />
								<span>구입 수량</span>${orderView.amount} 개<br />
								<span>최종 가격</span><fmt:formatNumber pattern="###,###,###" value="${orderView.productPrice * orderView.amount}" /> 원                   
							</p>
						</div>
					</li>					
					</c:forEach>
				</ul>

</section>
</section>
<%@ include file="../../include/footer.jsp"%>
</body></html>