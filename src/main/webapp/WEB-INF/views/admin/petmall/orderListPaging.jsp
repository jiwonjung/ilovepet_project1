<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/left_sidebar.jsp"%>
<style>
 div#container_box ul li { border:5px solid #eee; padding:10px 20px; margin-bottom:20px; }
 div#container_box .orderList span { font-size:20px; font-weight:bold; display:inline-block; width:90px; margin-right:10px; }
	
.deliveryChange { text-align:right; }
.delivery1_btn,
.delivery2_btn { font-size:16px; background:#fff; border:1px solid #999; margin-left:10px; }
</style>

<body>
<section id="main-content">
  <section class="wrapper" style="margin-top: 0;">
	<div class="row mt" style="margin-top:0px;">
      <div class="col-lg-12">
       <div class="form-panel">
 			
 					  <h3 class="mb" style="text-align:center;"><b>구매내역</b></h3>
 					   <p>※ 최신순, 배송준비>배송중>배송완료 순서로 제일 상단에 나타납니다. (10개씩)</p>
 		 		
                       <c:forEach items="${list}" var="orderList">
                       <hr/>
                       <div class="content-panel" style="min-height:220px;">
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">주문번호</label>
                              <div class="col-lg-10">
                                  <p><a href="/admin/petmall/orderView?orderNo=${orderList.orderNo}">${orderList.orderNo}</a></p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">주문인</label>
                              <div class="col-lg-10">
                                  <p>${orderList.userId}</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">수령인</label>
                              <div class="col-lg-10">
                                  <p>${orderList.orderReceiver}</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">주소</label>
                              <div class="col-lg-10">
                                  <p>(${orderList.userAddcode}) ${orderList.userAddress1} ${orderList.userAddress2}</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">가격</label>
                              <div class="col-lg-10">
                                  <p><fmt:formatNumber pattern="###,###,###" value="${orderList.orderAmount}" /> 원</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">주문날짜</label>
                              <div class="col-lg-10">
                                  <p><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${orderList.orderdate }"/></p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">상태</label>
                              <div class="col-lg-10">
                                  <p>${orderList.delivery}</a></p>
                              </div>
                          </div>
                       </div>
                       </c:forEach>

					<div class="showback" align="center">
						<div class="btn-group">
						  <c:if test="${pagingMaker.prev }">
						  	<a href="orderListPaging?page=${pagingMaker.startPage-1}" >
						  	<button type="button" class="btn btn-default"><i class="fa fa-angle-double-left" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  <c:forEach begin="${pagingMaker.startPage }" end="${pagingMaker.endPage }" var="pageNum">
						  <a href="orderListPaging${pagingMaker.makeUriComponents(pageNum)}">
						 	 <button type="button" class="<c:out value="${pagingMaker.cri.page == pageNum?'btn btn-primary':'btn btn-default'}"/>">${pageNum}</button> <!--${pagingMaker.cri.page}은 pagingMaker.getCri()과 PageCriteria의 getPage()와 같음  -->
						  </a>
						  </c:forEach>
						  <c:if test="${pagingMaker.next && pagingMaker.endPage > 0 }">
						  	<a href="orderListPaging?page=${pagingMaker.endPage+1}" >
						  		<button type="button" class="btn btn-default"><i class="fa fa-angle-double-right" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  
						</div>      					
      				</div><!-- /showback -->
	  </div>  <!-- main-content -->
	</div>  <!-- wrapper -->
   </div>  <!-- row mt -->
 </section>  <!-- col-lg-12 -->
</section>  <!-- form-panel -->
<%@ include file="../../include/footer.jsp"%>
</body></html>