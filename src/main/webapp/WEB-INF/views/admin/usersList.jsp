<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/left_sidebar.jsp"%>
<section id="main-content">
<!--main content start-->
	<section class="wrapper" style="margin-top: 0;">
		<div class="row mt">
			<div class="col-lg-12">
				<div class="content-panel">
					<br/>
					<h4 style="text-align:center; font-size: 40px;"><b> 회원 목록 </b></h4>
						<br/><br/>
					<section id="unseen">
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>아이디</th>
									<th>이름</th>
									<th>이메일</th>
									<th>연락처</th>
									<th>주소</th>
									<th class="numeric">가입날짜</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${list }" var="user">  
								<tr>
									<td>${user.userId }</td>
									<td>${user.userName }</td>  <!-- "/bbs/read?bid"는 controller 맵핑경로로 bid값을 함꼐 넘김. ex. 글 선택하면 url은 http://localhost:8080/bbs/read?bid=2  -->
									<td>${user.userEmail }</td>
									<td>${user.userPhone }</td>
									<td>[${user.userAddCode}]${user.userAddress1 } ${user.userAddress2 }</td>
									<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${user.regdate }"/></td> <!-- D: 365일로 표현, d: 월에 해당하는 일로 표현, H: 24시간기준, h: 12시간기준 -->
								</tr>
							</c:forEach>
							</tbody>
						</table>
					</section>

				</div>
				<!-- /content-panel -->
			</div>
			<!-- /col-lg-4 -->
		</div>
		<!-- /row -->
	</section>
	<!-- /wrapper -->

</section>
<%@ include file="../include/footer.jsp"%>
</body></html>