<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>
d
d
d
d
dd
d
d
d
d
d
d
d
<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12" style="width:100%; text-align:center;">
                  <div class="form-panel" style="width:60%; display:inline-block;">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i>   무료분양 글쓰기</h4>
                      <form class="form-horizontal style-form" method="post">
                          
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">제목</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="title" placeholder="분양글의 제목을 입력해주세요.">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptorName" placeholder="ex)홍길동">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인 아이디</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="userId">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인 연락처</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptorPhone" placeholder="ex) 010-1234-1234">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인 이메일</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptorEmail" placeholder="ex) abc@naver.com">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">분양동물</label>
                              <div class="col-sm-10">
                                  <select name="petCategory">
                                  	<option>===선택하세요===</option>
                                  	<option value="강아지">강아지</option>
                                  	<option value="고양이">고양이</option>
                                  	<option value="고슴도치">고슴도치</option>
                                  	<option value="햄스터">햄스터</option>
                                  	<option value="그 외">그 외 동물들</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">분양지역</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="petLocation">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">책임비</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="petAmount" placeholder="ex) 무료/1만원/3만원/5만원 등등">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">암수구분</label>
                              <div class="col-sm-10">
                                  <select name="petSex">
                                  	<option>===선택하세요===</option>
                                  	<option value="남아">남아</option>
                                  	<option value="여아">여아</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">내용</label>
                              <div class="col-sm-10">
                                  <textarea class="form-control" name="petContent" rows="6" placeholder="ex) 분양하는 아이에 대한 상세 정보 및 자유롭게 적어주세요."></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                          	<div class="col-sm-12" align="center">
                          		<button type="submit" class="btn btn-success">등록하기</button>
                          		<button type="submit" class="btn btn-primary">목록으로</button>
                          	</div>
                          </div>
                          
                      </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->

<%@ include file="../include/footer.jsp"%>
</body></html>