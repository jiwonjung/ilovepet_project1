<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>

d
d
d
dd
d
d
d
d

d

      <div class="modal-body">
      		<form role="form" method="post">	
				<input type="hidden" name="adoptionNo" value="${adoptionVO.adoptionNo}" />
			</form>
        	
        	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt" style="width:100%; text-align:center;">
          		<div class="col-lg-12" style="width:70%; display:inline-block;">
                  <div class="form-panel">
                      <form class="form-horizontal style-form" method="post">
                      	<h4 class="modal-title" id="myModalLabel" style="text-align:center;"><i class='fas fa-heart' style='font-size:22px;color:red'></i><b style="color:black;"> 무료분양합니다 </b><i class='fas fa-heart' style='font-size:22px;color:red'></i></h4>
                          <hr/>
                          <div class="form-group">
                              <label class="col-lg-4 col-sm-4 control-label">제목</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.title }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">등록날짜</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static"><fmt:formatDate pattern="yyyy-MM-dd  HH:mm:ss" value="${adoptionVO.regdate }"/></p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-4 col-sm-4 control-label">등록인</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.adoptorName }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">등록인 아이디</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.userId }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">연락처</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.adoptorPhone }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">이메일</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.adoptorEmail }</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-4 col-sm-4 control-label">분양동물</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.petCategory }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">분양지역</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.petLocation }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">책임비</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.petAmount }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">암수구분</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.petSex }</p>
                              </div>
                          </div>
                        
                          <div class="form-group">
                              <label class="col-lg-4 col-sm-4 control-label">내용</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.petContent }</p>
                              </div>
                          </div>
                          
                          <div class="centered">
                          	<!-- bxslider -->
                          <%-- 	<ul class="bxslider">
								<li><img src="<c:url value="/resources/custom/img/adoption_1.jpg" />" style="margin-top: 15px;" width="90%"></li>
								<li><img src="<c:url value="/resources/custom/img/adoption_1.jpg" />" style="margin-top: 15px;" width="90%"></li>
								<li><img src="<c:url value="/resources/custom/img/adoption_1.jpg" />" style="margin-top: 15px;" width="90%"></li>
							</ul> --%>
							
						  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="4000" style="height: 330px;">
						    <!-- Indicators -->
						    <ol class="carousel-indicators">
						      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						      <li data-target="#myCarousel" data-slide-to="1"></li>
						      <li data-target="#myCarousel" data-slide-to="2"></li>
						      <li data-target="#myCarousel" data-slide-to="3"></li>
						    </ol>
						
						    <!-- Wrapper for slides -->
						    <div style="height: 330px;" class="carousel-inner" role="listbox">
						
						      <div class="item active">
						        <img class="carousel_img" style="height: 320px;" src="<c:url value="/resources/custom/img/adoption_1.jpg" />" alt="이미지 준비중입니다.(1)" width="460" height="200">
						      </div>
								
						      <div class="item">
						        <img class="carousel_img" style="height: 320px;" src="https://owner.yogiyo.co.kr/media/images/owners/slide-banner1.png" alt="이미지 준비중입니다.(2)" width="460" height="200">
						      </div>
						    
						      <div class="item">
						        <img class="carousel_img" style="height: 320px;" src="<c:url value="/resources/custom/img/adoption_1.jpg" />" alt="이미지 준비중입니다.(3)" width="460" height="200">
						      </div>
						
						      <div class="item">
						        <img class="carousel_img" style="height: 320px;" alt="이미지 준비중입니다(4)" width="460" height="200">
						      </div>
						    </div>
						
						    <!-- Left and right controls -->
						    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
						      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						      <span class="sr-only">Previous</span>
						    </a>
						    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
						      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						      <span class="sr-only">Next</span>
						    </a>
						  </div>

						<div class="form-group">
							<div class="col-sm-12" align="center">
								<button type="submit" id="btn_modify" class="btn btn-success">수정하기</button>
								&nbsp;
								<button type="submit" id="btn_delete" class="btn btn-danger">삭제하기</button>
								&nbsp;
								<button type="submit" id="list_btn" class="btn btn-primary">목록으로!</button>
							</div>
						</div>

					</div> <!-- centerd -->
                      </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
          	
      </div>
       
<script>
	$(document).ready(function(){
		var formObj = $("form[role='form']");
		
		console.log("폼태그입니다..");
		
		// 수정버튼 클릭
		$("#btn_modify").on("click", function(){
			formObj.attr("action", "/adoption/update");
			formObj.attr("method", "get"); 
			formObj.submit(); 
		});
		
		// 삭제버튼 클릭
		$("#btn_delete").on("click", function(){
			var result = confirm("정말로 해당 게시글을 삭제하시겠습니까?");
			if(result){
				formObj.attr("action", "/adoption/delete");  
				formObj.submit();
			}
		});
		
		// 목록가기버튼 클릭
		$("#list_btn").on("click", function(){
			self.location ="/adoption/list";
		});
	});
</script>

<%@ include file="../include/footer.jsp"%>
</body></html>