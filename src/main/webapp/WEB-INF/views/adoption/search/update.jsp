<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>

<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12" style="width:100%; text-align:center;">
                  <div class="form-panel" style="width:70%; display:inline-block; max-hieght: 1000px;">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i>   무료분양 게시물 수정페이지</h4>
                      <form role="form" class="form-horizontal style-form" method="post" enctype="multipart/form-data">
                          <input type="hidden" name="page" value="${sCriteria.page}" /> <!-- 수정처리 컨트롤러로 넘길 때 수정처리에 필요한 정보는: bid, page, numPerPage 이므로, hidden으로  넘김 -->
						  <input type="hidden" name="numPerPage" value="${sCriteria.numPerPage }" />
						  <input type="hidden" name="searchingCategory" value="${sCriteria.searchingCategory}" />
						  <input type="hidden" name="keyword" value="${sCriteria.keyword}" />
							
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">글번호</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptionNo" value="${adoptionVO.adoptionNo }" readonly="readonly">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">분양상태</label>
                              <div class="col-sm-10">
                                  <input type="radio" name="state" value="분양중" ${adoptionVO.state eq '분양중' ? "checked" : ""} />&nbsp;분양중  &nbsp;&nbsp;&nbsp;
                                  <input type="radio" name="state" value="분양완료" ${adoptionVO.state eq '분양완료' ? "checked" : ""} />&nbsp;분양완료
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">제목</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="title" value="${adoptionVO.title }">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptorName" value="${adoptionVO.adoptorName }">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인 아이디</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="userId" value="${adoptionVO.userId }" readonly="readonly">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인 연락처</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptorPhone" value="${adoptionVO.adoptorPhone }">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인 이메일</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptorEmail" value="${adoptionVO.adoptorEmail }">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">분양동물</label>
                              <div class="col-sm-10">
                                  <select name="petCategory">
                                  	<option value="${adoptionVO.petCategory }">${adoptionVO.petCategory }</option>
                                  	<option value="강아지">강아지</option>
                                  	<option value="고양이">고양이</option>
                                  	<option value="고슴도치">고슴도치</option>
                                  	<option value="햄스터">햄스터</option>
                                  	<option value="그 외">그 외 동물들</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">분양지역</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="petLocation" value="${adoptionVO.petLocation }">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">책임비</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="petAmount" value="${adoptionVO.petAmount }">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">암수구분</label>
                              <div class="col-sm-10">
                                  <select name="petSex">
                                  	<option value="${adoptionVO.petSex }">${adoptionVO.petSex }</option>
                                  	<option value="남아">남아</option>
                                  	<option value="여아">여아</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">내용</label>
                              <div class="col-sm-10">
                                  <textarea class="form-control" name="petContent" rows="6">${adoptionVO.petContent }</textarea>
                              </div>
                          </div>
                          <div class="form-group">
							 <label class="col-sm-1 col-sm-1 control-label">사진</label>
							 <div class="col-sm-11">
							 <input type="file" id="petImg" name="file" />
							 <div class="select_img" style="text-align: center;">
							  <img src="${adoptionVO.petImg}" style="width: 750px; height: auto; margin: 20px 0;"/>
							  <input type="hidden" name="petImg" value="${adoptionVO.petImg}" />
							  <input type="hidden" name="petThumbImg" value="${adoptionVO.petThumbImg}" /> 
							 </div>
							 </div>
							 <script>
							  $("#petImg").change(function(){
							   if(this.files && this.files[0]) {
							    var reader = new FileReader;
							    reader.onload = function(data) {
							     $(".select_img img").attr("src", data.target.result).width(500);        
							    }
							    reader.readAsDataURL(this.files[0]);
							   }
							  });
							 </script>
							 <%-- <%=request.getRealPath("/") %> --%>
							</div>
                      </form>

						<div class="form-group">
							<div class="col-sm-12" align="center">
								<button type="submit" id="btn_modify" class="btn btn-success">저장하기</button>
								<button type="submit" id="btn_back" class="btn btn-warning">뒤로가기</button>
							</div>
						</div>
						
					</div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
<script>
	$(document).ready(function(){
		var formObj = $("form[role='form']");

	 	$("input[name='state']:radio").change(function () {
	 		var st = $(":input:radio[name=state]:checked").val();
			console.log(st);
			$('input:radio[name=state]:input[value=' + st + ']').attr("checked", true);
			
		}); 
		
		// 수정버튼 클릭
		$("#btn_modify").on("click", function(){
			
			var result = confirm("해당 게시글을 수정하시겠습니까?");
			if(result) {
				formObj.submit(); 
			}
		});
		
		// 뒤로가기버튼 클릭
		$("#btn_back").on("click", function(){
			window.history.back();
		});
		
		
	});
</script>
<%@ include file="../../include/footer.jsp"%>
</body></html>