<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>

      <section class="wrapper" style="margin-top:0;">
      		<form role="form" method="post">	
				<input type="hidden" name="adoptionNo" value="${adoptionVO.adoptionNo}" />
				<input type="hidden" name="page" value="${sCriteria.page}" />
				<input type="hidden" name="numPerPage" value="${sCriteria.numPerPage}" />
				<input type="hidden" name="searchingCategory" value="${sCriteria.searchingCategory}" />
				<input type="hidden" name="keyword" value="${sCriteria.keyword}" />
			</form>
        	
        	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12" style="width:100%; text-align:center;">
                  <div class="form-panel" style="max-height:1200px; width: 60%; display:inline-block;">
                      <form class="form-horizontal style-form" method="post">
                      	<h4 class="modal-title" id="myModalLabel" style="text-align:center;"><i class='fas fa-heart' style='font-size:22px;color:red'></i><b style="color:black;"> 무료분양합니다 </b><i class='fas fa-heart' style='font-size:22px;color:red'></i></h4>
                          <hr/>
                          <div class="form-group">
                              <label class="col-lg-4 col-sm-4 control-label">제목</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static" name="title">${adoptionVO.title }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">등록날짜</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static"><fmt:formatDate pattern="yyyy-MM-dd  HH:mm:ss" value="${adoptionVO.regdate }"/></p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">분양상태</label>
                              <div class="col-lg-8">
                              <c:if test="${adoptionVO.state == '분양중'}"><span class="label label-success">${adoptionVO.state }</span></c:if>
                              <c:if test="${adoptionVO.state eq '분양완료'}"><span class="label label-danger">${adoptionVO.state }</span></c:if>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-4 col-sm-4 control-label">등록인</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.adoptorName }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">등록인 아이디</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.userId }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">연락처</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.adoptorPhone }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">이메일</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.adoptorEmail }</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-4 col-sm-4 control-label">분양동물</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.petCategory }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">분양지역</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.petLocation }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">책임비</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.petAmount }</p>
                              </div>
                              <label class="col-lg-4 col-sm-4 control-label">암수구분</label>
                              <div class="col-lg-8">
                                  <p class="form-control-static">${adoptionVO.petSex }</p>
                              </div>
                          </div>
                        
                          <div class="form-group">
                              <div style="text-align:center;">▼상세내용</div>
                          </div>
                          <div class="form-group">
                                  <p class="form-control-static" style="text-align: center;">${adoptionVO.petContent }</p>
                          </div>
                          <c:if test="${adoptionVO.petImg != null }">
                          <div class="centered">
                          	<img src="${adoptionVO.petImg}" style="width: 530px; height: 400px; cursor: pointer;" onclick="fnImgPop(this.src)"/>
                          	<br/><p>※ 이미지를 클릭하면 크게 보실 수 있습니다.</p>
						   	 	<script type="text/javascript">
								 function fnImgPop(url){
								  var img=new Image();
								  img.src=url;
								  var img_width=img.width;
								  var win_width=img.width+25;
								  var img_height=img.height;
								  var win=img.height+30;
								  var OpenWindow=window.open('','_blank', 'width='+img_width+', height='+img_height+', menubars=no, scrollbars=auto');
								  OpenWindow.document.write("<style>body{margin:0px;}</style><img src='"+url+"' width='"+win_width+"'>");
								 }
								</script>
						   </div> <!-- centerd -->
						  </c:if>
                      </form>

						<div class="form-group">
							<div class="col-sm-12" align="center">
								<c:if test="${login.userId == adoptionVO.userId}">
									<button type="submit" id="btn_modify" class="btn btn-success">수정하기</button>
									&nbsp;
								</c:if>
								<c:if test="${login.userId == adoptionVO.userId || login.isAdmin == 1}">
									<button type="submit" id="btn_delete" class="btn btn-danger">삭제하기</button>
									&nbsp;
								</c:if>
								<button type="submit" id="list_btn" class="btn btn-primary">목록으로</button>
							</div>
						</div>

                  </div> <!-- form-panel -->
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
          	
     </section> 
       
<script>
	$(document).ready(function(){
		var formObj = $("form[role='form']");
		
		console.log("폼태그입니다..");
		
		// 수정버튼 클릭
		$("#btn_modify").on("click", function(){
			formObj.attr("action", "/adoption/search/update");
			formObj.attr("method", "get"); 
			formObj.submit(); 
		});
		
		// 삭제버튼 클릭
		$("#btn_delete").on("click", function(){
			var result = confirm("정말로 해당 게시글을 삭제하시겠습니까?");
			if(result){
				formObj.attr("action", "/adoption/search/delete");  
				formObj.submit();
			}
		});
		
		// 목록가기버튼 클릭
		$("#list_btn").on("click", function(){
			formObj.attr("action", "/adoption/search/list");  
			formObj.attr("method", "get"); 
			formObj.submit();
		});
	});
</script>

<%@ include file="../../include/footer.jsp"%>
</body></html>