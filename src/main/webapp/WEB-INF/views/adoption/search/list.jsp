<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
	<section class="wrapper" style="margin-top: 0;">

<div class="row" >
	<div class="col-lg-12 main-chart" style="padding-top:0px;">
		<div class="row mt" style="margin-top: 0;">
			<h2 style="text-align:center;"><b> 무료분양 </b></h2><hr/>
			 		<!-- 검색창 -->
                   <form class="form-inline" role="form" align="center">
                       <span class="col-md-12"> 
                     <select class="form-control" name="searchingCategory">
					  <option value="N" 
					  	<c:out value="${sCriteria.searchingCategory == null? 'selected':''}"/>>----선택하세요----</option>
					  <option value="C"
					    <c:out value="${sCriteria.searchingCategory == 'C'? 'selected' : ''}"/>>동물타입   ex)강아지/고양이/햄스터/고슴도치</option>
					  <option value="S"
					    <c:out value="${sCriteria.searchingCategory == 'S'? 'selected' : ''}"/>>분양상태   ex)분양중/분양완료</option>
					  <option value="W"
					    <c:out value="${sCriteria.searchingCategory == 'W'? 'selected': ''}"/>>작성자</option>
					  <option value="CS"
					    <c:out value="${sCriteria.searchingCategory == 'CS'? 'selected' :''}"/>>동물타입+분양상태</option>
					  <option value="SW"
					    <c:out value="${sCriteria.searchingCategory =='SW'? 'selected':''}"/>>분양상태+작성자</option>
					  <option value="CSW"
					    <c:out value="${sCriteria.searchingCategory =='CSW'? 'selected':''}"/>>동물타입+분양상태+작성자</option>
					</select>
                           <input type="text" class="form-control" name="keyword" id="searchword" value="${sCriteria.keyword}"/>
                        <button type="submit" class="btn btn-theme" id="searchBtn">검색</button> 
					</span>
					
						
                   </form> 
                   <br/><br/><br/>
			<!-- 분양동물 리스트 틀 -->
			<c:forEach items="${list }" var="adoption">
				<a href="/adoption/search/read${pagingMaker.makeSearchingURI(pagingMaker.cri.page)}&adoptionNo=${adoption.adoptionNo }">
					<div class="col-md-3 col-sm-3 mb" data-toggle="modal" data-target="#myModal" class="quickview">
						<div class="white-panel pn" style="width: 220px;">
							<!-- 메인사진 -->
							<c:if test="${adoption.petImg == null }">
								<div class="centered">
									<img src="<c:url value="/resources/custom/img/none.png" />" style="width:100%; height:170px;">
								</div>
							</c:if>
							<c:if test="${adoption.petImg != null }">
								<div class="centered">
									<img src="${adoption.petImg }" style="width:100%; margin-bottom: 8px; height:170px;">
								</div>
							</c:if>
							<!-- 제목 및 등록날짜 -->
							<div class="row">
								<div class="col-md-12">
									<h5 style="color: black;"><b>${adoption.title}</b></h5>
								</div>
								<div class="col-md-12">
									<p style="color: #999999"><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${adoption.regdate }"/> 
										<span class="<c:out value="${adoption.state == '분양중' ? 'label label-success' : 'label label-danger'}" />" style="margin-left:15px;">
											${adoption.state}
										</span>
									</p>
								</div>
							</div>
						</div>
					</div><!-- /col-md-3 -->
				</a>
			</c:forEach>

		</div> <!-- /row mt -->
	</div> <!-- main-chart -->
	
		<div class="showback" align="center">
			<div class="btn-group">
			  <c:if test="${pagingMaker.prev }">
			  	<%-- <a href="pagingList?page=${pagingMaker.startPage-1}" > --%>
			  	<a href="list${pagingMaker.makeSearchingURI(pagingMaker.startPage-1)}" >
			  	<button type="button" class="btn btn-default"><i class="fa fa-angle-double-left" style="font-size:10px"></i></button>
			  	</a>
			  </c:if>
			  <c:forEach begin="${pagingMaker.startPage }" end="${pagingMaker.endPage }" var="pageNum">
			  <a href="list${pagingMaker.makeSearchingURI(pageNum)}">
			 	 <button type="button" class="<c:out value="${pagingMaker.cri.page == pageNum?'btn btn-primary':'btn btn-default'}"/>">${pageNum}</button> <!--${pagingMaker.cri.page}은 pagingMaker.getCri()과 PageCriteria의 getPage()와 같음  -->
			  </a>
			  </c:forEach>
			  <c:if test="${pagingMaker.next && pagingMaker.endPage > 0 }">
			  	<a href ="list${pagingMaker.makeSearchingURI(pagingMaker.endPage+1) }">
			  		<button type="button" class="btn btn-default"><i class="fa fa-angle-double-right" style="font-size:10px"></i></button>
			  	</a>
			  </c:if>
			  
			</div>      					
				 
				 <div class="pull-right">
					<button type="button" class="btn btn-theme02 btn-flat"
						id="writeBtn" style="margin-right: 30px;">
						<i class='fas fa-pen'> 글쓰기</i> 
					</button>
				</div>
   	 	</div><!-- /showback -->
   	 	
						
</div><!-- /row -->
</section>

<script type="text/javascript"> 
$(document).ready(function(){ 
	var img = document.getElementsByClassName("carousel_img");
	for (var x = 0; x < img.length; x++) {
		img.item(x).onclick = function() {
			window.open(this.src)
		};
	}
	
	$('#writeBtn').on("click", function(e) {
		self.location = "write";  // 컨트롤러의 fbbs/write으로 감
	});

});
</script>
<script>
	var result= '${result}';
	
	if (result == 'writeSuccess') {
		alert('분양글이 정상적으로 입력되었습니다.');
	} else if (result == 'deleteSuccess') {
		alert('게시글이 정상적으로 삭제되었습니다.');
	} else if (result == 'updateSuccess') {
		alert('게시글이 정상적으로 수정되었습니다.');
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#searchBtn').on("click", function(e) {
			self.location = "list" 
				+ "${pagingMaker.makeSearchingURI(1)}" /* 검색하게되면 1페이지부터 데이터 나오는 것 */
				+ "&searchCategory="
				+ $("select option:selected").val()
				+ "&keyword=" + $("#searchword").val()
		});
		
		$('#writeBtn').on("click", function(e) {
			self.location = "write";  // 컨트롤러의 fbbs/write으로 감
		});
		
	});
</script>
<%@ include file="../../include/footer.jsp"%>
</body></html>