<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>

<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12" style="width:100%; text-align:center;">
                  <div class="form-panel" style="width:60%; height: 1000px; margin-top: 0; display:inline-block;">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i>   무료분양 글쓰기</h4>
                      <form role="form" class="form-horizontal style-form" method="post" enctype="multipart/form-data">
                          
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">제목</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="title" placeholder="분양글의 제목을 입력해주세요." required="required">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptorName" placeholder="ex)홍길동" required="required">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인 아이디</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="userId" value="${login.userId}" readonly="readonly">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인 연락처</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptorPhone" placeholder="ex) 010-1234-1234" required="required">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록인 이메일</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="adoptorEmail" placeholder="ex) abc@naver.com" required="required">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">분양동물</label>
                              <div class="col-sm-10">
                                  <select name="petCategory" required>
                                  	<option value="">===선택하세요===</option>
                                  	<option value="강아지">강아지</option>
                                  	<option value="고양이">고양이</option>
                                  	<option value="고슴도치">고슴도치</option>
                                  	<option value="햄스터">햄스터</option>
                                  	<option value="그 외">그 외 동물들</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">분양지역</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="petLocation" required="required">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">책임비</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="petAmount" placeholder="ex) 무료/1만원/3만원/5만원 등등" required="required">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">암수구분</label>
                              <div class="col-sm-10">
                                  <select name="petSex" required>
                                  	<option value="">===선택하세요===</option>
                                  	<option value="남아">남아</option>
                                  	<option value="여아">여아</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">내용</label>
                              <div class="col-sm-10">
                                  <textarea class="form-control" name="petContent" rows="6" placeholder="ex) 분양하는 아이에 대한 상세 정보 및 자유롭게 적어주세요." required="required"></textarea>
                              </div>
                          </div>
                          <!-- 첨부파일 영역 추가  -->
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">사진첨부</label>
                              <div class="col-sm-10">
                                  <input type="file" name="file" class="form-control" id="petImg" required />
                                  <div class="select_img" style="margin: 7px 0; text-align: center;"><img src=""/></div>
                                  	<script>
									  $("#petImg").change(function(){
									   if(this.files && this.files[0]) {
									    var reader = new FileReader;
									    reader.onload = function(data) {
									     $(".select_img img").attr("src", data.target.result).width(330);        
									     $('.form-panel').css({height: 1300 });
									    }
									    reader.readAsDataURL(this.files[0]);
									   }
									  });
									 </script>
									<%--  <%=request.getRealPath("/") %> --%>
                              </div>
                          </div>
                          
                          <div class="form-group">
                          	<div class="col-sm-12" align="center">
                          		<button type="submit" class="btn btn-success">등록하기</button>
                          		<button type="submit" id="list_btn" class="btn btn-primary">목록으로</button>
                          	</div>
                          </div>
                      </form>
                       
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
	
<script>

    $(document).ready(function () {
    	var formObj = $("form[role='form']");
		
		// 목록가기버튼 클릭
		$("#list_btn").on("click", function(){
			formObj.attr("action", "/adoption/search/list");  
			formObj.attr("method", "get"); 
			formObj.submit();
		});
    });
</script>
<%@ include file="../../include/footer.jsp"%>
</body></html>