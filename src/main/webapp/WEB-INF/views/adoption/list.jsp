<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>
	<section class="wrapper">

<div class="row" >
	<div class="col-lg-12 main-chart" style="margin-left:30px;">
		<div class="row mt">
			
			<!-- 분양동물 리스트 틀 -->
			<c:forEach items="${list }" var="adoption">
				<a href="/adoption/read?adoptionNo=${adoption.adoptionNo }">
					<div class="col-md-3 col-sm-3 mb" data-toggle="modal" data-target="#myModal" class="quickview">
						<div class="white-panel pn" style="width: 220px;">
							<!-- 메인사진 -->
							
							<div class="centered">
								<img src="<c:url value="/resources/custom/img/adoption_1.jpg" />" style="width:100%; height:170px;">
							</div>
							<!-- 제목 및 등록날짜 -->
							<div class="row">
								<div class="col-md-12">
									${adoption.adoptionNo} &nbsp; &nbsp; &nbsp; &nbsp;<h5>${adoption.title}</h5>
								</div>
								<div class="col-md-12">
									<p><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${adoption.regdate }"/> 
										<span class="<c:out value="${adoption.state == '분양중' ? 'label label-success' : 'label label-danger'}" />" style="margin-left:35px;">
											${adoption.state}
										</span>
									</p>
								</div>
							</div>
						</div>
					</div><!-- /col-md-3 -->
				</a>
			</c:forEach>

		</div> <!-- /row mt -->
	</div> <!-- main-chart -->
</div><!-- /row -->
</section>

<script type="text/javascript"> 
$(document).ready(function(){ 
	var img = document.getElementsByClassName("carousel_img");
	for (var x = 0; x < img.length; x++) {
		img.item(x).onclick = function() {
			window.open(this.src)
		};
	}

});
</script>
<script>
	var result= '${result}';
	
	if (result == 'writeSuccess') {
		alert('분양글이 정상적으로 입력되었습니다.');
	} else if (result == 'deleteSuccess') {
		alert('게시글이 정상적으로 삭제되었습니다.');
	} else if (result == 'modifySuccess') {
		alert('게시글이 정상적으로 수정되었습니다.');
	}
</script>
<%@ include file="../include/footer.jsp"%>
</body></html>