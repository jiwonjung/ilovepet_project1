<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

      <!--footer start-->
      <footer>
          <div class="text-center"> <hr/>
               <p>(주)아이러브펫/대표 정지원/서울특별시 강남구 학동로 강남타워</p>
               <p>사업자등록번호 123-00-12345/통신판매업 제 2019-서울강남-01234호 사업자정보 확인</p>
               <p>개인정보보호책임자 정지원/팩스 010-6876-5125/이메일 seg615@naver.com</p>
        		<address>고객센터 1588-1234/평일 09:00~18:00 주말/공휴일 휴무</address>
          </div>  
      </footer>
     <!--  footer end -->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<c:url value="/resources/assets/js/jquery.js" />" ></script>
    <script src="<c:url value="/resources/assets/js/jquery-1.8.3.min.js" />" ></script>
    <script src="<c:url value="/resources/assets/js/bootstrap.min.js" />" ></script>
    <script class="include" type="text/javascript" src="<c:url value="/resources/assets/js/jquery.dcjqaccordion.2.7.js" />" ></script>
    <script src="<c:url value="/resources/assets/js/jquery.scrollTo.min.js" />" ></script>
    <script src="<c:url value="/resources/assets/js/jquery.nicescroll.js"/>"  type="text/javascript" ></script>
    <script src="<c:url value="/resources/assets/js/jquery.sparkline.js" />" ></script>


    <!--common script for all pages-->
    <script src="<c:url value="/resources/assets/js/common-scripts.js"  />" ></script>
    
    <script type="text/javascript" src="<c:url value="/resources/assets/js/gritter/js/jquery.gritter.js"  />" ></script>
    <script type="text/javascript" src="<c:url value="/resources/assets/js/gritter-conf.js"  />" ></script>

    <!--script for this page-->
    <script src="<c:url value="/resources/assets/js/sparkline-chart.js"  />" ></script>    
	<script src="<c:url value="/resources/assets/js/zabuto_calendar.js"  />" ></script>	
