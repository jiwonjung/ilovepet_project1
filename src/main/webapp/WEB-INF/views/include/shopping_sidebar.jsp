 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion" style="margin-top: 50px;">
                  
              	  <h3 style="color:white; font-weight: bold; margin-left: 30px; margin-top: 0; font-weight: JejuGothic;"><!-- <i class='fas fa-home' style='font-size:20px'></i> --> 펫쇼핑몰</h3>
              	  <br/><br/>
                  <li class="sub-menu">
                      <a href="/petmall/search/listByCategory?c=100&l=1" >
                          <i class='fas fa-paw' style='font-size:20px'></i>
                          <span><h4> 강아지</h4></span>
                      </a>
                      <ul class="sub">
                          <li><a href="/petmall/search/listByCategory?c=101&l=2">사료</a></li>
                          <li><a href="/petmall/search/listByCategory?c=102&l=2">간식</a></li>
                          <li><a href="/petmall/search/listByCategory?c=103&l=2">강아지옷</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="/petmall/search/listByCategory?c=200&l=1" >
                          <i class='fas fa-paw' style='font-size:20px'></i>
                          <span><h4>고양이</h4></span>
                      </a>
                      <ul class="sub">
                          <li><a href="/petmall/search/listByCategory?c=201&l=2">사료 및 간식</a></li>
                          <li><a href="/petmall/search/listByCategory?c=202&l=2">모래/탈취제</a></li>
                          <li><a href="/petmall/search/listByCategory?c=203&l=2">스크래쳐/캣타워</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="/petmall/search/listByCategory?c=300&l=1" >
                          <i class='fas fa-paw' style='font-size:20px'></i>
                          <span><h4>고슴도치</h4></span>
                      </a>
                      <ul class="sub">
                          <li><a href="/petmall/search/listByCategory?c=301&l=2">사료</a></li>
                          <li><a href="/petmall/search/listByCategory?c=302&l=2">은신처</a></li>
                          <li><a href="/petmall/search/listByCategory?c=303&l=2">위생/청결/화장실</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="/petmall/search/listByCategory?c=400&l=1" >
                          <i class='fas fa-paw' style='font-size:20px'></i>
                          <span><h4>햄스터</h4></span>
                      </a>
                      <ul class="sub">
                          <li><a href="/petmall/search/listByCategory?c=401&l=2">사료</a></li>
                          <li><a href="/petmall/search/listByCategory?c=402&l=2">장남감/쳇바퀴</a></li>
                          <li><a href="/petmall/search/listByCategory?c=403&l=2">베딩/모래</a></li>
                      </ul>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
     
                  
                  