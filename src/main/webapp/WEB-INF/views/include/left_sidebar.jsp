 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
<script>
	$(function() {
		// tab operation
		$('.sub-menu tab').click(function() {
			$("#tabcontent").empty();
			var userId = '${login.userId}';
			var urlPath = '/user/mypage/';
			var activeTab = $(this).attr('data-tab');
			$.ajax({
				type : 'GET',                 //get방식으로 통신
				url : urlPath + activeTab,    //탭의 data-tab속성의 값으로 된 html파일로 통신
				data: 
					{userId : userId},
				dataType : "html",            //html형식으로 값 읽기
				error : function() {          //통신 실패시
					alert('통신실패! 관리자에게 문의하세요.');
				},
				success : function(data) {    //통신 성공시 탭 내용담는 div를 읽어들인 값으로 채운다.
					$('#tabcontent').html(data);
				}
			});
		});
		$('#default').click();          
	});
</script>     
     
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion" style="margin-top: 45px;">
              	  
              	  <h5 class="centered">${login.userName}님의 마이페이지</h5>
              	  	 <br><br>	
              	  <c:if test="${login != null && login.isAdmin == 1}">
                  <li data-tab="tab_register" class="sub-menu">
                      <a href="/petmall/search/insert">
                          <i class="fa fa-book"></i>
                          <span>상품등록하기(관리자전용)</span>
                      </a>
                  </li>
                  <li data-tab="tab_register" class="sub-menu">
                      <a href="/admin/petmall/orderListPaging">
                          <i class="fa fa-book"></i>
                          <span>전체주문내역(관리자전용)</span>
                      </a>
                  </li>
                  <li data-tab="tab_register" class="sub-menu">
                      <a href="/admin/listUsersPaging">
                          <i class="fa fa-book"></i>
                          <span>회원 전체 목록(관리자전용)</span>
                      </a>
                  </li>
                  </c:if>
                  <li class="sub-menu">
                      <a href="/petmall/cart/list" id="myCart">
                          <i class="fa fa-book"></i>
                          <span>장바구니</span>
                      </a>
                  </li>
                  <li class="sub-menu" >
                      <a href="/petmall/cart/orderList" id="myPayment">
                          <i class="fa fa-book"></i>
                          <span>구매내역</span>
                      </a>
                  </li>
                  <li data-tab="myinfo" class="sub-menu tab">
                      <a href="/user/mypage/myinfo?userId=${userVO.userId }" id="myInfo">
                          <i class="fa fa-book"></i>
                          <span>내정보</span>
                      </a>
                  </li>
                  

              </ul>
              <!-- sidebar menu end-->
              
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- tab 내용 -->
     <section id="main-content">
         <!-- <section class="wrapper site-min-height"> -->
         	<div class="row mt">
         		
         	</div>
		
	<!-- </section>/wrapper -->
	
	
</section><!-- /MAIN CONTENT -->
     
     
     
                  
                  