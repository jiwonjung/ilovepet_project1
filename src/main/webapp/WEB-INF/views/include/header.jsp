<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.jiwon.ilovepet.user.vo.UserVO"%>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              
            <!--logo start-->
            <a href="/" class="logo" style="font-size: 34px;"><i class='fas fa-paw' style='font-size:33px; cursor:pointer; vertical-align: center;'></i>&nbsp;<b>I♥PET</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav navbar-nav" style="text-align:center; cursor:pointer;">
			      <%-- <c:if test="${login != null && login.isAdmin == 1}">
				      <li><a href="/petmall/search/insert" style="font-size: 17px; color: #595959; font-weight: bold;">상품등록(관리자용)</a></li>
				  </c:if> --%>
			      <li class="active"><a href="/adoption/search/list" style="font-size: 17px; color: #595959; font-weight: bold;">무료분양</a></li>
			      <li><a href="/petmall/search/list" style="font-size: 17px; color: #595959; font-weight: bold;">펫쇼핑몰</a></li>
			      <li><a href="/bbs/search/list" style="font-size: 17px; color: #595959; font-weight: bold;">자유게시판</a></li>
			    </ul>
			        
            </div>
            <div class="top-menu">
            	<ul class="nav pull-right top-menu" style="float: right;">
            		<c:if test="${login != null}">
            		<div class="btn-group" style="margin:0 15px 0 0;">
						<button type="button" class="btn btn-theme btn-sm dropdown-toggle" style="margin-top: 35px;" data-toggle="dropdown">
						    마이페이지 <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						   <c:if test="${login != null && login.isAdmin == 1}">
						    <li><a href="/petmall/search/insert">상품등록(관리자용)</a></li>
			                <li><a href="/admin/petmall/orderListPaging">전체주문내역(관리자용)</a></li>
			                <li><a href="/admin/listUsersPaging">회원전체목록(관리자용)</a></li>
						    </c:if>
						    <li><a href="/petmall/cart/list">장바구니</a></li>
			                <li><a href="/petmall/cart/orderList">구매내역</a></li>
			                <li><a href="/user/mypage/myinfo?userId=${userVO.userId }">내정보</a></li>
						  </ul>
					</div>
					<!--  <a href="/user/logout"><button type="button" style="margin-top:15px;" class="btn btn-theme" id="logout_btn">로그아웃</button></a> -->
					  <a href="/user/logout"><button type="button" style="margin:35px 15px 0 15px;" class="btn btn-primary btn-sm" id="logout_btn"> <span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;로그아웃</button></a>	
                    </c:if>
                    <c:if test="${login == null}">
                    	<a href="/user/login"><button type="button" style="margin:35px 15px 0 0;" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;로그인</button></a>	
                    	<a href="/user/register"><button type="button" style="margin-top:35px;" class="btn btn-theme btn-sm"> <i class='fas fa-user'></i>&nbsp;&nbsp;회원가입</button></a>
                    </c:if>
            	</ul>
            </div>
        </header>
        
      <!--header end-->
<script>
$(document).ready(function() {
	
	var userResult = '${login}';
	var userName = '${user.userName}';
	var loginChk = '${loginChk}';
	
	console.log('userResult:' + userResult);
	if(userResult != null && loginChk == 1) {
	
		alert('성공적으로 로그인했습니다!');
	}
	
	/* 로그아웃 버튼 클릭 */
	$('#logout_btn').on("click", function() {
		if(confirm("로그아웃 하시겠습니까?")) {
			window.location.href="/user/logout";
		} else {
			window.location.reload(); // 현재페이지 새로고침
		}
	});
});
</script>