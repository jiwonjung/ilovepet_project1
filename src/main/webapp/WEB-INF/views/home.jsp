<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>

	<%@ include file="include/head.jsp"%>
	<%@ include file="include/header.jsp"%>
  <body style="background-color:white;">
<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="4000" style="height: 370px;">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div style="height: 370px;" class="carousel-inner" role="listbox">

    <div class="item active">
      <img class="carousel_img" style="height: 370px;" src="<c:url value="/resources/custom/img/carousel_5.jpg" />" alt="이미지 준비중입니다.(1)" width="460" height="370">
    </div>

    <div class="item">
      <img class="carousel_img" style="height: 370px;" src="<c:url value="/resources/custom/img/carousel_4.jpg" />" alt="이미지 준비중입니다.(1)" width="460" height="370">
    </div>
  
    <div class="item">
      <img class="carousel_img" style="height: 370px;" src="<c:url value="/resources/custom/img/carousel_2.jpg" />" alt="이미지 준비중입니다.(1)" width="450" height="370">
    </div>

    <div class="item">
     <a href="/petmall/search/listByCategory?c=201&l=2"><img class="carousel_img" style="height: 370px;" src="<c:url value="/resources/custom/img/cat_carousel.jpg" />" alt="이미지 준비중입니다.(1)" width="460" height="370"></a>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
          <section class="wrapper" style="margin-top:0;">

              <div class="row" >
                  <div class="col-lg-12 main-chart" style="width: 100%; text-align: center;">
                    <h4 style="color:black;"><b>무료분양 최신글</b></h4>
                    <hr/>
                    
                      <div class="row mt">
                      <!-- SERVER STATUS PANELS -->
						<c:forEach items="${listMainAdoption }" var="adoption">
                      	<a href="/adoption/search/read?${pagingMaker.makeSearchingURI(pagingMaker.cri.page)}&adoptionNo=${adoption.adoptionNo }">
                      	<div class="col-md-3 col-sm-3 mb" style="cursor:pointer;">
                      		<div class="white-panel pn">
	                      		<div class="centered" style="cursor:pointer;">
								<img src="${adoption.petImg }" style="width:100%; height:170px; margin-bottom: 10px;">
								
							</div>
							<!-- 제목 및 등록날짜 -->
							<div class="row">
								<div class="col-md-12">
									<h5 style="color:black;"><b>${adoption.title}</b></h5>
								</div>
								<div class="col-md-12">
									<p style="color: #999999"><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${adoption.regdate }"/> 
										<span class="<c:out value="${adoption.state == '분양중' ? 'label label-success' : 'label label-danger'}" />" style="margin-left:15px;">
											${adoption.state}
										</span>
									</p>
								</div>
							</div>
                      		</div>
                      	</div><!-- /col-md-4 -->
                      	</a>
                      	</c:forEach>
                      	
                    <a href="/adoption/search/list"  style="float:right; font-size: 18px; margin-right: 30px;">+ 더보기</a>
                     <br class="clear" style="clear:both;">
                    <br/><br/><h4 style="color:black;"><b>펫쇼핑몰 최신상품</b></h4>
                    <hr/>
                    
                  
                  <c:forEach items="${listMainProudct }" var="product">
                  	<div class="col-md-3 col-sm-3 mb" style="cursor:pointer;">
                  	<a href="/petmall/search/read?${pagingMaker.makeSearchingURI(pagingMaker.cri.page)}&productNo=${product.productNo }">
                      		<div class="white-panel pn" >
                      			<div class="white-header" style="margin-bottom:6px;">
						  			<h5>NO. ${product.productNo }</h5>
                      			</div>
	                      		<div class="centered">
										<img src="${product.productUrl }" alt="이미지 준비중입니다."  style="width:80%; height:140px;">
	                      		</div>
	                      		<div class="row">
								<div class="col-md-12">
									<h5 style="color: black;"><b>${product.productName }</b></h5>
									<h5 style="color: #ff0000;"><b><fmt:formatNumber pattern="###,###,###" value="${product.productPrice }" />원 &nbsp;&nbsp;&nbsp;<span class="badge bg-default"><i class='far fa-comment'></i> ${product.reviewCnt}</span></b></h5>
								</div>
							</div>
							</div>
					</a>
                      	</div><!-- /col-md-4 -->
                     </c:forEach> 	
                  	   <a href="petmall/search/list"  style="float:right; font-size: 18px; margin-right: 30px;">+ 더보기</a><br/><br/>
                  <br class="clear" style="clear:both;">
                      	
     <br class="clear" style="clear:both;">
     <!--footer start-->
      <footer><br/>
          <div class="text-center"> <hr/>
               <p>(주)아이러브펫/대표 정지원/서울특별시 강남구 학동로 강남타워</p>
               <p>사업자등록번호 123-00-12345/통신판매업 제 2019-서울강남-01234호 사업자정보 확인</p>
               <p>개인정보보호책임자 정지원/팩스 010-6876-5125/이메일 seg615@naver.com</p>
        		<address>고객센터 1588-1234/평일 09:00~18:00 주말/공휴일 휴무</address>
          </div>  
      </footer>
     <!--  footer end -->
  </section>
<script>
	var result = '${result}';
	if(result == 'logoutSuccess') {
		alert('성공적으로 로그아웃되었습니다.');
	} 

	
</script>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<c:url value="/resources/assets/js/jquery.js" />" ></script>
    <script src="<c:url value="/resources/assets/js/jquery-1.8.3.min.js" />" ></script>
    <script src="<c:url value="/resources/assets/js/bootstrap.min.js" />" ></script>
    <script class="include" type="text/javascript" src="<c:url value="/resources/assets/js/jquery.dcjqaccordion.2.7.js" />" ></script>
    <script src="<c:url value="/resources/assets/js/jquery.scrollTo.min.js" />" ></script>
    <script src="<c:url value="/resources/assets/js/jquery.nicescroll.js"/>"  type="text/javascript" ></script>
    <script src="<c:url value="/resources/assets/js/jquery.sparkline.js" />" ></script>


    <!--common script for all pages-->
    <script src="<c:url value="/resources/assets/js/common-scripts.js"  />" ></script>
    
    <script type="text/javascript" src="<c:url value="/resources/assets/js/gritter/js/jquery.gritter.js"  />" ></script>
    <script type="text/javascript" src="<c:url value="/resources/assets/js/gritter-conf.js"  />" ></script>

    <!--script for this page-->
    <script src="<c:url value="/resources/assets/js/sparkline-chart.js"  />" ></script>    
	<script src="<c:url value="/resources/assets/js/zabuto_calendar.js"  />" ></script>	
	

  </body>
</html>
