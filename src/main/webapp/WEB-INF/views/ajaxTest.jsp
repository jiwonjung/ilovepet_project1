<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<title>Insert title here</title>
<style>
	#modifyDiv {
		width: 500px;
		height: 100px;
		background-color: gray;
		position: absolute;
		top: 20%;
		left: 30%;
		padding: 20px;
		z-index: 100;
	}
</style>

</head>
<body>
<h3>Reply REST + Ajax Test</h3>

<div id = "modifyDiv" style="display:none;">
	<span class='title-dialog'></span>번 댓글
	<div>		
		<input type="text" id="reContent" size="60">
	</div>
	
	<div align="center">
		 <button type="button" id="reModifyBtn">수정</button>
		 <button type="button" id="reDelBtn">삭제</button>
		 <button type="button" id="closeBtn">닫기</button>
	</div> 
</div>

<div>
	<div>
		작성자: <input type="text" name="userId" id="writer" />
	</div>
	<div>
		&nbsp;&nbsp;&nbsp;댓글 : <input type="text" size="50" name="reBoardContent" id="reContent" />
	</div>
	<div>
		<button id="submitBtn">댓글작성</button>
	</div>
</div>


<h4>댓글 리스트</h4>
<ul id="reply">

</ul>

<script type="text/javascript">
var boardNo = 2;
	
// 댓글 전체 화면에 뿌리기
function replyList() {
	$.getJSON("/replies/selectAll/"+boardNo, function(data) {
		console.log(data.length);
		console.log(data);
		
		var str = "";
		
		$(data).each(function() {
			
			 str += "<li data-reBoardNo='" + this.reBoardNo + "' class='replyList'>"
             +   "<p class='reBoardContent'>" + this.reBoardContent + "</p>"
             +   "<p class='userId'>" + this.userId + "</p>"
             + "<button>수정</button>"
             + "</li>"
             + "<hr/>";
		});
		
		$("#reply").html(str);
	});
} // replyList()

	// 댓글 쓴 뒤 수정버튼 클릭 
	$("#reply").on("click", ".replyList button", function() {
		
	});
	
	
	// 댓글 쓰기 처리
	$("#submitBtn").on("click", function() {
		var replyWriter = $("#writer").val();
		var replyContent = $("#reContent").val();
		
		$.ajax({
			type: 'post',
			url: '/replies',
			headers:{
				"Content-Type" : "application/json",
				"X-HTTP-Method-Override" : "POST"
			},
			dataType : 'text',
			data :JSON.stringify({
			boardNo : boardNo,
			userId : replyWriter,
			reBoardContent : replyContent
			}),
			
			success : function(result){
				if (result == 'writeSuccess'){
					// alert("댓글 등록 성공!!!")
					replyList();				
				}
			} 
		}); // ajax
	}); // #submitBtn
	
</script>	
</body>
</html>