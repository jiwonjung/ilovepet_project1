<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/shopping_sidebar.jsp"%>
<!--main content start-->
<section id="main-content">
  <section class="wrapper" style="margin-top:0;">
	<div class="row mt" style="margin-top:0;">
      <div class="col-lg-12">
      
		<div class="row mt" style="display:inline-block;">
			<c:forEach items="${list }" var="product">
			<!-- 상품 리스트 -->
			<div class="col-md-4 col-sm-4 mb">
				<div class="white-panel pn">
			<a href="/petmall/read?productNo=${product.productNo }">
					<div class="row">
						<div class="col-sm-6 col-xs-6 goleft">
							<p>No.${product.productNo }</p>
						</div>
						<div class="col-sm-6 col-xs-6"></div>
                    </div>
					<div class="centered">
						<img src="<c:url value="/resources/assets/img/product.png" />" alt="이미지 준비중입니다." width="150" style="margin-top: 10px;">
					</div>
					<div class="row">
						<div class="col-md-12">
							<h3>${product.productName }</h3>
							<h4><b><fmt:formatNumber pattern="###,###,###" value="${product.productPrice }" />원</b></h4>
						</div>
					</div>
					
			</a>
				</div>
			</div><!-- col-md-4 -->
			</c:forEach>

		</div> <!-- /row mt -->
	</div> <!-- main-chart -->
</div><!-- /row -->
</section>
</section>
<%@ include file="../include/footer.jsp"%>
</body></html>