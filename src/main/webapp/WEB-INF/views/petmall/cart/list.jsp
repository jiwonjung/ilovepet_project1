<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/left_sidebar.jsp"%>

<style>
.allCheck { float:left; width:200px; }
.allCheck input { width:16px; height:16px; }
.allCheck label { margin-left:10px; }
.delBtn { float:right; width:300px; text-align:right; }

.checkBox { float:left; width:30px; }
.checkBox input { width:16px; height:16px; }

 section#content ul li { margin:10px 0; }
 section#content ul li img { width:200px; height:200px; }
 section#content ul li::after { content:""; display:block; clear:both; }
 section#content div.thumb { float:left; width:250px; clear:both;}
 section#content div.productInfo { float:right; width:calc(100% - 270px); }
 section#content div.productInfo { font-size:20px; line-height:2; }
 section#content div.productInfo span { display:inline-block; width:100px; font-weight:bold; margin-right:10px; }
 section#content div.productInfo .delete { text-align:right; }
 section#content div.productInfo .delete button { font-size:22px;
            padding:5px 10px; border:1px solid #eee; background:#eee;}
 
 .listResult { padding:20px; background:white; }
.listResult .sum { float:left; width:45%; font-size:22px; }

.listResult .orderOpne { float:right; width:45%; text-align:right; }
.listResult::after { content:""; display:block; clear:both; }


#sample3_address { width: 180px;}
#sample3_detailAddress { width: 200px;}
#sample3_extraAddress { display:none; }
</style>
<body>
<section id="main-content">
  <section class="wrapper" style="margin-top: 0px;">
	<div class="row mt" style="margin-top: 0;">
      <div class="col-lg-12">
      <!--  <div class="form-panel" style="height:1600px;"> -->
 		 
 		 <h3 class="mb" style="text-align:center;"><i class='fas fa-shopping-cart' style='font-size:24px'></i> 장바구니</h3>

<c:choose>
	<c:when test="${map.count == 1 }">
		장바구니가 비었습니다.	
	</c:when>
	<c:otherwise>
		<section id="content">
			 <ul>
			  <li>
			  	<div class="allCheck">
			  		<input type="checkbox" name="allCheck" id="allCheck" /><label for="allCheck"><h5>모두 선택</h5></label>
			  		
			  		<script>
					$("#allCheck").click(function(){
						 var chk = $("#allCheck").prop("checked");
						 if(chk) {
						 	$(".checkbox").prop("checked", true);
						 } else {
						 	$(".checkbox").prop("checked", false);
						 }
					});
					</script>
			  	</div>
			  	<div class="delBtn">
			  		<button type="button" class="btn btn-warning" id="selectedDelete_btn">선택 삭제</button>
			  		<script>
			  			$("#selectedDelete_btn").click(function() {
							var confirm_val = confirm("선택한 상품들을 장바구니에서 삭제하시겠습니까?");
							if(confirm_val) {
								var checkArr = new Array();
								$("input[class='checkbox']:checked").each(function() {
									checkArr.push($(this).attr("data-cartNo")); // 개별 선택된 체크박스들을 배열 변수 checkArr에 저장
								});
								
								$.ajax({
									url : "/petmall/cart/delete",
									type : "post",
									data : { checkbox : checkArr },
									success : function(result) {
										if(result == 1) {
											alert('정상처리되었습니다.');
											location.href = "/petmall/cart/list";
										} else {
											alert("장바구니 삭제에 실패했습니다.");
										}
									}
								});
							}
			  			});
			  		</script>
			  	</div>
			  </li> <hr/>
			  
			  <c:set var="sum" value="0" />
			  
			  <c:forEach items="${list}" var="cartList">
			  <li>
			  	   <div class="checkBox">
			  	   	<input type="checkbox" name="checkbox" class="checkbox"  data-cartNo="${cartList.cartNo }" />
			  	   	<script>
			  	   		$(".checkbox").click(function() {
			  	   			$("#allCheck").prop("checked", false);
			  	   		});
			  	   	</script>
			  	   	
			  	   </div>
				   <div class="thumb">
				   	<img src="${cartList.productUrl}" />
				   </div>
				   <div class="productInfo">
				   		<p>
					    	<span>상품명 : </span>${cartList.productName}<br />
					     	<span>개당 가격 : </span><fmt:formatNumber pattern="###,###,###" value="${cartList.productPrice}" /> 원<br />
					     	<span>구입 수량 : </span>${cartList.amount} 개<br />
					     	<span>최종 가격 : </span><fmt:formatNumber pattern="###,###,###" value="${cartList.productPrice * cartList.amount}" /> 원
					    </p>
				    	<div class="delete">
				    		<button type="button" class="btn ${cartList.cartNo} deleteCart" data-cartNo="${cartList.cartNo }">삭제</button>
				    		<script>
								$('.deleteCart').click(function() {
									var confirm_val = confirm("해당 상품을 정말로 장바구니에서 삭제하시겠습니까?");
									if(confirm_val) {
										var checkArr = new Array();
										checkArr.push($(this).attr("data-cartNo"));
										
										$.ajax({
											url : "/petmall/cart/delete",
											type : "post",
											data : {checkbox : checkArr},
											success : function(result) {
												if(result == 1) {
													location.href = "/petmall/cart/list";
												} else {
													alert("장바구니 삭제에 실패했습니다.")
												}
													
											}
										});
									} else {
										
									}
								});				    		
				    		</script>
				    	</div>    
				   </div>
			  </li>
			  <hr/>
			  
			  <%-- 반복할 때마다 sum에 상품 가격(productPrice)*상품 갯수(amount)만큼을 더함 --%>
			  <c:set var="sum" value="${sum + (cartList.productPrice * cartList.amount) }" />
			  
			  </c:forEach>
			 </ul>
			
			 <div class="listResult">
				 <div class="sum">
				  <b>총 합계 : <fmt:formatNumber pattern="###,###,###" value="${sum}" />원</b>
				 </div>
				 <div class="orderOpne">
				
				  <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">주문 정보 입력</button>  
				 </div>
			</div>
			
						<!-- 주문 정보 압력 버튼클릭 시 뜨는  Modal창 -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						        <h4 class="modal-title" id="myModalLabel">주문 정보 입력</h4>
						      </div>
						      <div class="modal-body">
						        <form class="form-horizontal style-form" role="form" method="post" autocomplete="off" action="/petmall/cart/order">
									<input type="hidden" name="orderAmount" value="${sum }" />
									<div class="form-group">
										<label class="col-sm-3 col-sm-3 control-label">수령인</label>
										<div class="col-sm-9">
											<input type="text" name="orderReceiver" id="orderReceiver" required="required" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 col-sm-3 control-label" for="orderPhone">수령인 연락처</label>
										<div class="col-sm-9">
											<input type="text" name="orderPhone" id="orderPhone" required="required" />
										</div>
									</div>
									<!-- <div class="inputArea">
										<label for="">우편번호</label>
										<input type="text" name="userAddcode" id="userAddcode" required="required" />
									</div>
									<div class="inputArea">
										<label for="">1차주소</label>
										<input type="text" name="userAddress1" id="userAddress1" required="required" />
									</div>
									<div class="inputArea">
										<label for="">2차주소</label>
										<input type="text" name="userAddress2" id="userAddress2" required="required" />
									</div> -->
									<div class="form-group">
										<label class="col-sm-3 col-sm-3 control-label">우편번호</label>
										<div class="col-sm-9">
											<input type="text" id="sample3_postcode" placeholder="우편번호">
											<input type="button" onclick="sample3_execDaumPostcode()" value="우편번호 찾기"><br>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 col-sm-3 control-label">주소</label>
										<div class="col-sm-9">
											<input type="text" name="userAddcode" id="sample3_address" placeholder="주소">
											<input type="text" name="userAddress1" id="sample3_detailAddress" placeholder="상세주소">
											<input type="text"  name="userAddress2" id="sample3_extraAddress" placeholder="참고항목">
										</div>
									</div> <!-- 주소 form-group -->
											<div id="wrap" style="display:none;border:1px solid;width:420px;height:280px;margin:5px 0; margin-left:80px; position:relative">
												<img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
											</div>
										<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
										<script>
										    // 우편번호 찾기 찾기 화면을 넣을 element
										    var element_wrap = document.getElementById('wrap');
										    function foldDaumPostcode() {
										        // iframe을 넣은 element를 안보이게 한다.
										        element_wrap.style.display = 'none';
										    }
										    function sample3_execDaumPostcode() {
										        // 현재 scroll 위치를 저장해놓는다.
										        var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
										        new daum.Postcode({
										            oncomplete: function(data) {
										                // 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
										                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
										                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
										                var addr = ''; // 주소 변수
										                var extraAddr = ''; // 참고항목 변수
										                //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
										                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
										                    addr = data.roadAddress;
										                } else { // 사용자가 지번 주소를 선택했을 경우(J)
										                    addr = data.jibunAddress;
										                }
										                // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
										                if(data.userSelectedType === 'R'){
										                    // 법정동명이 있을 경우 추가한다. (법정리는 제외)
										                    // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
										                    if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
										                        extraAddr += data.bname;
										                    }
										                    // 건물명이 있고, 공동주택일 경우 추가한다.
										                    if(data.buildingName !== '' && data.apartment === 'Y'){
										                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
										                    }
										                    // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
										                    if(extraAddr !== ''){
										                        extraAddr = ' (' + extraAddr + ')';
										                    }
										                    // 조합된 참고항목을 해당 필드에 넣는다.
										                    document.getElementById("sample3_extraAddress").value = extraAddr;
										                
										                } else {
										                    document.getElementById("sample3_extraAddress").value = '';
										                }
										                // 우편번호와 주소 정보를 해당 필드에 넣는다.
										                document.getElementById('sample3_postcode').value = data.zonecode;
										                document.getElementById("sample3_address").value = addr;
										                // 커서를 상세주소 필드로 이동한다.
										                document.getElementById("sample3_detailAddress").focus();
										                // iframe을 넣은 element를 안보이게 한다.
										                // (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.)
										                element_wrap.style.display = 'none';
										                // 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
										                document.body.scrollTop = currentScroll;
										            },
										            // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
										            onresize : function(size) {
										                element_wrap.style.height = size.height+'px';
										            },
										            width : '100%',
										            height : '100%'
										        }).embed(element_wrap);
										        // iframe을 넣은 element를 보이게 한다.
										        element_wrap.style.display = 'block';
										    }
										</script>
									
									
								</form>
						      </div>
						      <div class="modal-footer">
						        <button type="submit" class="btn btn-primary" id="btn_order">주문하기</button>
						        <button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
						        
						        <script>
						        		var formObj = $("form[role='form']");
						        	$("#btn_order").on("click", function() {
						        		var result = confirm("입력하신 정보로 주문을 진행하시겠습니까?");
						    			if(result){
						    				formObj.submit();
						    			} else {
						    				alert('취소를 선택하셨습니다.');
						    				window.location.reload();
						    			}
						        	});
						        </script>
						      </div> <!-- modal-footer  -->
						    </div>
						  </div>
						</div>      				
			
			
			
			
			
			
		</section>
	</c:otherwise>
</c:choose>
	
	 <!--  </div>  form panel -->
	</div>  <!-- row mt -->
   </div>  <!--col-lg-12 -->
 </section>  <!-- wrapper-->
</section>  <!-- main content -->
<%@ include file="../../include/footer.jsp"%>
</body></html>