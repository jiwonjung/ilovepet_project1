<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/left_sidebar.jsp"%>
<style>

 div#container_box ul li { border:5px solid #eee; padding:10px 20px; margin-bottom:20px; }
 div#container_box .orderList span { font-size:20px; font-weight:bold; display:inline-block; width:90px; margin-right:10px; }
</style>

<body>
<section id="main-content">
  <section class="wrapper" style="margin-top:0;">
	<div class="row mt" style="margin-top:0;">
      <div class="col-lg-12">
       <div class="form-panel" style="min-height:400px;">
 		 
 		 <h3 class="mb" style="text-align:center;">구매내역</h3>
 		 		
                       <c:forEach items="${orderList}" var="orderList">
                       <hr/>
                       <div class="content-panel" style="min-height:180px;">
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">주문번호</label>
                              <div class="col-lg-10">
                                  <p><a href="/petmall/cart/orderView?orderNo=${orderList.orderNo}">${orderList.orderNo}</a></p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">수령인</label>
                              <div class="col-lg-10">
                                  <p>${orderList.orderReceiver}</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">주소</label>
                              <div class="col-lg-10">
                                  <p>(${orderList.userAddcode}) ${orderList.userAddress1} ${orderList.userAddress2}</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">가격</label>
                              <div class="col-lg-10">
                                  <p><fmt:formatNumber pattern="###,###,###" value="${orderList.orderAmount}" /> 원</p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">주문한 날짜</label>
                              <div class="col-lg-10">
                                  <p><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${orderList.orderdate}"/></p>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-lg-2 col-sm-2 control-label">상태</label>
                              <div class="col-lg-10">
                                  <p>${orderList.delivery}</a></p>
                              </div>
                          </div>
                       </div>
                       </c:forEach>
	
	  </div>  <!-- main-content -->
	</div>  <!-- wrapper -->
   </div>  <!-- row mt -->
 </section>  <!-- col-lg-12 -->
</section>  <!-- form-panel -->
<script>
	var result= '${result}';
	if(result == 'orderSuccess') {
		alert('해당 상품을 성공적으로 주문완료했습니다.');
	}
</script>
<%@ include file="../../include/footer.jsp"%>
</body></html>