<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/left_sidebar.jsp"%>
<style>
 .orderInfo { border:5px solid #eee; padding:10px 20px; margin:20px 0;}
 .orderInfo .specificReceipt { font-size:10px; font-weight:bold; display:inline-block; width:90px; }
 .orderInfo .totalReceipt { font-size:20px; font-weight:bold; display:inline-block; width:90px; }
 
 .orderView li { margin-bottom:20px; padding-bottom:20px; border-bottom:1px solid #999; }
 .orderView li::after { content:""; display:block; clear:both; }
 
 .thumb { float:left; width:200px; }
 .thumb img { width:130px; height:130px; }
 .gdsInfo { float:right; width:calc(100% - 220px); line-height:2; }
 .gdsInfo span { font-size:20px; font-weight:bold; display:inline-block; width:100px; margin-right:10px; }
</style>
<body>
<section id="main-content">
  <section class="wrapper" style="margin-top:0;">
	<div class="row mt" style="margin-top:0;">
      <div class="col-lg-12">
       <div class="form-panel">
 			
	<section id="content">
	
	 <div class="orderInfo">
	  <c:forEach items="${orderView}" var="orderView" varStatus="status" >
	   
	   <c:if test="${status.first}">
	    <p><span class="totalReceipt">수령인</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${orderView.orderReceiver}</p>
	    <p><span class="totalReceipt">주소</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(${orderView.userAddcode}) ${orderView.userAddress1} ${orderView.userAddress2}</p>
	    <p><span class="totalReceipt">최종 가격</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:formatNumber pattern="###,###,###" value="${orderView.orderAmount}" /> 원</p>
	    <p><span class="totalReceipt">상태</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${orderView.delivery}</p>
	   </c:if>
	  </c:forEach>
	 </div>
	 <ul class="orderView">
	  <c:forEach items="${orderView}" var="orderView">     
	  <li>
	    <c:if test="${orderView.productThumbImg == null }">
			<div class="thumb">
				<img src="<c:url value="/resources/custom/img/none.png" />" >
			</div>
		</c:if>
	 	<c:if test="${orderView.productThumbImg != null }">
		   <div class="thumb">
		    <img src="${orderView.productThumbImg}" />
		   </div>
	    </c:if>
	   <div class="gdsInfo">
	    <p>
	    	
	     <span class="specificReceipt" style="font-size:12px;">상품명</span>&nbsp;&nbsp; ${orderView.productName}<br />
	     <span class="specificReceipt" style="font-size:12px;">개당 가격</span>&nbsp;&nbsp;<fmt:formatNumber pattern="###,###,###" value="${orderView.productPrice}" /> 원<br />
	     <span class="specificReceipt" style="font-size:12px;">구입 수량</span>&nbsp;&nbsp;${orderView.amount} 개<br />
	     <span class="specificReceipt" style="font-size:12px;">가격</span>&nbsp;&nbsp;<fmt:formatNumber pattern="###,###,###" value="${orderView.productPrice * orderView.amount}" /> 원                  
	    </p>
	   </div>
	  </li>     
	  </c:forEach>
	 </ul>
	</section>

	
	  </div>  <!-- main-content -->
	</div>  <!-- wrapper -->
   </div>  <!-- row mt -->
 </section>  <!-- col-lg-12 -->
</section>  <!-- form-panel -->
<%@ include file="../../include/footer.jsp"%>
</body></html>