<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<script>
$(document).ready(function() {

	 
	function product_insert(){
		alert('test');
	    // 태그를 name으로 조회할 경우
	    //var product_name=document.form1.product_name.value;
	    // 태그를 id로 조회할 경우
	    var productName=$("#productName").val();
	    var productPrice=$("#productPrice").val();
	    var description=$("#description").val();
	    if(productName==""){ //빈값이면
	        alert("상품이름을 입력하세요"); 
	        $("#productName").focus(); //입력포커스 이동
	        return; //함수 종료, 폼 데이터를 제출하지 않음
	    }
	    if(productPrice==""){
	        alert("가격을 입력하세요");
	        $("#productPrice").focus();
	        return;
	    }
	}

});
</script>
<br/><br/>
<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt" style="margin-top: 0;">
          		<div class="col-lg-12" style="width:100%; text-align:center;">
                  <div class="form-panel" style="width:60%; display:inline-block;">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i>   상품수정하기</h4>
                      <form name="form" class="form-horizontal style-form" method="post" enctype="multipart/form-data">
                          <input type="hidden" name="page" value="${sCriteria.page}" /> <!-- 수정처리 컨트롤러로 넘길 때 수정처리에 필요한 정보는: bid, page, numPerPage 이므로, hidden으로  넘김 -->
						  <input type="hidden" name="numPerPage" value="${sCriteria.numPerPage }" />
						  <input type="hidden" name="searchingCategory" value="${sCriteria.searchingCategory}" />
						  <input type="hidden" name="keyword" value="${sCriteria.keyword}" />
						  
                          <div class="form-group">
                         	  <label class="col-sm-2 col-sm-2 control-label">상품카테고리</label>
                         	   <div class="col-sm-10">
	                              <label>상품 1차 분류 </label>
	                                  <select class="category1">
	                                  	<option value="">전체</option>
	                                  </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                              <label>상품 2차 분류 </label>
	                                  <select class="category2" name="cateCode">
	                                  	<option value="">전체</option>
	                                  </select>
                              </div>
                          </div>	
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">상품명</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="productName" id="productName" value="${productVO.productName}" >
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">판매가격</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="productPrice" id="productPrice" value="${productVO.productPrice}">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">상품수량</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="productStock" id="productStock" value="${productVO.productStock}">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">상품 상세내용</label>
                              <div class="col-sm-10">
                                  <textarea class="form-control" rows="6" name="productDetails" id="description">${productVO.productDetails}</textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">상품 이미지</label>
                              <div class="col-sm-10">
                                  <input type="file"  class="form-control" id="productUrl" name="file">
                              	  <div class="select_img">
                              	  	<img src="${productVO.productUrl }" style="margin: 20px 0; width:540px;"/>
                              	  	<input type="hidden" name="productUrl" value="${productVO.productUrl }" />
                              	  	<input type="hidden" name="productThumbImg" value="${productVO.productThumbImg }" />
                              	  </div>
                              </div>    
                                  <script>
								  $("#productUrl").change(function(){
								   if(this.files && this.files[0]) {
								    var reader = new FileReader;
								    reader.onload = function(data) {
								     $(".select_img img").attr("src", data.target.result).width(500);        
								    }
								    reader.readAsDataURL(this.files[0]);
								   }
								  });
								 </script>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">등록일</label>
                              <div class="col-sm-10">
                                  <p class="form-control-static"><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${productVO.regdate }"/> 작성 &nbsp;  &nbsp;  &nbsp; 
                               <fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${productVO.moddate }"/> 수정됨</p><br/>
                              </div>
                          </div>
                         
                          <div class="form-group">
                          	<div class="col-sm-12" align="center">
                          		<button type="submit" id="btn_modify" class="btn btn-success">수정하기</button>
                          	</div>
                          </div>
                          
                      </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
<script>
$(document).ready(function() {
	var formObj = $("form[role='form']");
	
	//수정버튼 클릭
	$("#btn_modify").on("click", function(){
		var result = confirm("해당 게시글을 수정하시겠습니까?");
		if(result) {
			formObj.attr("action", "/petmall/search/update");
			formObj.submit(); 
			
		}
	});
	
});
/* 1차, 2차 카테고리  */
var select_cateCode = '${productVO.cateCode}';
var select_cateCodeRef = '${productVO.cateCodeRef}';
var select_cateName = '${productVO.cateName}';

if(select_cateCodeRef != null && select_cateCodeRef != '') {
 $(".category1").val(select_cateCodeRef);
 $(".category2").val(select_cateCode);
 $(".category2").children().remove();
 $(".category2").append("<option value='"
       + select_cateCode + "'>" + select_cateName + "</option>");
} else {
 $(".category1").val(select_cateCode);
 //$(".category2").val(select_cateCode);
 // select_cateCod가 부여되지 않는 현상이 있어서 아래 코드로 대체
 $(".category2").append("<option value='" + select_cateCode + "'selected=' selected'>전체</option>");
}
</script>
<%@ include file="../../include/footer.jsp"%>
  </body>
</html>