<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/shopping_sidebar.jsp"%>
<style>

/* 별점 주기 */

#star a{ text-decoration: none; color: gray; } #star a.on{ color: orange; }

</style>
<!--main content start-->
<section id="main-content" align="center";>
	<section class="wrapper" style="margin-top: 0px;">
		<h3>※ 상품 상세정보 ※</h3>
		
		<form role="form" method="post">	
			<input type="hidden" name="productNo" value="${productVO.productNo }" />
			<input type="hidden" name="page" value="${sCriteria.page}" />
			<input type="hidden" name="numPerPage" value="${sCriteria.numPerPage}" />
			<input type="hidden" name="searchingCategory" value="${sCriteria.searchingCategory}" />
			<input type="hidden" name="keyword" value="${sCriteria.keyword}" />
		</form>
		
		<div class="row mt">
			<div class="col-lg-12" style="width: 100%; text-align: center;">
				<div class="content-panel" style="display: inline-block; width:900px;" >
					<section id="unseen">
						<table class="table table-bordered table-striped table-condensed"
							border="1">
							<tr>
								<c:if test="${productVO.productThumbImg == null }">
								<td><a><img src="<c:url value="/resources/custom/img/none.png" />" width="380" height="330"></a>
								</td>
								</c:if>
								<c:if test="${productVO.productThumbImg != null }">
								<td><a><img src="${productVO.productUrl}"
									width="380" height="330" onclick="fnImgPop(this.src)" style="cursor:pointer;"></a>
								</td>
								</c:if>
								<script type="text/javascript">
								 function fnImgPop(url){
								  var img=new Image();
								  img.src=url;
								  var img_width=img.width;
								  var win_width=img.width+25;
								  var img_height=img.height;
								  var win=img.height+30;
								  var OpenWindow=window.open('','_blank', 'width='+img_width+', height='+img_height+', menubars=no, scrollbars=auto');
								  OpenWindow.document.write("<style>body{margin:0px;}</style><img src='"+url+"' width='"+win_width+"'>");
								 }
								</script>
								<td>
									<table border="1" style="height: 330px; width: 500px;">
										<tr align="center">
											<td style="width:100px;">상품카테고리</td>
											<td>${productVO.cateName }</td>
										</tr>
										<tr align="center">
											<td>상품명</td>
											<td>${productVO.productName }</td>
										</tr>
										<tr align="center">
											<td>상품가격</td>
											<td><fmt:formatNumber value="${productVO.productPrice }" pattern="###,###,###" /></td>
										</tr>
										<tr align="center">
											<td>상품재고</td>
											<td>${productVO.productStock}</td>
										</tr>
										<tr align="center">
											<td>상품설명</td>
											<td>${productVO.productDetails }</td>
										</tr>
										<tr align="center">
											<td colspan="2">
												<form name="form1" id="form1" action="/petmall/cart/insert.do"
													method="post">
													<input type="hidden" name="productNo" id="productNo" value="${productVO.productNo }"> 
													<c:if test="${productVO.productStock != 0 }">
														<%-- <select name="amount" id="amount">
														
															<c:forEach begin="1" end="10" var="i">
																<option value="${i }">${i }</option>
															</c:forEach>
														</select>&nbsp;개 &nbsp;&nbsp; --%>
														<p class="cartStock">
															 <span>구입 수량</span>
															 <button type="button" id="btn_plus" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span></button>
															 <input type="number" class="numBox" min="1" max="${productVO.productStock}" value="1" readonly="readonly"/>
															 <button type="button" id="btn_minus" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-minus"></span></button>
															 
															 <script>
															 // + 버튼 누르면 1씩 커지기
															  $("#btn_plus").click(function(){
																   var num = $(".numBox").val();
																   var plusNum = Number(num) + 1;
																   
																   if(plusNum > ${productVO.productStock}) {
																    $(".numBox").val(num);
																   } else {
																    $(".numBox").val(plusNum);          
																   }
															  });
															  // - 버튼 누르면 1씩 작아지기
															  $("#btn_minus").click(function(){
																   var num = $(".numBox").val();
																   var minusNum = Number(num) - 1;
															   
															   if(minusNum <= 0) {
															   	 	$(".numBox").val(num);
															   } else {
															    	$(".numBox").val(minusNum);          
															   }
															  });
															 </script>
														 
														</p>
														
														<button id="btn_cart" class="btn btn-warning btn-sm" onclick="return false;">장바구니에 담기</button>
														<script>
														// 장바구니 버튼 클릭
														$("#btn_cart").on("click", function() {
															var productNo = $('#productNo').val();
															var amount = $('.numBox').val();
															
															var data = {
																	productNo : productNo,
																	amount : amount
															};
															
															$.ajax({
																url : "/petmall/cart/insert",
																type : "post",
																data : data,
																success : function(result) {
																	if(result == 1) {
																		alert('카트에 해당 상품을 성공적으로 담았습니다.');
																		$("#amount").val("1");
																		
																		var q_result = confirm('장바구니 함으로 이동하시겠습니까?');
																		if(q_result) {
																			location.href='/petmall/cart/list';
																		} else {
																			window.location.reload();
																		}
																		
																	} else {
																		alert('로그인 후 장바구니 이용이 가능합니다.');
																		$("#amount").val("1");
																		window.location.href='/user/login';
																	}
																},
																error : function() {
																	alert('카트에 해당 상품 담기에 실패했습니다.');
																}
															});
														}); // #btn_cart
														</script>
													
													</c:if>
												</form>
												<c:if test="${productVO.productStock == 0 }">
														<h5 style="color:red;"><b>해당 상품은 현재 품절입니다.</b></h5>
												</c:if>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</section>
					<p style="text-align:left; margin-left:40px;">※상품사진을 클릭하시면 큰 화면으로 확인 가능합니다!</p>
					<button type="submit" id="btn_list" class="btn btn-primary">목록으로</button>
					<c:if test="${login != null && login.isAdmin == 1}">
						<button id="btn_modify" class="btn btn-success">수정하기</button>&nbsp;
						<button id="btn_delete" class="btn btn-danger">삭제하기</button>&nbsp;
					</c:if>
					
					<hr />
					
					<!-- 댓글영역  -->
					<h4>♥ REVIEW 상품후기 ♥</h4>
					<table class="table table-bordered table-striped table-condensed">
                              <thead>
	                              <tr>
									<th>리뷰번호</th>
									<th>리뷰내용</th>
									<th>별점</th>
									<th>작성자</th>
									<th>작성일</th>
								</tr>
                              </thead>
                              <tbody id="reviewList">
                              <!-- 상품평 목록 -->
                              </tbody>
                          </table>
                          <div id="ifNoReview"></div>
                          <hr	/>
                           <c:if test="${login == null }">
								<div class="form-group">
									<a href="/user/login" class="btn btn-default btn-block" role="button">
		                                <i class="fa fa-edit"></i> 상품 후기를 남기시려면 로그인 후 이용해주세요.
		                            </a>
								</div><br/>
							</c:if>
							<!-- 새로운 상품평 등록  -->
							<c:if test="${login != null}">
								<div class="form-group">
								
									<div class="col-sm-1">
									 <input type="text" name="userId" id="reviewWriter" value="${login.userId}" readonly="readonly" disabled />
									</div>&nbsp;&nbsp;&nbsp;
									<span><p id="star"> <!-- 부모 --> 
									<a href="#" value="1">★</a> <!-- 자식들--> 
									<a href="#" value="2">★</a> 
									<a href="#" value="3">★</a> 
									<a href="#" value="4">★</a> 
									<a href="#" value="5">★</a> 
									</p></span>
									
									<div class="col-sm-10">
										 <textarea class="form-control" name="reBoardContent" rows="4" id="reviewContent" placeholder="상품후기를 작성해주세요."></textarea>
									</div>
								</div><br/>
								<button id="btn_reviewInsert" type="submit" class="btn btn-info"><i class='fas fa-pen-alt'></i>&nbsp;&nbsp;등록</button><br/>
							</c:if>
							<br style="">
							<!-- 페이징 번호 -->
							<div class="showback" align="center">
								<div class="btn-group" id="pagingReviewList">
									<!-- 1,2,3,4,5,6,7,8,9,10 >>  버튼들 그리기 -->
								</div>
							</div>

				</div>
				<!-- /content-panel -->


			</div>
			<!-- /col-lg-4 -->

		</div>
		<!-- /row -->


	</section>
</section>


		<!-- Modal 창 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">리뷰 내용 보기</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="form-group">
                    <label for="reviewNo">리뷰 번호</label>  <!-- for="replNo" 은 id로 쓰임  -->
                    <input class="form-control" id="modal_reviewNo" name="reviewNo" readonly>
                </div>
		      	<div class="form-group">
                    <label for="reviewContent">리뷰 내용</label>  <!-- for="replNo" 은 id로 쓰임  -->
                    <input class="form-control" id="modal_reviewContent" name="reviewContent" readonly>
                </div>
		      	<div class="form-group">
                    <label for="reviewScore">리뷰 점수</label>  <!-- for="replNo" 은 id로 쓰임  -->
                    <input class="form-control" id="modal_reviewScore" name="reviewScore" readonly>
                </div>
		      	<div class="form-group">
                    <label for="userId">작성자</label>  <!-- for="replNo" 은 id로 쓰임  -->
                    <input class="form-control" id="modal_userId" name="userId" readonly>
                </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
		        <c:if test='${login.userId != null}'>
		        <button type="button" class="btn btn-danger modalReviewDeleteBtn"><i class='fas fa-trash-alt'></i> &nbsp; &nbsp; 삭제</button>
		      	</c:if>
		      </div>
		    </div>
		  </div>
		</div>      	
<script>
$(document).ready(function(){
	var formObj = $("form[role='form']");
	
	// 수정버튼 클릭
	$("#btn_modify").on("click", function(){
		formObj.attr("action", "/petmall/search/update");
		formObj.attr("method", "get"); 
		formObj.submit(); 
	});
	
	// 삭제버튼 클릭
	$("#btn_delete").on("click", function(){
		var result = confirm("정말로 해당 상품글을 삭제하시겠습니까?");
		if(result){
			formObj.attr("action", "/petmall/search/delete");  
			formObj.submit();
		}
	});
	
	// 목록가기버튼 클릭
	$("#btn_list").on("click", function(){
		formObj.attr("action", "/petmall/search/list");  
		formObj.attr("method", "get"); 
		formObj.submit();
	});
	
	
/* 상품후기 영역 */
	
	var productNo = "${productVO.productNo}"  // 현재 상품 번호
	var page = 1;
	
	getPagingReviewList(page);  // 전체 상품후기 리스트 (페이징 적용)
	//reviewList(); // 전체 상품후기 리스트 (페이징X)
	
	// 전체 리스트(페이징x)
	function reviewList() {
		$.getJSON("/review/selectAll/" + productNo, function(data) {
			console.log("data length: " + data.length);
			console.log(data);
	
			var str = "";
			$(data).each(function() {
			str += "<tr>"	
				+ "<td>" + this.reviewNo + "</td>"
				+ "<td>" + this.reviewContent
				+ 		"<div class='pull-right hidden-phone'>"
				+			"<button class='btn btn-primary btn-xs'><i class='fas fa-pen'></i></button>"
				+			"<button class='btn btn-danger btn-xs'><i class='fas fa-trash-alt'></i></button>"
				+		"</div>"
				+ "</td>"
				+ "<td>" + this.reviewScore + "</td>"
				+ "<td>" + this.userId + "</td>"
				+ "<td>" + this.regdate + "</td>"
				+ "</tr>";
			});
			$("#reviewList").html(str);
		}); // $getJSON
	}// reviewList()
	
	// 전체 리스트 (페이징o)
	function getPagingReviewList(page) {
		$.getJSON("/review/"+productNo+"/"+page, function(data) {
			console.log(data.pagingMaker.totalData);
			console.log(data);
			
			var str = "";
			var str2 = "";
			if(data.pagingMaker.totalData >= 1) {
				$(data.reList).each(function() {
					var reviewScore = "";
					if(this.reviewScore == 5) {
						reviewScore += "<i class='fas fa-star' style='color:orange'></i>"
									+ "<i class='fas fa-star' style='color:orange'></i>"
									+ "<i class='fas fa-star' style='color:orange'></i>"
									+ "<i class='fas fa-star' style='color:orange'></i>"
									+ "<i class='fas fa-star' style='color:orange'></i>";
					} else if (this.reviewScore == 4) {
						reviewScore += "<i class='fas fa-star' style='color:orange'></i>"
							+ "<i class='fas fa-star' style='color:orange'></i>"
							+ "<i class='fas fa-star' style='color:orange'></i>"
							+ "<i class='fas fa-star' style='color:orange'></i>";
					} else if (this.reviewScore == 3) {
						reviewScore += "<i class='fas fa-star' style='color:orange'></i>"
							+ "<i class='fas fa-star' style='color:orange'></i>"
							+ "<i class='fas fa-star' style='color:orange'></i>";
					} else if (this.reviewScore == 2) {
						reviewScore += "<i class='fas fa-star' style='color:orange'></i>"
							+ "<i class='fas fa-star' style='color:orange'></i>";
					} else {
						reviewScore += "<i class='fas fa-star' style='color:orange'></i>";
					}
				
					str += "<tr class='reviewList'>"	
						+ "<td class='reviewNo'>" + this.reviewNo + "</td>"
						+ "<td class='reviewContent'>" 
						+ "<a data-toggle='modal' data-target='#myModal'>" + this.reviewContent
						+ "</a>"
						+ "</td>"
						+ "<td class='reviewScore'>" + reviewScore + "</td>"
						+ "<td class='userId'>" + this.userId + "</td>"
						+ "<td>" + this.regdate + "</td>"
						+ "</tr>";
				});
			} else {
				str2 += "<h5 style='text-align:center;'>해당 게시물은 댓글이 없습니다. 첫 댓글을 남겨주세요.</h5>";
				$("#ifNoReview").html(str2);
			}
			$("#reviewList").html(str);
			
			showPagingNum(data.pagingMaker); // 페이징 버튼들 그리기
		}); // $getJSON
	} // getPagingReviewList(page)
	
	/* 페이징 처리된 리스트 목록 아래에 존재하는 버튼들 */
	function showPagingNum(pagingMaker) {
		var str = "";
		
		// 이전 버튼 활성화
		if(pagingMaker.prev){
			str += '<a href="'+(pagingMaker.startPage-1)+'">'
				+ 		'<button type="button" class="btn btn-default">'
				+ 			'<i class="fa fa-angle-double-left" style="font-size:10px"></i>'
				+ 		'</button>'
				+	'</a>';
		}
		
		/*<a href="#">
		<button type="button" class="btn btn-default">1</button>
									</a>  */
		// 페이지 번호
		for(var i=pagingMaker.startPage, end=pagingMaker.endPage; i <= end; i++){			
			var selectedClass = pagingMaker.cri.page == i ? 'class=btn btn-primary' : 'class=btn btn-default';
			str +="<a href='"+i+"'><button type='button' "+selectedClass+">"+i+"</button></a>";	
		}
		
		// 다음 버튼 활성화
		if(pagingMaker.next){
			str += '<a href="'+(pagingMaker.endPage+1)+'">'
			+ 		'<button type="button" class="btn btn-default">'
			+ 			'<i class="fa fa-angle-double-right" style="font-size:10px"></i>'
			+ 		'</button>'
			+	'</a>';
		}
		
		$("#pagingReviewList").html(str);
	} // showPagingNum(pagingMaker)
	
	/* 목록페이지 번호 클릭 이벤트 */
	$("#pagingReviewList").on("click", "a button", function(e) {
		
		e.preventDefault(); // <a> 태그의 화면전환이 일어나지 않도록 하는 역할을 하는 메소드
		
		rePage = $(this).parent().attr("href");   // this는 버튼, this.parent()는 a 태그 
		
		getPagingReviewList(rePage); // 목록 페이지 호출
	});
	
	// 별 클릭하면 'on'이라는 class부여
	$('#star a').click(function(){ 
		$(this).parent().children("a").removeClass("on"); 
		$(this).addClass("on").prevAll("a").addClass("on"); 
		console.log($(this).attr("value")); 
		var starScore= $(this).attr("value");
	
	
		/* 상품리뷰 쓰고 등록버튼 클릭 시 */
		$("#btn_reviewInsert").on("click", function() {
			var writer = $("#reviewWriter").val();
			var content = $("#reviewContent").val();
			
			var score = starScore;  // 선택한 별점 점수
			
			$.ajax({
				type: 'post',
				url : '/review',
				headers:{
					"Content-Type" : "application/json",
					"X-HTTP-Method-Override" : "POST"
				},
				dataType : 'text',
				data : JSON.stringify({
					productNo : productNo,
					userId : writer,
					reviewContent :	content,
					reviewScore : score,
				}),
				success : function(result) {
					if(result == 'writeSuccess') {
						alert('상품후기를 성공적으로 등록하였습니다.');
						$("#reviewWriter").val("");  // 댓글 작성자 초기화
						$("#reviewContent").val(""); // 댓글 내용 초기화
						$('#star a').parent().children("a").removeClass("on"); // 별점 초기화
						$('html').scrollTop(0);
					}
					getPagingReviewList(page); 
				}
				
			}); // ajax
		});
	});  // 별클릭
	
	/* 댓글 쓴 뒤 수정버튼 클릭 시 모달 창 뜨도록 */
	$("#reviewList").on("click", "a", function() {
		var tr = $(this).closest('tr');
		console.log("tr: " + tr);
		var reviewNo = tr.find(".reviewNo").text();
		var reviewContent = tr.find(".reviewContent").text();
		var reviewScore = tr.find(".reviewScore").children().length;

	if(reviewScore == 5) {
			reviewScore = '★★★★★';
		} else if (reviewScore == 4) {
			reviewScore = '★★★★';
		} else if (reviewScore == 3) {
			reviewScore = '★★★';
		} else if (reviewScore == 2) {
			reviewScore = '★★';
		} else {
			reviewScore = '★';
		}
		 
		var userId = tr.find(".userId").text();
		
		// 해당 댓글의 내용도 함께 나오도록
		$("#modal_reviewNo").val(reviewNo);
		$("#modal_reviewContent").val(reviewContent);
		$("#modal_reviewScore").val(reviewScore);
		$("#modal_userId").val(userId);
		
		// 로그인한 사용자가 자신이 쓴 댓글에는 '삭제버튼'이 보이고, 아니라면 안보이게 함
		var loginId = '${login.userId}';
		if(userId != loginId) {
			$('.modalReviewDeleteBtn').hide();
		} else {
			$('.modalReviewDeleteBtn').show();
		}
		
	});
	
	/* 모달창에서 댓글 삭제처리 */
	$(".modalReviewDeleteBtn").on("click", function() {
		var result = confirm("해당 상품후기를 삭제 하시겠습니까?");
		if(result) {
			
			var reviewNo = $(this).parent().parent().find("#modal_reviewNo").val();
			$.ajax({
				type:'delete',
				url:'/review/'+reviewNo,
				headers :{
					"Content-Type" : "application/json",
					"X-HTTP-Method-Override" : "DELETE"
				},
				dataType: 'text',
				success: function(result) {
					console.log("result: " + result);
					if(result == "deleteSuccess") {
						alert('정상적으로 댓글을 삭제하였습니다.');
						$("#myModal").modal("hide");
						getPagingReviewList(page); // 댓글 목록 갱신 
					}
				}
			}); // ajax
		}
	}); // .modalReviewDeleteBtn
	
	
});
</script>

<%@ include file="../../include/footer.jsp"%>
</body>
</html>