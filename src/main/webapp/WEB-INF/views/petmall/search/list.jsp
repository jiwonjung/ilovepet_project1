<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/shopping_sidebar.jsp"%>
<section id="main-content">
  <section class="wrapper" style="margin-top: 0;">
	<div class="row mt">
      <div class="col-lg-12">
       <div class="form-panel" style="min-height:400px;">
			<!-- 검색창 -->
            <form class="form-inline" role="form" align="center">
               <span class="col-md-12"> 
                    <select class="form-control" name="searchingCategory">
				  <option value="N" 
				  	<c:out value="${sCriteria.searchingCategory == null? 'selected':''}"/>>----선택하세요----</option>
				  <option value="T"
				    <c:out value="${sCriteria.searchingCategory == 'T'? 'selected' : ''}"/>>제목</option>
				  <option value="C"
				    <c:out value="${sCriteria.searchingCategory == 'C'? 'selected' : ''}"/>>내용</option>
				  <option value="W"
				    <c:out value="${sCriteria.searchingCategory == 'W'? 'selected': ''}"/>>작성자</option>
				  <option value="TC"
				    <c:out value="${sCriteria.searchingCategory == 'TC'? 'selected' :''}"/>>제목+내용</option>
				  <option value="CW"
				    <c:out value="${sCriteria.searchingCategory =='CW'? 'selected':''}"/>>내용+작성자</option>
				  <option value="TCW"
				    <c:out value="${sCriteria.searchingCategory =='TCW'? 'selected':''}"/>>제목+내용+작성자</option>
				</select>
                          <input type="text" class="form-control" name="keyword" id="searchword" value="${sCriteria.keyword}"/>
                       <button type="submit" class="btn btn-theme" id="searchBtn">검색</button> 
				</span>
            </form> <br/><br/><br/>
		
		
			<c:forEach items="${list }" var="product">
			<!-- 상품 리스트 -->
			<div class="col-md-4 col-sm-4 mb">
				<div class="white-panel pn">
			<a href="/petmall/search/read${pagingMaker.makeSearchingURI(pagingMaker.cri.page)}&productNo=${product.productNo }">
					<div class="row">
						<div class="col-sm-6 col-xs-6 goleft">
							<p>No.${product.productNo }</p>
						</div>
						<div class="col-sm-6 col-xs-6"></div>
                    </div>
                    <c:if test="${product.productThumbImg == null }">
						<div class="centered">
							<img src="<c:url value="/resources/custom/img/none.png" />" alt="상품이미지 준비중" style="margin-top: 5px; height: 130px;">
						</div>
					</c:if>
					<c:if test="${product.productThumbImg != null }">
						<div class="centered">
							<img src="${product.productUrl }" width="200px;" style="margin-top: 5px; height: 130px;">
						</div>
					</c:if>
					<div class="row">
						<div class="col-md-12">
							<h5 style="color: black;"><b>${product.productName }</b></h5>
							<h5 style="color: #ff0000;"><b><fmt:formatNumber pattern="###,###,###" value="${product.productPrice }" />원 &nbsp;&nbsp;&nbsp;<span class="badge bg-default"><i class='far fa-comment'></i> ${product.reviewCnt}</span></b></h5>
						</div>
					</div>
					
			</a>
				</div>
			</div><!-- col-md-4 -->
			</c:forEach>
			<br class="clear" style="clear:both;">
			<div class="showback" align="center">
						<div class="btn-group">
						  <c:if test="${pagingMaker.prev }">
						  	<%-- <a href="pagingList?page=${pagingMaker.startPage-1}" > --%>
						  	<a href="list${pagingMaker.makeSearchingURI(pagingMaker.startPage-1)}" >
						  	<button type="button" class="btn btn-default"><i class="fa fa-angle-double-left" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  <c:forEach begin="${pagingMaker.startPage }" end="${pagingMaker.endPage }" var="pageNum">
						  <a href="list${pagingMaker.makeSearchingURI(pageNum)}">
						 	 <button type="button" class="<c:out value="${pagingMaker.cri.page == pageNum?'btn btn-primary':'btn btn-default'}"/>">${pageNum}</button> <!--${pagingMaker.cri.page}은 pagingMaker.getCri()과 PageCriteria의 getPage()와 같음  -->
						  </a>
						  </c:forEach>
						  <c:if test="${pagingMaker.next && pagingMaker.endPage > 0 }">
						  	<a href ="list${pagingMaker.makeSearchingURI(pagingMaker.endPage+1) }">
						  		<button type="button" class="btn btn-default"><i class="fa fa-angle-double-right" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  
						</div>      					
      		</div><!-- /showback -->
		</div> <!-- /row mt -->
	</div> <!-- main-chart -->
</div><!-- /row -->
</section>
</section>
<script>
	var result= '${result}';
	
	if (result == 'insertSuccess') {
		alert('새로운 상품이 정상적으로 등록되었습니다.');
	} else if (result == 'deleteSuccess') {
		alert('해당 상품이 정상적으로 삭제되었습니다.');
	} else if (result == 'updateSuccess') {
		alert('해당 상품의 내용이 정상적으로 수정되었습니다.');
	}
</script>
<%@ include file="../../include/footer.jsp"%>
</body></html>