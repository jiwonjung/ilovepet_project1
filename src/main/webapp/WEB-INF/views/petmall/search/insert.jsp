<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/left_sidebar.jsp"%>
<script>
$(document).ready(function() {
	function product_insert(){
		alert('test');
	    // 태그를 name으로 조회할 경우
	    //var product_name=document.form1.product_name.value;
	    // 태그를 id로 조회할 경우
	    var productName=$("#productName").val();
	    var productPrice=$("#productPrice").val();
	    var description=$("#description").val();
	    if(productName==""){ //빈값이면
	        alert("상품이름을 입력하세요"); 
	        $("#productName").focus(); //입력포커스 이동
	        return; //함수 종료, 폼 데이터를 제출하지 않음
	    }
	    if(productPrice==""){
	        alert("가격을 입력하세요");
	        $("#productPrice").focus();
	        return;
	    }
	
	    //폼 데이터를 받을 주소
	    document.form1.action="/petmall/search/insert";
	    document.form1.method="post";
	    //폼 데이터를 서버에 전송
	    document.form1.submit();
	}

});
</script>
<section id="main-content">
<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt" style="margin-top: 0;">
          		<div class="col-lg-12" style="width:100%; text-align:center;">
                  <div class="form-panel" style="width:80%; height: 940px; margin-top: 0; display:inline-block;">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i>   상품등록하기</h4>
                      <form role="form" name="form1" class="form-horizontal style-form" method="post" enctype="multipart/form-data">
                         <div class="form-group">
                         	  <label class="col-sm-2 col-sm-2 control-label">상품카테고리</label>
                         	   <div class="col-sm-10">
	                              <label>상품 1차 분류 </label>
	                                  <select class="category1" required>
	                                  	<option value="">전체</option>
	                                  </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                              <label>상품 2차 분류 </label>
	                                  <select class="category2" name="cateCode" required>
	                                  	<option value="">전체</option>
	                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">상품명</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="productName" id="productName" required="required">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">판매가격</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="productPrice" id="productPrice" required="required">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">상품수량</label>
                              <div class="col-sm-10">
                                  <input type="text"  class="form-control" name="productStock" id="productStock" required="required">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">상품 상세내용</label>
                              <div class="col-sm-10">
                                  <textarea class="form-control" rows="6" name="productDetails" id="description" required="required"></textarea>
                              	  <!-- <script>
									 var ckeditor_config = {
									   resize_enaleb : false,
									   enterMode : CKEDITOR.ENTER_BR,
									   shiftEnterMode : CKEDITOR.ENTER_P,
									   filebrowserUploadUrl : "/petmall/search/insert"
									 };
									 
									 CKEDITOR.replace("description", ckeditor_config);
								  </script> -->
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">상품 이미지</label>
                                <div class="col-sm-10">
                                  <input type="file" class="form-control" id="productUrl" name="file" required>
                                  <div class="select_img" style="margin: 20px 0 0 50px; text-align: center;"><img src="" /></div>
                                  
                                  <script>
								  $("#productUrl").change(function(){
								   if(this.files && this.files[0]) {
								    var reader = new FileReader;
								    reader.onload = function(data) {
								     $(".select_img img").attr("src", data.target.result).width(300);        
								    }
								    reader.readAsDataURL(this.files[0]);
								   }
								  });
								 </script>
								 <%-- <%=request.getRealPath("/") %> --%>
								</div>
                          </div>
                         
                          <div class="form-group">
                          	<div class="col-sm-12" align="center">
                          		<button type="submit" class="btn btn-success" onclick="javascript:product_insert()">등록하기</button>
                          		<button type="button" id="btn_list" class="btn btn-primary">상품 목록으로</button>
                          	</div>
                          </div>
                          
                      </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
</section>
<script>
	$(document).ready(function(){
		var formObj = $("form[role='form']");
		
		console.log("폼태그입니다..");
		
		// 목록가기버튼 클릭
		$("#btn_list").on("click", function(){
			formObj.attr("action", "/petmall/search/list");  
			formObj.attr("method", "get"); 
			formObj.submit();
		});
		
		
		/* 1차분류 , 2차분류 카테고리 */
			// 컨트롤러에서 데이터 받기
			var jsonData = JSON.parse('${category}'); // ''가 아닌 ""로 하면 에러 발생
			console.log(jsonData);
		
			var cate1Arr = new Array();
			var cate1Obj = new Object();
			
			// 1차 분류 셀렉트 박스에 삽입할 데이터 준비
			for(var i = 0; i < jsonData.length; i++) {
				if(jsonData[i].level == "1") { // jsonData값이 1인 경우에만 cate1Obj에 추가
				  cate1Obj = new Object();  //초기화
				  cate1Obj.cateCode = jsonData[i].cateCode;
				  cate1Obj.cateName = jsonData[i].cateName;
				  cate1Arr.push(cate1Obj);
				}
			}
			
			
			// 1차 분류 셀렉트 박스에 데이터 삽입
			var cate1Select = $("select.category1")

			for(var i = 0; i < cate1Arr.length; i++) {
				cate1Select.append("<option value='" + cate1Arr[i].cateCode + "'>"
			      + cate1Arr[i].cateName + "</option>"); 
			}
			
			
			$(document).on("change", "select.category1", function(){

				 var cate2Arr = new Array();
				 var cate2Obj = new Object();
				 
				 // 2차 분류 셀렉트 박스에 삽입할 데이터 준비
				 for(var i = 0; i < jsonData.length; i++) {
				  
				  if(jsonData[i].level == "2") {
				   cate2Obj = new Object();  //초기화
				   cate2Obj.cateCode = jsonData[i].cateCode;
				   cate2Obj.cateName = jsonData[i].cateName;
				   cate2Obj.cateCodeRef = jsonData[i].cateCodeRef;
				   
				   cate2Arr.push(cate2Obj);
				  }
				 }
				 
				 var cate2Select = $("select.category2");
				 
				 /*
				 for(var i = 0; i < cate2Arr.length; i++) {
				   cate2Select.append("<option value='" + cate2Arr[i].cateCode + "'>"
				        + cate2Arr[i].cateName + "</option>");
				 }
				 */
				 
				 cate2Select.children().remove();

				 $("option:selected", this).each(function(){
				  
				  var selectVal = $(this).val();  
				  cate2Select.append("<option value='" + selectVal + "'>전체</option>");
				  
				  for(var i = 0; i < cate2Arr.length; i++) {
				  	if(selectVal == cate2Arr[i].cateCodeRef) {
				    	cate2Select.append("<option value='" + cate2Arr[i].cateCode + "'>"
				        				 + cate2Arr[i].cateName + "</option>");
				  	}
				  }
				  
				});
				 
			});;
	}); // document
</script>
<%@ include file="../../include/footer.jsp"%>
  </body>
</html>