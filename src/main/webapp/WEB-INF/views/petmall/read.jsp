<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/shopping_sidebar.jsp"%>
<!--main content start-->
<section id="main-content" align="center";>
	<section class="wrapper" style="margin-top: 0px;">
		<h3 style="margin-top: 5px;">※ 상품 상세정보 ※</h3>
		<div class="row mt">
			<div class="col-lg-12" style="width: 100%; text-align: center;">
				<div class="content-panel" style="display: inline-block;">
					<section id="unseen">
						<table class="table table-bordered table-striped table-condensed"
							border="1">
							<tr>
								<td><img src="${path }/img/${productVO.productUrl}"
									width="340" height="300"></td>
								<td>
									<table border="1" style="height: 300px; width: 400px;">
										<tr align="center">
											<td>상품명</td>
											<td>${productVo.productName }</td>
										</tr>
										<tr align="center">
											<td>상품가격</td>
											<td><fmt:formatNumber value="${productVo.productPrice }" pattern="###,###,###" /></td>
										</tr>
										<tr align="center">
											<td>상품설명</td>
											<td>${productVo.productDetails }</td>
										</tr>
										<tr align="center">
											<td colspan="2">
												<form name="form1" action="/petmall/cart/insert"
													method="post">
													<input type="hidden" name="productNo" value="${productVo.productNo }"> 
													<select name="amount">
														<c:forEach begin="1" end="10" var="i">
															<option value="${i }">${i }</option>
														</c:forEach>
													</select>&nbsp;개 &nbsp;&nbsp;
													<button id="btn_cart" class="btn btn-warning btn-sm">장바구니에 담기</button>
												</form>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</section>

					<button type="submit" id="btn_list" class="btn btn-primary btn-sm">목록으로</button>
					<hr />
					<h4>♥ REVIEW 상품후기</h4>
					<table class="table table-bordered table-striped table-condensed">
                              <thead>
	                              <tr>
									<th>리뷰번호</th>
									<th>리뷰내용</th>
									<th>별점</th>
									<th>작성자</th>
									<th>작성일</th>
								</tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td>1</td>
                                  <td>잘 쓰고 있어요</td>
                                  <td>★★★★★</td>
                                  <td>seg615</td>
                                  <td>2019.12.1</td>
                              </tr>
                              </tbody>
                          </table>

				</div>
				<!-- /content-panel -->


			</div>
			<!-- /col-lg-4 -->

		</div>
		<!-- /row -->


	</section>
</section>
<script>
	$("#btn_list").on("click", function() {
		self.location = "/petmall/list";
	});
	
	$("#btn_cart").on("click", function() {
	});
</script>
<%@ include file="../include/footer.jsp"%>
</body>
</html>
<%-- 
♥ REVIEW 상품후기
                          
                            <table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>리뷰번호</th>
									<th>리뷰내용</th>
									<th>별점</th>
									<th>작성자</th>
									<th>작성일</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>잘쓰고 있어요!!</td>
									<td>★★★★★</td>  
									<td>${board.userId }</td>
									<td>seg615</td> <!-- D: 365일로 표현, d: 월에 해당하는 일로 표현, H: 24시간기준, h: 12시간기준 -->
									<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="2019-11-20"/></td> 
								</tr>
							</tbody>
						</table>
							<div class="box-footer">
								<div class="pull-right">
									<button type="button" class="btn btn-success btn-flat"
										id="writeBtn">
										<i class='fas fa-pen'> 글쓰기</i> 
									</button>
								</div>
							</div> --%>