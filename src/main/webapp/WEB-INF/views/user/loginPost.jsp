 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>

<script>
	var result = "${result}";
	console.log(result);	
	if(result == "notExist") {
		alert("해당 아이디가 존재하지 않습니다.");
	} else if(result == "wrongPw") {
		alert("아이디 또는 비밀번호가 일치하지 않습니다.");
	} else if(result == "loginSuccess"){
		alert('성공적으로 로그인하였습니다.');
		self.location = "/";
	}
	self.location = "/user/login";
</script>

<%@ include file="../include/footer.jsp"%>

  </body>
</html>