<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>
      
      <div id="login-page">
	  	<div class="container">
	  	
		      <form role="form" class="form-login" action="/user/loginPost" method="post" style="margin-top: 30px;">
		        <h2 class="form-login-heading">SIGN IN NOW</h2>
		        <div class="login-wrap">
		            <input type="text" class="form-control" name="userId" id="userId" placeholder="아이디" autofocus required>
		            <br>
		            <input type="password" class="form-control" name="userPw" id="userPw" placeholder="비밀번호" required>
		           <!--  <label class="checkbox">
		                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="useCookie">로그인 유지 -->
		                <span class="pull-right">
		                    <a data-toggle="modal" href="login.html#myModal"> 비밀번호를 잊으셨나요?</a>
		
		                </span>
		            </label>
		            <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> 로그인</button>
		            <hr>
		            
		           <!--  <div class="login-social-link centered">
		            <p>또는 카카오로 로그인 하기</p>
		                <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Kakao</button>
		            </div> -->
		            <div class="registration">
		                	아직 회원이 아니신가요?<br/>
		                <a class="" href="/user/register">회원가입</a>
		            </div>
		
		        </div>
		
		          <!-- Modal -->
		          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">비밀번호를 잊으셨나요?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>비밀번호를 재설정하기 위해 가입 시 입력했던  아이디와 이메일 주소를 입력해주세요.</p>
		                          <input type="text" name="text" autocomplete="off" class="form-control placeholder-no-fix">
		                          <input type="text" name="email" placeholder="abcd123@naver.com" autocomplete="off" class="form-control placeholder-no-fix">
		
		                      </div>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">닫기</button>
		                          <button class="btn btn-theme" id="login_btn" type="button">전송</button>
		                      </div>
		                  </div>
		              </div>
		          </div>
		          <!-- modal -->
		      </form>	  	
		          <br/><br/>
	  	
	  	</div>
	  </div>

<%@ include file="../include/footer.jsp"%>
<script>
$(document).ready(function() {
	var formObj = $("form[role='form']");
	
	var result = '${result}';
	if (result == "registerSuccess") {
		alert('회원가입이 성공적으로 처리되었습니다. 로그인 해주세요.');
	} 
	
	$("#login_btn").on("click", function() {
		var userId = $('#userId').val();
		var userPw = $('#userPw').val();
		
		if(userId == "") {
			alert('아이디를 입력해주세요.');
			$('#userId').focus();
			return;
		} 
		
		if (userPw == "") {
			alert('비밀번호를 입력해주세요.');
			$('#userPw').focus();
			return;
		} 
		formObj.submit();
	});


});
</script>
  </body>
</html>