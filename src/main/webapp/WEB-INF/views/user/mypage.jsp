<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/left_sidebar.jsp"%>
<section id="main-content">
	<section class="wrapper site-min-height" >

	</section> <!-- wrapper -->
</section> <!-- main-content -->

							
<script>
$(document).ready(function() {
	var result = '${result}';
	if(result == 'modifyUserSuccess') {
		alert('회원님의 정보가 정상적으로 수정되었습니다.');
	} else if(result == 'updated userPw') {
		alert('회원님의 비밀번호가 정상적으로 변경되었습니다.');
	} else if(result == 'deleteUserSuccess') {
		alert('정상적으로 탈퇴되었습니다.');
		window.location.href='/';
	}
	
	var formObj = $("form[role='form']");
	
	/* 정보수정하기 */ 
	$('#btn_modifyUserInfo').on("click", function() {
		var result = confirm('회원 정보를 수정하시겠습니까?');
		if(result) {
			formObj.submit(); 
		}
	});
	
	/* 회원탈퇴하기 */
	$('#btn_deleteUser').on("click", function() {
		//패스워드 입력 확인
		if($('#password').val() == ''){
			alert("패스워드를 입력해 주세요.");
			$('#passwordd').focus();
			return;
		}
		
		//패스워드 맞는지 확인
		$.ajax({
			url: "/passCheck.do",
			type: "POST",
			data: $('#deleteForm').serializeArray(),
			success: function(data){
				if(data==0){
					alert("패스워드가 틀렸습니다.");
					return;
				}else{
					//탈퇴
					var result = confirm('정말 탈퇴 하시겠습니까?');
					if(result){
						$('#deleteForm').submit();
					}
				}
			},
			error: function(){
				alert("서버 에러.");
			}
		}); // $.ajax
	}); // #btn_deleteUser




});/* document.ready */
</script>
<%@ include file="../include/footer.jsp"%>
</body>
</html>