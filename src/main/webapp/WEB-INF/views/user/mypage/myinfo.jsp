<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/left_sidebar.jsp"%>
<section id="main-content">
	<form role="form" method="post">	
		<input type="hidden" name="userId" value="${login.userId}" />
	</form>
<section class="wrapper" style="margin-top:0px;">
<div class="row mt">
	<div class="col-lg-12" style="width:100%; text-align:center;">
		<div class="form-panel" style="width:60%; display:inline-block; margin-top: 0;">
			<h3 class="mb" style="text-align:center;">회원정보</h3>
			<form class="form-horizontal style-form" method="post">
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">아이디</label>
				<div class="col-sm-10">
					<input type="text" name="userId" class="form-control" value="${login.userId}" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">이름</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userName" value="${login.userName}" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">이메일</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userEmail" value="${login.userEmail}" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">연락처</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userPhone" value="${login.userPhone}" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">우편번호</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userAddCode" value="${login.userAddCode}" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">주소</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userAddress1" value="${login.userAddress1}" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">상세주소</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userAddress2" value="${login.userAddress2}" readonly="readonly">
				</div>
			</div>
			</form>
			<div class="form-group">
				<div class="col-sm-12" align="center">
					<button type="button" id="btn_myInfoUpdate" class="btn btn-success">정보 수정</button>
				</div>
			</div>
			
		</div> <!-- form-panel -->
	</div> <!-- col-lg-12 -->
</div> <!-- row mt -->

						

						
<script>
$(document).ready(function() {
	var result = '${result}';
	if(result == 'modifyUserSuccess') {
		alert('회원님의 정보가 정상적으로 수정되었습니다.');
	} else if(result == 'updated userPw') {
		alert('회원님의 비밀번호가 정상적으로 수정되었습니다.');
	}
	
	
	var formObj = $("form[role='form']");
	
	/* 정보수정 페이지로 이동 */ 
	$('#btn_myInfoUpdate').on("click", function() {
		formObj.attr("action", "/user/mypage/myinfoUpdate"); 
		formObj.attr("method", "get");
		formObj.submit(); 
		
	});

});
</script>
</section>
</section>
<%@ include file="../../include/footer.jsp"%>
</body></html>