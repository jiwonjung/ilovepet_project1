<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<%@ include file="../../include/left_sidebar.jsp"%>
<section id="main-content">
<div class="row mt">
	<div class="col-lg-12" style="width:100%; text-align:center;">
		<div class="form-panel" style="width:60%; display:inline-block; margin-top: 0;">
			<h3 class="mb" style="text-align:center;">회원정보수정</h3>
			<form role="form" class="form-horizontal style-form" method="post">
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">아이디</label>
				<div class="col-sm-10">
					<input type="text" name="userId" class="form-control" value="${login.userId}" readonly="readonly">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">이름</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userName" value="${login.userName}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">이메일</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userEmail" value="${login.userEmail}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">연락처</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userPhone" value="${login.userPhone}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">우편번호</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userAddCode" value="${login.userAddCode}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">주소</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userAddress1" value="${login.userAddress1}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 col-sm-2 control-label">상세주소</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userAddress2" value="${login.userAddress2}">
				</div>
			</div>
			</form>
			<div class="form-group">
				<div class="col-sm-12" align="center">
					<button type="submit" id="btn_modifyUserInfo" class="btn btn-success">수정하기</button>
					<hr/>
					<button type="button" data-toggle="modal" data-target="#myDeleteModal" class="btn btn-default">회원 탈퇴</button>&nbsp;&nbsp;
					<button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-theme">비밀번호 변경</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- 모달 창 -->
<!-- 비밀번호 변경 MODAL -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						        <h4 class="modal-title" id="myModalLabel">비밀번호 변경</h4>
						      </div>
						      <div class="modal-body">
						      	변경하실 비밀번호를 입려해주세요.
						      </div>
						       <form role="form1" class="form-horizontal style-form" method="post" action="/user/mypage/password/update">
		                          <input type="hidden" name="userId" class="form-control" value="${login.userId }" >
		                           <div class="modal-body">
		                                  <input type="password" name="oldUserPw" class="form-control" placeholder="현재 비밀번호" id="currentPw" required>
		                                  <b><span class="help-block" style="color:red;" id="msg_oldPw"></span></b>
		                          </div>
 								  <div class="modal-body">
		                                  <input type="password" name="userPw" class="form-control" placeholder="새 비밀번호" id="newPw" required>
		                          		  <b><span class="help-block" style="color:red;" id="msg_newPw"></span></b>
		                          </div>
		                           <div class="modal-body">
		                                  <input type="password" name="userPwCk" class="form-control" placeholder="새 비밀번호 확인" id="newPw2" required>
		                                  <b><span class="help-block" style="color:red;" id="msg_newPw2"></span></b>
		                          </div>
		                        </form>
						      <div class="modal-footer">
						        <button type="submit" class="btn btn-primary" id="btn_changePw">비밀번호 변경</button>
						        <button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
						      </div>
						    </div>
						  </div>
						</div>      	
						
						<!-- 회원탈퇴 MODAL -->
						<div class="modal fade" id="myDeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						        <h4 class="modal-title" id="myModalLabel">정말로 탈퇴하시겠습니까?</h4>
						      </div>
						      <div class="modal-body">
						      	탈퇴를 진행하기 위하여 현재 비밀번호를 입력해주세요. 
						      </div>
						       <form role="form2" class="form-horizontal style-form" id="deleteForm" method="post" action="/user/mypage/delete">
		                          <input type="hidden" name="userId" value="${login.userId}" class="form-control" id="password">
		                           <div class="modal-body">
		                                  <input type="password" name="userPw" class="form-control" placeholder="현재 비밀번호">
		                          </div>
		                        </form>
						      <div class="modal-footer">
						        <button type="submit" id="btn_deleteUser" class="btn btn-primary">탈퇴하기</button>
						        <button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
						      </div>
						    </div>
						  </div>
						</div>  

						
<script>
$(document).ready(function() {
	var result = '${result}';
	if(result == 'modifyUserSuccess') {
		alert('회원님의 정보가 정상적으로 수정되었습니다.');
	}
	
	/* 정보수정하기 */ 
	var formObj = $("form[role='form']");

	$('#btn_modifyUserInfo').on("click", function() {
		var result = confirm('회원 정보를 수정하시겠습니까?');
		if(result) {
			formObj.attr("method", "post"); 
			formObj.attr("action", "/user/mypage/myinfoUpdate");  
			formObj.submit(); 
		} else {
			window.location.reload();
		}
	});
	
	/* 비밀번호 수정 */
	
	// 비밀번호 정규식
	var pwJ = /^[A-Za-z0-9]{4,12}$/;   // A~Z, a~z, 0~9로 시작하는 4~12자리 비밀번호 설정 가능
	
	var userPw = '${login.userPw}';

	/* $("#currentPw").blur(function() { // 현재비밀번호 입력란에서 blur
  		var inputPw = $('#currentPw').val();
  		console.log("입력pw: " + inputPw);
  		console.log("사용자 pw:" + userPw);
  		if(inputPw == userPw) {  // 이 부분 수정필요(BCrypt로 비번암호화 했기 때문에 제대로 값 비교를 못하고 있음)
			$('#btn_changePw').attr("disabled", false);
  		} else {
  			$('#msg_oldPw').text('현재 비밀번호를 잘못 입력하셨습니다.');
			$('#msg_oldPw').css("color", "red");
			$('#btn_changePw').attr("disabled", true);
  		}
  	}); // #userPw */
  	
	$("#newPw").blur(function() {   // 새비밀번호 입력란에서 blur
  		var newPw = $('#newPw').val();
  		if(pwJ.test($('#newPw').val())) {
  			$('#msg_newPw').text('사용가능한 새 비밀번호입니다.');
  			$('#msg_newPw').css("color", "green");
			$('#btn_changePw').attr("disabled", false);
  		} else {
  			$('#msg_newPw').text('새 비밀번호를 숫자 or 문자 4~12자리로 입력해주세요.');
			$('#msg_newPw').css("color", "red");
			$('#btn_changePw').attr("disabled", true);
  		}
  	}); // #userPw
  	 
	$("#newPw2").blur(function() {  // 새비밀번호 확인 입력란에서 blur
  		var newPw2 = $('#newPw2').val();
  		var newPw = $('#newPw').val();
  		if(newPw == newPw2) {
  			$('#msg_newPw2').text('비밀번호가 일치합니다.');
  			$('#msg_newPw2').css("color", "green");
			$('#btn_changePw').attr("disabled", false);
  		} else {
  			$('#msg_newPw2').text('새 비밀번호와 일치하지 않습니다.');
			$('#msg_newPw2').css("color", "red");
			$('#btn_changePw').attr("disabled", true);
  		}
  	}); // #userPw
  	
  	// 비밀번호변경 버튼 클릭 시
	$("#btn_changePw").on("click", function() {
		var result = confirm('새 비밀번호로 변경하시겠습니까?');
		if(result) {
			$("form[role='form1']").submit();
		} else {
			window.location.reload();
		}
	});
	
	
	/* 회원 탈퇴하기 */
	$("#btn_deleteUser").on("click", function() {
		var result = confirm('정말로 탈퇴하시겠습니까?');
		if(result) {
			$("form[role='form2']").submit();
		} else {
			window.location.reload();
		}
	});
	
});
</script>

</section>
<%@ include file="../../include/footer.jsp"%>
</body></html>