<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>

<!--main content start-->   
<section id="main-content" style="margin-right: 300px; width: 800px">
	<section class="wrapper">
		<h3>
			<i class="fa fa-angle-right"></i> 게시글 수정
		</h3>
		
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<form role="form" class="form-horizontal style-form" method="post">
						<input type="hidden" name="page" value="${pCriteria.page}" /> <!-- 수정처리 컨트롤러로 넘길 때 수정처리에 필요한 정보는: bid, page, numPerPage 이므로, hidden으로  넘김 -->
						<input type="hidden" name="numPerPage" value="${pCriteria.numPerPage }" />
						
						<h4 class="mb">
							<i class="fa fa-angle-right"></i> 수정내용
						</h4>
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">작성자</label>
							<div class="col-sm-10">
								<input type="text" name="userId" class="form-control" value="${boardVO.userId}" readonly="readonly">							
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">제목</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="boardTitle" value="${boardVO.boardTitle}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">내용</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="boardContent" rows="4">${boardVO.boardContent}</textarea>
							</div>
						</div>

					<div class="form-group">
                           <label class="col-lg-2 col-sm-2 control-label">작성일</label>
                           <div class="col-lg-10">
                               <p class="form-control-static"><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${boardVO.regdate }"/> 작성 &nbsp;  &nbsp;  &nbsp; 
                               <fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${boardVO.moddate }"/> 수정됨</p><br/>
                           </div>
                    </div>
					</form>
				</div>
				<!-- form-panel-->

				<div class="form-group">
					<div class="col-sm-12" align="center">
						<button type="submit" id="btn_modify" class="btn btn-success">저장하기</button>&nbsp;
						<button type="submit" id="btn_list" class="btn btn-primary">목록가기</button>
					</div>
				</div>

			<script>
				$(document).ready(function(){
					var formObj = $("form[role='form']");
					
					console.log("폼태그입니다..");
					
					// 수정버튼 클릭
					$("#btn_modify").on("click", function(){
						var result = confirm("해당 게시글을 수정하시겠습니까?");
						if(result) {
							formObj.submit(); 
						}
					});
					
					// 목록가기버튼 클릭
					$("#btn_list").on("click", function(){
						self.location = "/bbs/pagingList?page${pCriteria.page}&numPerPage=${pCriteria.numPerPage}"; 
					});
				});
			</script>


			</div>
			<!-- col-lg-12-->
		</div>
		<!-- /row -->
	</section>
	<!--/wrapper -->
</section>
<!-- /MAIN CONTENT -->    
    

<%@ include file="../include/footer.jsp"%>
  </body>
</html>