<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>

  
<!--main content start-->
<section id="main-content" style="margin-right: 300px; width: 800px">
	<section class="wrapper">
		<div class="row mt">
			<div class="col-lg-12">
				<div class="content-panel">
					<h4 style="text-align:center; font-size: 40px;"><b> 자유게시판   </b></h4>
			<div class="showback">
				<p style="text-align:center;">
				  <button type="button" class="btn btn-theme02 btn-lg" style="width: 200px;">자유이야기/분양후기</button>
				  <button type="button" class="btn btn-warning btn-lg" style="width: 200px;">질문게시판</button>
				</p>
			</div>	
					<section id="unseen">
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>글번호</th>
									<th>카테고리</th>
									<th>제목</th>
									<th>작성자</th>
									<th>작성일</th>
									<th>파일수</th>
									<th class="numeric">조회수</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${list }" var="board">  
								<tr>
									<td>${board.boardNo }</td>
									<td>${board.boardCategory}</td>
									<%-- <td><a href="/bbs/read?boardNo=${board.boardNo }">${board.boardTitle } <span class="badge bg-info">${board.replyCnt}</span></a></td>  --%>
									<td><a href="/bbs/readPaging${pagingMaker.makeUriComponents(pagingMaker.cri.page)}&boardNo=${board.boardNo }">${board.boardTitle } <span class="badge bg-info">${board.replyCnt}</span></a></td>
									<td>${board.userId }</td>
									<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${board.regdate }"/></td> <!-- D: 365일로 표현, d: 월에 해당하는 일로 표현, H: 24시간기준, h: 12시간기준 -->
									<td class="numeric"><span class="badge bg-inverse">${board.fileCnt }</span></td>
									<td class="numeric"><span class="badge bg-important">${board.hit }</span></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
							<div class="box-footer">
								<div class="pull-right">
									<button type="button" class="btn btn-success btn-flat"
										id="writeBtn">
										<i class='fas fa-pen'> 글쓰기</i> 
									</button>
								</div>
							</div>
					</section>

				</div>
				<!-- /content-panel -->
				
				<div class="showback" align="center">
						<div class="btn-group">
						  <c:if test="${pagingMaker.prev }">
						  	<%-- <a href="pagingList?page=${pagingMaker.startPage-1}" > --%>
						  	<a href="pagingList${pagingMake.makeUriComponents(pagingMaker.startPage-1)}" >
						  	<button type="button" class="btn btn-default"><i class="fa fa-angle-double-left" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  <c:forEach begin="${pagingMaker.startPage }" end="${pagingMaker.endPage }" var="pageNum">
						  <a href="pagingList${pagingMaker.makeUriComponents(pageNum)}">
						 	 <button type="button" class="<c:out value="${pagingMaker.cri.page == pageNum?'btn btn-primary':'btn btn-default'}"/>">${pageNum}</button> <!--${pagingMaker.cri.page}은 pagingMaker.getCri()과 PageCriteria의 getPage()와 같음  -->
						  </a>
						  </c:forEach>
						  <c:if test="${pagingMaker.next && pagingMaker.endPage > 0 }">
						  	<a href ="pagingList${pagingMaker.makeUriComponents(pagingMaker.endPage+1) }">
						  		<button type="button" class="btn btn-default"><i class="fa fa-angle-double-right" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  
						</div>      					
      				</div><!-- /showback -->
			</div>
			<!-- /col-lg-4 -->
		</div>
		<!-- /row -->
	</section>
	<!-- /wrapper -->
</section>

<script>
	var result= '${result}';
	
	if (result == 'writeSuccess') {
		alert('게시글이 정상적으로 입력되었습니다.');
	} else if (result == 'deleteSuccess') {
		alert('게시글이 정상적으로 삭제되었습니다.');
	} else if (result == 'modifySuccess') {
		alert('게시글이 정상적으로 수정되었습니다.');
	}
</script>

<%@ include file="../include/footer.jsp"%>
  </body>
</html>