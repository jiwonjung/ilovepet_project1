<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
<style>
	.fileDrop {
        width: 100%;
        height: 200px;
        border: 2px dotted #0b58a2;
    }
</style>
	 <!--main content start-->
      <section id="main-content" style="margin-right: 300px; width: 800px">
          <section class="wrapper" style="margin-top:20px;">
          	<h3><i class="fa fa-angle-right"></i> 자유게시판 글쓰기</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> 게시글 작성</h4>
                      <form role="form" class="form-horizontal style-form" method="post" enctype="multipart/form-data">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">글 카테고리</label>
                              <div class="col-sm-10">
                              	  <select class="form-control" name="boardCategory" required>
									  <option value="">----선택하세요-----</option>
									  <option value="자유/분양후기">자유이야기/분양후기</option>
									  <option value="Q&A">질문게시판</option>
								  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">제목</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="boardTitle" required="required">  <!-- name="컬럼명" 잘 맞춰 써줄 것!-->
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">내용</label>
                              <div class="col-sm-10">
                              	<textarea class="form-control" name="boardContent" rows="4" required="required"></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">작성자</label>
                              <div class="col-sm-10">
                                  <input type="text" name="userId" class="form-control" value="${login.userId}" readonly="readonly">
                              </div>
                          </div>
                          <!-- 첨부파일 영역 추가  -->
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">사진첨부</label>
                              <div class="col-sm-10">
                                  <input type="file" name="file" class="form-control" id="boardImg" />
                                  <div class="select_img"><img src="" style="margin:20px 0;" /></div>
                                  	<script>
									  $("#boardImg").change(function(){
									   if(this.files && this.files[0]) {
									    var reader = new FileReader;
									    reader.onload = function(data) {
									     $(".select_img img").attr("src", data.target.result).width(500);        
									    }
									    reader.readAsDataURL(this.files[0]);
									   }
									  });
									 </script>
									<%--  <%=request.getRealPath("/") %> --%>
                              </div>
                          </div>
                          
                          
                          <div class="form-group">
                          	<div class="col-sm-12" align="center">
                          		<button type="submit" id="wrtie_btn" class="btn btn-success">등록하기</button>
                          		<button type="submit" id="list_btn" class="btn btn-primary">목록으로</button>
                          	</div>
                          </div>
                      </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
          	
		</section><!-- /wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->

<script>

    $(document).ready(function () {
    	var formObj = $("form[role='form']");
		
		console.log("폼태그입니다..");
		
		// 글등록버튼 클릭
		$("#wrtie_btn").on("click", function(){
			alert('test');
			var boardCategory = ${"#boardCategory"}.val();
			if(boardCategory == "") {
				alert('글 카테고리를 선택하세요');
				$("#boardCategory").focus();
				return;
			} 
			
			var result = confirm("해당 게시글을 올리시겠습니까?");
			if(result){
				formObj.attr("action", "/bbs/search/write");  
				formObj.submit();
			} else {
				window.location.reload();
			}
		});
		
		// 목록가기버튼 클릭
		$("#list_btn").on("click", function(){
			formObj.attr("action", "/bbs/search/list");  
			formObj.attr("method", "get"); 
			formObj.submit();
		});
    });
</script>
<%@ include file="../../include/footer.jsp"%>
<!-- 첨부파일 출력 위해 HTML코드 동적으로 생성 위한 Handlebars템플릿 코드 -->
<script id="fileTemplate" type="text/x-handlebars-template">
    <li>
        <span class="mailbox-attachment-icon has-img">
            <img src="{{imgSrc}}" alt="Attachment">
        </span>
        <div class="mailbox-attachment-info">
            <a href="{{originalFileUrl}}" class="mailbox-attachment-name">
                <i class="fa fa-paperclip"></i> {{originalFileName}}
            </a>
            <a href="{{fullName}}" class="btn btn-default btn-xs pull-right delBtn">
                <i class="fa fa-fw fa-remove"></i>
            </a>
        </div>
    </li>
</script>
<script type="text/javascript" src="/resources/custom/js/article_file_upload.js"></script>

  </body>
</html>
