<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>
    
<section id="main-content" style="margin-right: 300px; width: 800px">
	<section class="wrapper" style="margin-top: 10px;">
		<h3 style="margin: 0;">
			<i class="fa fa-angle-right"></i> 게시글 조회
		</h3>

		<form role="form" method="post">	
			<input type="hidden" name="boardNo" value="${boardVO.boardNo}" />
			<input type="hidden" name="page" value="${sCriteria.page}" />
			<input type="hidden" name="numPerPage" value="${sCriteria.numPerPage}" />
			<input type="hidden" name="searchingCategory" value="${sCriteria.searchingCategory}" />
			<input type="hidden" name="keyword" value="${sCriteria.keyword}" />
		</form>

		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<form class="form-horizontal style-form" method="post">
					
						<h4 class="mb">
							<i class="fa fa-angle-right"></i> 조회내용
						</h4>
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">작성자</label>
							<div class="col-sm-10">
								<input type="text" name="userId" class="form-control" value="${boardVO.userId}" readonly="readonly">							
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">제목</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="boardTitle" value="${boardVO.boardTitle}" readonly="readonly">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">내용</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="boardContent" rows="4" readonly="readonly">${boardVO.boardContent}</textarea>
							</div>
							<c:if test="${boardVO.boardImg != null}">
								<div class="col-sm-12" style="text-align:center;">
									<img src="${boardVO.boardImg }" style="width:400px; height: auto; margin: 30px 0px;"/>
								</div>
							</c:if>
						</div>
						
						<br class="clear" style="clear:both;"/>

					<div class="form-group">
                           <label class="col-lg-2 col-sm-2 control-label">작성일</label>
                           <div class="col-lg-10">
                               <p class="form-control-static"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${boardVO.regdate }"/> 작성 &nbsp;  &nbsp;  &nbsp; 
                               <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${boardVO.moddate }"/> 수정됨</p><br/>
                           </div>
                    </div>
					<div class="form-group">
                           <label class="col-lg-2 col-sm-2 control-label">조회수</label>
                           <div class="col-lg-10">
                               <p class="form-control-static">${boardVO.hit }</p><br/>
                           </div>
                    </div>
                    
                    <%=request.getRealPath("/") %>
					</form><!-- form-panel-->
					
					<div class="col-sm-12" align="center">
                    		<br/><br/>
                    		<button id="btn_list" class="btn btn-primary">목록으로</button>
							<c:if test="${login.userId == boardVO.userId}">
								<button id="btn_modify" class="btn btn-success">수정하기</button>&nbsp;
							</c:if>
	                   		 <c:if test="${login.userId == boardVO.userId || login.isAdmin == 1}">
								<button id="btn_delete" class="btn btn-danger">삭제하기</button>&nbsp;
							</c:if>
						</div>
					</div>
					<br/><br/><br/><br/><br/><br/>

				

				

<!-- 댓글 영역 -->
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h3><i class="fa fa-edit"></i>댓글목록</h3>
					<hr/>
					
						<!-- 댓글 목록  -->
						<div class="form-group">
							<ul id="replyList">
							</ul><hr/>
						</div>
						
						<!-- 새로운 댓글 등록  -->
						<c:if test="${login != null}">
							<div class="form-group">
								<div class="col-sm-10">
									 작성자: &nbsp; <input type="text" name="userId" id="writer" value="${login.userId}" readonly="readonly" disabled />
								</div><br/>
								<div class="col-sm-10">
									 <textarea class="form-control" name="reBoardContent" rows="4" id="content" placeholder="댓글을 입력하세요."></textarea>
								</div>
							</div><br/>
							<button id="submitBtn" type="submit" class="btn btn-info">댓글등록</button><br/>
							<button type="reset" class="btn btn-default">댓글취소</button>
						</c:if>
						<br/><br/><br/><br/>
						<c:if test="${login == null }">
							<div class="form-group">
								<a href="/user/login" class="btn btn-default btn-block" role="button">
	                                <i class="fa fa-edit"></i> 로그인 한 사용자만 댓글 등록이 가능합니다.
	                            </a>
							</div><br/>
						
						</c:if>
						
						<!-- 페이징 번호 -->
						<div class="form-group">
							<div class="text-center">
								<ul class="pagination pagination-sm no-margin" id="pagingNumList">
									
								</ul>
							</div>
						</div>
						
				</div>		
			</div>
		</div>	
		
		
		<!-- Modal 창 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">댓글 수정창</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="form-group">
                    <label for="reBoardNo">댓글 번호</label>  <!-- for="replNo" 은 id로 쓰임  -->
                    <input class="form-control" id="reBoardNo" name="reBoardNo" readonly>
                </div>
		      	<div class="form-group">
                    <label for="reBoardContent">댓글 내용</label>  <!-- for="replNo" 은 id로 쓰임  -->
                    <input class="form-control" id="reBoardContent" name="reBoardContent">
                </div>
		      	<div class="form-group">
                    <label for="userId">댓글 작성자</label>  <!-- for="replNo" 은 id로 쓰임  -->
                    <input class="form-control" id="userId" name="userId" readonly>
                </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">닫기</button>
		        <button type="button" class="btn btn-success modalModifyBtn">수정</button>
		        <button type="button" class="btn btn-danger modalDeleteBtn">삭제</button>
		      </div>
		    </div>
		  </div>
		</div>      		

<script>
	$(document).ready(function(){
		var formObj = $("form[role='form']");
		
		console.log("폼태그입니다..");
		
		// 수정버튼 클릭
		$("#btn_modify").on("click", function(){
			formObj.attr("action", "/bbs/search/update");
			formObj.attr("method", "get"); 
			formObj.submit(); 
		});
		
		// 삭제버튼 클릭
		$("#btn_delete").on("click", function(){
			var result = confirm("정말로 해당 게시글을 삭제하시겠습니까?");
			if(result){
				formObj.attr("action", "/bbs/search/delete");  
				formObj.submit();
			}
		});
		
		// 목록가기버튼 클릭
		$("#btn_list").on("click", function(){
			formObj.attr("action", "/bbs/search/list");  
			formObj.attr("method", "get"); 
			formObj.submit();
		});
		
		
/* 댓글영역  */
		//var replyPageNum = 1;
		var boardNo = "${boardVO.boardNo}"; // 현재 게시글 번호
		var page = 1; // 댓글 페이지 번호 초기화
		
		getPagingReList(page);
		//replyList();
		
		// 댓글 리스트 가져오기(페이징 처리X)
		function replyList() {
			$.getJSON("/replies/selectAll/" + boardNo, function(data) {
				console.log("data length: " + data.length);
				console.log(data);
				
				var str = "";
				$(data).each(function() {
					str += "<li data-reBoardNo='" + this.reBoardNo + "' class='replyList'>"
		             +   "<p class='userId'>" + this.userId + "</p>"
		             +   "<p class='regdate'>" + this.regdate + "</p>"
		             +   "<p class='reBoardContent'>" + this.reBoardContent + "</p>"
		             +	 "<c:if test='${login != null}'>"
		             + 		"<button type='button' class='btn btn-success btn-xs' data-toggle='modal' data-target='#myModal'>댓글편집</button>"
		             +	 "</c:if>"
		             + "</li>"
		             + "<hr/>";
				});
				$("#replyList").html(str);
			});
		} // replyList()
		
		/* 댓글 쓰고 댓글등록 버튼 클릭 시 */
		$("#submitBtn").on("click", function() {
			var writer = $("#writer").val();
			var content = $("#content").val();
			
			$.ajax({
				type: 'post',
				url: '/replies',
				headers:{
					"Content-Type" : "application/json",
					"X-HTTP-Method-Override" : "POST"
				},
				dataType : 'text',
				data :JSON.stringify({  
				boardNo : boardNo,
				userId : writer,  // 오른쪽은 위에 정의된 변수명, 왼쪽은 VO 속성명
				reBoardContent : content
				}),
				
				success : function(result){
					if (result == 'writeSuccess'){
						alert("댓글을 성공적으로 등록하였습니다.");
						$("#writer").val("");  // 댓글 작성자 초기화
						$("#content").val(""); // 댓글 내용 초기화
						//$('html').scrollTop(0);
					}
					getPagingReList(page);
					//replyWriter.val("");  
					//replyContent.val(""); // 댓글 내용 초기화
				} 
			}); // ajax
		}); // #submitBtn
		
		/* 댓글 쓴 뒤 수정버튼 클릭 시 모달 창 뜨도록 */
		$("#replyList").on("click", ".replyList button", function() {
			var li = $(this).parent();
			var reBoardNo = li.attr("data-reBoardNo");
			var userId = li.find(".userId").text();
			var reBoardContent = li.find(".reBoardContent").text();
			
			// 해당 댓글의 내용도 함께 나오도록
			$("#reBoardNo").val(reBoardNo);
			$("#userId").val(userId);
			$("#reBoardContent").val(reBoardContent);
		});
		
		/* 모달창에서 댓글 삭제처리 */
		$(".modalDeleteBtn").on("click", function() {
			var result = confirm("해당 댓글을 삭제 하시겠습니까?");
			if(result) {
				
				var reBoardNo = $(this).parent().parent().find("#reBoardNo").val();
				$.ajax({
					type:'delete',
					url:'/replies/'+reBoardNo,
					headers :{
						"Content-Type" : "application/json",
						"X-HTTP-Method-Override" : "DELETE"
					},
					dataType: 'text',
					success: function(result) {
						console.log("result: " + result);
						if(result == "deleteSuccess") {
							alert('정상적으로 댓글을 삭제하였습니다.');
							$("#myModal").modal("hide");
							getPagingReList(page); // 댓글 목록 갱신 
						}
					}
				}); // ajax
			}
		}); // #modalDeleteBtn
		
		/* 모달창에서 댓글수정처리 */
		$(".modalModifyBtn").on("click", function() {
			var reply = $(this).parent().parent(); // 댓글 선택자
			var reBoardNo = reply.find("#reBoardNo").val(); // 댓글번호
			var reBoardContent = reply.find("#reBoardContent").val();
			
			$.ajax({
				type:'put',
				url:'/replies/'+reBoardNo,
				headers :{
					"Content-Type" : "application/json",
					"X-HTTP-Method-Override" : "PUT"				
				},
				data:JSON.stringify(
						{reBoardContent:reBoardContent}
				),
				dataType: 'text',
				success: function(result){
					console.log("result:" + result);
					if(result == 'updateSuccess'){
						alert("댓글을 성공적으로 수정하였습니다.");
						$("#myModal").modal("hide");
						getPagingReList(page); // 댓글 목록 갱신 
					}
				}
			}); // ajax
		}); // #modalModifyBtn
		
		/* 페이징 처리된 댓글 목록 리스트 */
						
		function getPagingReList(page) {
			$.getJSON("/replies/"+boardNo+ "/" + page, function(data) {
				console.log(data.pagingMaker.totalData);
				console.log(data);
				
				var str = "";
				
				if(data.pagingMaker.totalData >= 1) {
					$(data.reList).each(function() {
						str += "<li data-reBoardNo='" + this.reBoardNo + "' class='replyList'>"
			             +   "<p class='userId'>" + this.userId + "</p>"
			             +   "<p class='regdate'>" + this.regdate + "</p>"
			             +   "<p class='reBoardContent'>" + this.reBoardContent + "</p>"
			             + "<button type='button' class='btn btn-success btn-xs' data-toggle='modal' data-target='#myModal'>댓글편집</button>"
			             + "</li>"
			             + "<hr/>";
					});
				
				} else {
					str += "<li><h5 style='text-align:center;'>해당 게시물은 댓글이 없습니다. 첫 댓글을 남겨주세요.</h5></li>";
				}
					$("#replyList").html(str);
				
				showPagingNum(data.pagingMaker)
			}); // .getJSON
		}
		
		/* 페이징 처리된 리스트 목록 아래에 존재하는 버튼들 */
		function showPagingNum(pagingMaker) {
			var str = "";
			
			// 이전 버튼 활성화
			if(pagingMaker.prev){
				str +="<li><a href='"+(pagingMaker.startPage-1)+"'>◀</a></li>";
			}

			// 페이지 번호
			for(var i=pagingMaker.startPage, end=pagingMaker.endPage; i <= end; i++){			
				var selectedClass = pagingMaker.cri.page == i ? 'style="color:red;"' : '';
				str +="<a href='"+i+"'><button type='button'>"+i+"</button></a>";	
			}
			
			// 다음 버튼 활성화
			if(pagingMaker.next){
				str +="<li><a href='"+(pagingMaker.endPage+1)+"'>▶</a></li>";
			}
			
			$(".pagination-sm").html(str);
		}
		
		
		/* 목록페이지 번호 클릭 이벤트 */
		$(".pagination").on("click", "a button", function(e) {
			
			e.preventDefault(); // <a> 태그의 화면전환이 일어나지 않도록 하는 역할을 하는 메소드
			
			rePage = $(this).parent().attr("href");   // this는 버튼, this.parent()는 a 태그 
			
			getPagingReList(rePage); // 목록 페이지 호출
		});
		
	});
	
	var result = '${result}';
	if(result == 'modifyUserSuccess') {
		alert('회원님의 정보가 정상적으로 수정되었습니다.');
	}
	
</script>


			</div>
			<!-- col-lg-12-->
		</div>
		<!-- /row -->
	</section>
	<!--/wrapper -->
</section>
<!-- /MAIN CONTENT -->    
    

<%@ include file="../../include/footer.jsp"%>
  </body>
</html>