<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../../include/head.jsp"%>
<%@ include file="../../include/header.jsp"%>

  
<!--main content start-->
	<section class="wrapper" style="margin-top: 0;">
		<div class="row mt" style="width: 100%; text-align: center;">
			<div class="col-lg-12">
				<div class="content-panel" style="width:850px; display: inline-block; padding-top: 0;">
					<!-- <h4 style="text-align:center; font-size: 40px;"><b> 자유게시판   </b></h4> -->
			<section id="unseen">
						<p style="text-align:right; margin: 30px 20px;">
								  <a href="/bbs/search/list"><button type="button" id="btn_qna" class="btn btn-default btn-md">전체 보기</button></a>
								</p>
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>글번호</th>
									<th>카테고리</th>
									<th>제목</th>
									<th>작성자</th>
									<th>작성일</th>
									<th>댓글수</th>
									<th class="numeric">조회수</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${list }" var="board">  
								<tr>
									<td>${board.boardNo }</td>
									<td>${board.boardCategory}</td>
									<%-- <td><a href="/bbs/read?boardNo=${board.boardNo }">${board.boardTitle } <span class="badge bg-info">${board.replyCnt}</span></a></td>  --%>
									<td><a style="color:#002b80;" href="/bbs/search/read${pagingMaker.makeSearchingURI(pagingMaker.cri.page)}&boardNo=${board.boardNo }">${board.boardTitle } &nbsp;<c:if test="${board.boardImg != null}"><span class="badge bg-inverse"><i class="fa fa-file-photo-o"></i></span></c:if></a></td>
									<td>${board.userId }</td>
									<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${board.regdate }"/></td> <!-- D: 365일로 표현, d: 월에 해당하는 일로 표현, H: 24시간기준, h: 12시간기준 -->
									<td class="numeric">
										<span class="badge bg-info"><i class='fas fa-comment-dots'></i>&nbsp;${board.replyCnt}</span>	
									</td>
									<td class="numeric"><span class="badge bg-important">${board.hit }</span></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
						
						<div class="box-footer">
						<!-- 검색창 -->
	                      <form class="form-inline" role="form" align="center">
	                          <span class="col-md-12"> 
			                      <select class="form-control" name="searchingCategory">
									  <option value="N" 
									  	<c:out value="${sCriteria.searchingCategory == null? 'selected':''}"/>>----선택하세요----</option>
									   <option value="CATEGORY"
									    <c:out value="${sCriteria.searchingCategory == 'CATEGORY'? 'selected' : ''}"/>>카테고리</option>
									  <option value="T"
									    <c:out value="${sCriteria.searchingCategory == 'T'? 'selected' : ''}"/>>제목</option>
									  <option value="C"
									    <c:out value="${sCriteria.searchingCategory == 'C'? 'selected' : ''}"/>>내용</option>
									  <option value="W"
									    <c:out value="${sCriteria.searchingCategory == 'W'? 'selected': ''}"/>>작성자</option>
									  <option value="TC"
									    <c:out value="${sCriteria.searchingCategory == 'TC'? 'selected' :''}"/>>제목+내용</option>
									  <option value="CW"
									    <c:out value="${sCriteria.searchingCategory =='CW'? 'selected':''}"/>>내용+작성자</option>
									  <option value="TCW"
									    <c:out value="${sCriteria.searchingCategory =='TCW'? 'selected':''}"/>>제목+내용+작성자</option>
									</select>
	                              <input type="text" class="form-control" name="keyword" id="searchword" value="${sCriteria.keyword}"/>
		                          <button type="submit" class="btn btn-theme" id="searchBtn">검색</button> 
							</span>
								
	                      </form> 
							
								<span class="pull-right">
									<button type="button" class="btn btn-success btn-flat"
										id="writeBtn" style="margin-right: 10px; margin-top: 20px;">
										<i class='fas fa-pen'> 글쓰기</i> 
									</button>
								</span>
							</div>
					</section>

				</div> <br/><br/>
				<!-- /content-panel -->
				
				<div class="showback" align="center">
						<div class="btn-group">
						  <c:if test="${pagingMaker.prev }">
						  	<%-- <a href="pagingList?page=${pagingMaker.startPage-1}" > --%>
						  	<a href="list${pagingMaker.makeSearchingURI(pagingMaker.startPage-1)}" >
						  	<button type="button" class="btn btn-default"><i class="fa fa-angle-double-left" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  <c:forEach begin="${pagingMaker.startPage }" end="${pagingMaker.endPage }" var="pageNum">
						  <a href="list${pagingMaker.makeSearchingURI(pageNum)}">
						 	 <button type="button" class="<c:out value="${pagingMaker.cri.page == pageNum?'btn btn-primary':'btn btn-default'}"/>">${pageNum}</button> <!--${pagingMaker.cri.page}은 pagingMaker.getCri()과 PageCriteria의 getPage()와 같음  -->
						  </a>
						  </c:forEach>
						  <c:if test="${pagingMaker.next && pagingMaker.endPage > 0 }">
						  	<a href ="list${pagingMaker.makeSearchingURI(pagingMaker.endPage+1) }">
						  		<button type="button" class="btn btn-default"><i class="fa fa-angle-double-right" style="font-size:10px"></i></button>
						  	</a>
						  </c:if>
						  
						</div>      					
      				</div><!-- /showback -->
			</div>
			<!-- /col-lg-4 -->
		</div>
		<!-- /row -->
	</section>
	<!-- /wrapper -->

<script>
	var result= '${result}';
	
	if (result == 'writeSuccess') {
		alert('게시글이 정상적으로 입력되었습니다.');
	} else if (result == 'deleteSuccess') {
		alert('게시글이 정상적으로 삭제되었습니다.');
	} else if (result == 'updateSuccess') {
		alert('게시글이 정상적으로 수정되었습니다.');
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#searchBtn').on("click", function(e) {
			self.location = "list" 
				+ "${pagingMaker.makeSearchingURI(1)}" /* 검색하게되면 1페이지부터 데이터 나오는 것 */
				+ "&searchCategory="
				+ $("select option:selected").val()
				+ "&keyword=" + $("#searchword").val()
		});
		
		$('#writeBtn').on("click", function(e) {
			self.location = "write";  // 컨트롤러의 fbbs/write으로 감
		});
		
	});
</script>


<%@ include file="../../include/footer.jsp"%>
  </body>
</html>