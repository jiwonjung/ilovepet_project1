<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>
    
<section id="main-content" style="margin-right: 300px; width: 800px">
	<section class="wrapper">
		<h3>
			<i class="fa fa-angle-right"></i> 게시글 조회
		</h3>

		<form role="form" method="post">	
			<input type="hidden" name="boardNo" value="${boardVO.boardNo}" />
		</form>

		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<form class="form-horizontal style-form" method="post">
					
						<h4 class="mb">
							<i class="fa fa-angle-right"></i> 조회내용
						</h4>
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">작성자</label>
							<div class="col-sm-10">
								<input type="text" name="userId" class="form-control" value="${boardVO.userId}" readonly="readonly">							
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">제목</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="boardTitle" value="${boardVO.boardTitle}" readonly="readonly">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">내용</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="boardContent" rows="4" readonly="readonly">${boardVO.boardContent}</textarea>
							</div>
						</div>

					<div class="form-group">
                           <label class="col-lg-2 col-sm-2 control-label">작성일</label>
                           <div class="col-lg-10">
                               <p class="form-control-static"><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${boardVO.regdate }"/> 작성 &nbsp;  &nbsp;  &nbsp; 
                               <fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${boardVO.moddate }"/> 수정됨</p><br/>
                           </div>
                    </div>
					</form>
				</div>
				<!-- form-panel-->

				<div class="form-group">
					<div class="col-sm-12" align="center">
						<button type="submit" id="btn_modify" class="btn btn-success">수정하기</button>&nbsp;
						<button type="submit" id="btn_delete" class="btn btn-danger">삭제하기</button>&nbsp;
						<button type="submit" id="btn_list" class="btn btn-primary">목록으로</button>
					</div>
				</div>

			<script>
				$(document).ready(function(){
					var formObj = $("form[role='form']");
					
					console.log("폼태그입니다..");
					
					// 수정버튼 클릭
					$("#btn_modify").on("click", function(){
						formObj.attr("action", "/bbs/update");
						formObj.attr("method", "get"); 
						formObj.submit(); 
					});
					
					// 삭제버튼 클릭
					$("#btn_delete").on("click", function(){
						var result = confirm("정말로 해당 게시글을 삭제하시겠습니까?");
						if(result){
							formObj.attr("action", "/bbs/delete");  
							formObj.submit();
						}
					});
					
					// 목록가기버튼 클릭
					$("#btn_list").on("click", function(){
						self.location = "/bbs/list"; 
					});
				});
			</script>


			</div>
			<!-- col-lg-12-->
		</div>
		<!-- /row -->
	</section>
	<!--/wrapper -->
</section>
<!-- /MAIN CONTENT -->    
    

<%@ include file="../include/footer.jsp"%>
  </body>
</html>