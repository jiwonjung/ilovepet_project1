<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>

<%@ include file="../include/head.jsp"%>
<%@ include file="../include/header.jsp"%>

	 <!--main content start-->
      <section id="main-content" style="margin-right: 300px; width: 800px">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> 자유게시판</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> 게시글 작성</h4>
                      <form class="form-horizontal style-form" method="post">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">글 카테고리</label>
                              <div class="col-sm-10">
                              	  <select class="form-control" name="boardCategory">
									  <option>----선택하세요-----</option>
									  <option value="자유/분양후기">자유이야기/분양후기</option>
									  <option value="Q&A">질문게시판</option>
								  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">제목</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="boardTitle">  <!-- name="컬럼명" 잘 맞춰 써줄 것!-->
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">내용</label>
                              <div class="col-sm-10">
                              	<textarea class="form-control" name="boardContent" rows="4"></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">작성자</label>
                              <div class="col-sm-10">
                                  <input type="text" name="userId" class="form-control">
                              </div>
                          </div>
                          <div class="form-group">
                          	<div class="col-sm-12" align="center">
                          		<button type="submit" class="btn btn-success">등록하기</button>
                          		<button type="submit" class="btn btn-primary">목록으로</button>
                          	</div>
                          </div>
                      </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
          	
		</section><!-- /wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
	
<%@ include file="../include/footer.jsp"%>
  </body>
</html>
