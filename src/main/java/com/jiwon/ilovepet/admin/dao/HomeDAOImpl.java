package com.jiwon.ilovepet.admin.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

@Repository
public class HomeDAOImpl implements HomeDAO {
	
	@Inject 
	private SqlSession sqlSession;
	
	@Override
	public List<AdoptionVO> getMainAdoption() throws Exception {
		return sqlSession.selectList("getMainAdoption");
	}

	@Override
	public List<ProductVO> getMainProduct() throws Exception {
		return sqlSession.selectList("getMainProduct");
	}

}
