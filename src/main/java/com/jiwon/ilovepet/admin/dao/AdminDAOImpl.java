package com.jiwon.ilovepet.admin.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.petmall.vo.OrderListVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;
import com.jiwon.ilovepet.user.vo.UserVO;

@Repository
public class AdminDAOImpl implements AdminDAO {
	
	@Inject
	SqlSession sqlSession;

	// 회원들 전체 주문 목록
	@Override
	public List<OrderVO> listOrderAdmin() throws Exception {
		return sqlSession.selectList("orderListAdmin");
	}
	
	// 회원들 전체 주문 목록 (+ 페이징처리)
	@Override
	public List<OrderVO> listPagingOrderAdmin(PagingCriteria pCriteria) throws Exception {
		return sqlSession.selectList("listPagingOrderAdmin", pCriteria);
	}

	@Override
	public int countPagingOrderAdmin(PagingCriteria pCriteria) throws Exception {
		return sqlSession.selectOne("countPagingOrderAdmin", pCriteria);
	}

	
	// 특정 주문 목록
	@Override
	public List<OrderListVO> orderViewAdmin(OrderVO orderVO) throws Exception {
		return sqlSession.selectList("orderViewAdmin", orderVO);
	}
	
	// 배송 상태
	@Override
	public void deliveryAdmin(OrderVO orderVO) throws Exception {
		sqlSession.update("deliveryAdmin", orderVO);
	}
	
	// 상품 수량 조절
	@Override
	public void changeStockAdmin(ProductVO productVO) throws Exception {
		sqlSession.update("changeStockAdmin", productVO);
	}

	// 사용자 전체 목록
	@Override
	public List<UserVO> usersListAdmin() throws Exception {
		return sqlSession.selectList("usersListAdmin");
	}
	
	// 사용자 전체 목록 (페이징처리 O)
	@Override
	public List<UserVO> listUsersPaging(PagingCriteria pCriteria) throws Exception {
		return sqlSession.selectList("listUsersPaging", pCriteria);
	}

	@Override
	public int countListUsers(PagingCriteria pCriteria) throws Exception {
		return sqlSession.selectOne("countListUsers", pCriteria);
	}

}
