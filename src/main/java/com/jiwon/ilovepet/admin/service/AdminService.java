package com.jiwon.ilovepet.admin.service;

import java.util.List;

import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.petmall.vo.OrderListVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;
import com.jiwon.ilovepet.user.vo.UserVO;

public interface AdminService {
	// 회원들 전체 주문 목록
	public List<OrderVO> listOrderAdmin() throws Exception;
	
	// 회원들 전체 주문 목록 (+ 페이징처리)
	public List<OrderVO> listPagingOrderAdmin(PagingCriteria pCriteria) throws Exception;
	public int countPagingOrderAdmin(PagingCriteria pCriteria) throws Exception;
		
	
	// 특정 주문 목록
	public List<OrderListVO> orderViewAdmin(OrderVO orderVO) throws Exception;
	
	// 배송 상태
	public void delivery(OrderVO orderVO) throws Exception;

	// 상품 수량 조절
	public void changeStock(ProductVO productVO) throws Exception;
	
	// 사용자 전체 목록
	public List<UserVO> usersListAdmin() throws Exception;
	
	// 사용자 전체 목록 (페이징처리 O)
	public List<UserVO> listUsersPaging(PagingCriteria pCriteria) throws Exception;
	public int countListUsers(PagingCriteria pCriteria) throws Exception;
}
