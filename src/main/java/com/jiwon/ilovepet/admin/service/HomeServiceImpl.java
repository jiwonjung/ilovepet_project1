package com.jiwon.ilovepet.admin.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.jiwon.ilovepet.admin.dao.HomeDAO;
import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

@Service
public class HomeServiceImpl implements HomeService {
	
	@Inject
	private HomeDAO dao;
	
	@Override
	public List<AdoptionVO> getMainAdoption() throws Exception {
		return dao.getMainAdoption();
	}

	@Override
	public List<ProductVO> getMainProduct() throws Exception {
		return dao.getMainProduct();
	}

}
