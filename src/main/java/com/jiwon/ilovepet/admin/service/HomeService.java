package com.jiwon.ilovepet.admin.service;

import java.util.List;

import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

public interface HomeService {
	
	public List<AdoptionVO> getMainAdoption() throws Exception;

	public List<ProductVO> getMainProduct() throws Exception;
}
