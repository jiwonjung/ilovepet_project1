package com.jiwon.ilovepet.admin.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.jiwon.ilovepet.admin.dao.AdminDAO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.petmall.vo.OrderListVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;
import com.jiwon.ilovepet.user.vo.UserVO;

@Service
public class AdminServiceImpl implements AdminService {
	
	@Inject
	AdminDAO dao;
	
	// 회원들 전체 주문 목록
	@Override
	public List<OrderVO> listOrderAdmin() throws Exception {
		return dao.listOrderAdmin();
	}
	
	// 회원들 전체 주문 목록 (+ 페이징처리)
	@Override
	public List<OrderVO> listPagingOrderAdmin(PagingCriteria pCriteria) throws Exception {
		return dao.listPagingOrderAdmin(pCriteria);
	}

	@Override
	public int countPagingOrderAdmin(PagingCriteria pCriteria) throws Exception {
		return dao.countPagingOrderAdmin(pCriteria);
	}

	// 특정 주문 목록
	@Override
	public List<OrderListVO> orderViewAdmin(OrderVO orderVO) throws Exception {
		return dao.orderViewAdmin(orderVO);
	}
	
	// 배송 상태
	@Override
	public void delivery(OrderVO orderVO) throws Exception {
		dao.deliveryAdmin(orderVO);
	}
	
	// 상품 수량 조절
	@Override
	public void changeStock(ProductVO productVO) throws Exception {
		dao.changeStockAdmin(productVO);
	}
	
	// 사용자 전체 목록
	@Override
	public List<UserVO> usersListAdmin() throws Exception {
		return dao.usersListAdmin();
	}
	// 사용자 전체 목록 (페이징처리 O)
	@Override
	public List<UserVO> listUsersPaging(PagingCriteria pCriteria) throws Exception {
		return dao.listUsersPaging(pCriteria);
	}

	@Override
	public int countListUsers(PagingCriteria pCriteria) throws Exception {
		return dao.countListUsers(pCriteria);
	}
}
