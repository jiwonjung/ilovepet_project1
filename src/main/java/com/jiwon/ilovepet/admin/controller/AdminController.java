package com.jiwon.ilovepet.admin.controller;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jiwon.ilovepet.admin.service.AdminService;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.PagingMaker;
import com.jiwon.ilovepet.petmall.controller.OrderController;
import com.jiwon.ilovepet.petmall.vo.OrderListVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

@Controller
@RequestMapping("/admin/*")
public class AdminController {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
	
	@Inject
	private AdminService adminService;
	
	
	// 주문 목록
	@RequestMapping(value = "/petmall/orderList", method = RequestMethod.GET)
	public void getOrderList(Model model) throws Exception {
	 logger.info("관리자페이지의 orderlist()호출..........");
	   
	 List<OrderVO> orderList = adminService.listOrderAdmin();
	 
	 model.addAttribute("orderList", orderList);
	}
	
	// 주문 목록 (+ 페이징 처리) 
	@RequestMapping(value="/petmall/orderListPaging", method=RequestMethod.GET)
	public void pagingList(PagingCriteria pCriteria, Model model) throws Exception { // PageCriteria 타입을 보고 자동으로 저 객체를 생성해냄
		logger.info("관리자페이지에서 paging처리 된 orderlist()호출..........");
		logger.info(pCriteria.toString());
		
		// 리스트 넘기기
		model.addAttribute("list", adminService.listPagingOrderAdmin(pCriteria)); 
		
		// 페이징 처리한 결과 넘기기
		PagingMaker pagingMaker = new PagingMaker();
		pagingMaker.setCri(pCriteria);
		pagingMaker.setTotalData(adminService.countPagingOrderAdmin(pCriteria));
		
		model.addAttribute("pagingMaker", pagingMaker);
	}
	
	// 주문 상세 목록
	@RequestMapping(value = "/petmall/orderView", method = RequestMethod.GET)
	public void getOrderList(@RequestParam("orderNo") String orderNo,
	      OrderVO orderVO, Model model) throws Exception {
	 logger.info("관리자페이지의 orderView호출..........");
	 
		 orderVO.setOrderNo(orderNo);
		 List<OrderListVO> orderView = adminService.orderViewAdmin(orderVO);
		 
		 model.addAttribute("orderView", orderView);
	}
	
	// 주문 상세 목록- 주문 상태 변경
	@RequestMapping(value = "/petmall/orderView", method = RequestMethod.POST)
	public String delivery(OrderVO orderVO) throws Exception {
		logger.info("관리자페이지의 delivery 호출..........");
		
		System.out.println(orderVO.getOrderNo());
		adminService.delivery(orderVO);
		
		// 새로운 Service → DAO → Mapper 를 사용하지 않고, 기존에 있던 Service를 사용
		List<OrderListVO> orderView = adminService.orderViewAdmin(orderVO);	
				
		// 생성자 사용
		ProductVO product = new ProductVO();
				
		for(OrderListVO i : orderView) {
			product.setProductNo(i.getProductNo());
			System.out.println("productNo: " + i + "." + i.getProductNo());

			product.setProductStock(i.getAmount());
			System.out.println("productNo: " + i + "." + i.getAmount());
			
			adminService.changeStock(product);  // 주문완료시 주문의 갯수만큼 재고수량을 바꿈
		}
		
		return "redirect:/admin/petmall/orderView?orderNo=" + orderVO.getOrderNo();
	}
	
	// 사용자 전체 목록 보기 (페이징 처리 X) 
	@RequestMapping(value="/usersList", method = RequestMethod.GET)
	public void usersList(Model model) throws Exception {
		logger.info("관리자 페이지이에서 usersList 가져오기");
		
		model.addAttribute("list", adminService.usersListAdmin());
	}
	
	// 사용자 전체 목록 보기 (페이징 처리 O)
	@RequestMapping(value="/listUsersPaging", method = RequestMethod.GET)
	public void listUsersPaging(PagingCriteria pCriteria, Model model) throws Exception {
		logger.info("관리자 페이지에서 listUsersPaging 호출..........");
		logger.info(pCriteria.toString());
		
		// 리스트 넘기기
		model.addAttribute("list", adminService.listUsersPaging(pCriteria));
		
		// 페이징 처리한 결과 넘기기
		PagingMaker pagingMaker = new PagingMaker();
		pagingMaker.setCri(pCriteria);
		pagingMaker.setTotalData(adminService.countListUsers(pCriteria));
		
		model.addAttribute("pagingMaker", pagingMaker);
	}
	
}
