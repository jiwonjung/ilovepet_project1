package com.jiwon.ilovepet.user.vo;

import java.util.Date;

public class UserVO {
	/*
	 create table users (
	user_id varchar(50) not null primary key,
	user_pw varchar(50) not null,
	user_name varchar(50) not null,
	user_email varchar(100) not null,
	user_phone varchar(13) not null,
	user_addcode varchar(50), 
	user_address1 varchar(100), 
	user_address2 varchar(50), 
	isadmin char(1) default 'N',
	session_key varchar(50) not null default 'none',
	session_limit timestamp,
	regdate timestamp default now(),
	moddate timestamp default now()
); * */
	
	private String userId;
	private String userPw;
	private String userName;
	private String userEmail;
	private String userPhone;
	private String userAddCode;
	private String userAddress1;
	private String userAddress2;
	private int isAdmin;
	private Date regdate;
	private Date moddate;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getUserAddCode() {
		return userAddCode;
	}
	public void setUserAddCode(String userAddCode) {
		this.userAddCode = userAddCode;
	}
	public String getUserAddress1() {
		return userAddress1;
	}
	public void setUserAddress1(String userAddress1) {
		this.userAddress1 = userAddress1;
	}
	public String getUserAddress2() {
		return userAddress2;
	}
	public void setUserAddress2(String userAddress2) {
		this.userAddress2 = userAddress2;
	}
	public int getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(int isAdmin) {
		this.isAdmin = isAdmin;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModdate() {
		return moddate;
	}
	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}
	@Override
	public String toString() {
		return "UserVO [userId=" + userId + ", userPw=" + userPw + ", userName=" + userName + ", userEmail=" + userEmail
				+ ", userPhone=" + userPhone + ", userAddCode=" + userAddCode + ", userAddress1=" + userAddress1
				+ ", userAddress2=" + userAddress2 + ", isAdmin=" + isAdmin + ", regdate=" + regdate + ", moddate="
				+ moddate + "]";
	}
	
}
