package com.jiwon.ilovepet.user.vo;
/* 
 * VO와 DTO의 공통점: 데이터 수집과 전달에 사용, 파라미터/리턴타입으로 사용 가능
 * VO와 DTO의 차이점
 * 	- VO  : 데이터베이스와 거리가 가까워 테이블 구조를 이용해서 작성되는 경우 많음
 *  - DTO : 화면과 거리가 가까워 화면에서 전달되는 데이터를 수집하는 용도로 사용하는 경우 많음
 * */
public class LoginDTO {
	
	private String userId;
	private String userPw;
	private boolean useCookie;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	public boolean isUseCookie() {
		return useCookie;
	}
	public void setUseCookie(boolean useCookie) {
		this.useCookie = useCookie;
	}
	@Override
	public String toString() {
		return "LoginDTO [userId=" + userId + ", userPw=" + userPw + ", useCookie=" + useCookie + "]";
	}
	
	
}
