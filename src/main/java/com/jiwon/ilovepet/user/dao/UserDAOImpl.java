package com.jiwon.ilovepet.user.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.user.vo.LoginDTO;
import com.jiwon.ilovepet.user.vo.UserVO;

@Repository 
public class UserDAOImpl implements UserDAO {
	
	@Inject 
	private SqlSession sqlSession;
	
	@Override
	public String getTime() throws Exception {
		return sqlSession.selectOne("getTime"); 
	}
	
	// 회원 가입
	@Override
	public void insertUser(UserVO userVO) throws Exception {
		sqlSession.insert("insertUser", userVO);
	}
	
	// 회원 조회
	@Override
	public UserVO readUser(String userId) throws Exception {
		return sqlSession.selectOne("readUser", userId);
	}
	
	// 회원 정보 수정
	@Override
	public void updateUser(UserVO userVO) throws Exception {
		sqlSession.update("updateUser", userVO);
	}
	
	// 회원 비밀번호 수정
	@Override
	public void updateUserPw(String userId, String userNewPw) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("userId", userId);
		map.put("userPw", userNewPw);
		
		sqlSession.update("updateUserPw", map);
	}
	// 회원 탈퇴
	@Override
	public void deleteUser(UserVO userVO) throws Exception {
		sqlSession.delete("deleteUser", userVO);
	}
	
	// 회원 전체 목록
	@Override
	public List<UserVO> listUsers() throws Exception {
		return sqlSession.selectList("listUsers");
	}
	
	// 로그인 
	@Override
	public UserVO login(LoginDTO loginDTO) throws Exception {
		return sqlSession.selectOne("login", loginDTO);
	}
	
	// 로그인 유지 처리
	@Override
	public void keepLogin(String userId, String sessionId, Date sessionLimit) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("userId", userId);
		map.put("sessionId", sessionId);
		map.put("sessionLimit", sessionLimit);
		
		sqlSession.update("keepLogin", map);
	}
	
	// 세션키 검증
	@Override
	public UserVO checkSessionKeyOfUser(String value) throws Exception {
		return sqlSession.selectOne("checkSessionKeyOfUser", value);
	}
	
	// 회원비밀번호
	@Override
	public String getUserPw(String userId) throws Exception {
		return sqlSession.selectOne("getUserPw", userId);
	}
	
	// 중복 아이디 검사
	@Override
	public int idCheck(String userId) throws Exception {
		return sqlSession.selectOne("idCheck", userId);
	}
	
	// 회원자신이 쓴 자유게시판 글내역 조회
	@Override
	public List<BoardVO> listBbsById(PagingCriteria pCriteria, String userId) throws Exception {
		Map<Object, Object> map = new HashMap<>();
		map.put("userId", userId);
		map.put("pCriteria", pCriteria);
		
		return sqlSession.selectList("listBbsById", map);
	}

	// 회원자신이 쓴 무료분양 글내역 조회
	@Override
	public List<AdoptionVO> listAdoptionById(PagingCriteria pCriteria, String userId) throws Exception {
		return null;
	}


}
