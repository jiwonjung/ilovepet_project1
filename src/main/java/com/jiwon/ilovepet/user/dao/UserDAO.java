package com.jiwon.ilovepet.user.dao;

import java.util.Date;
import java.util.List;

import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.user.vo.LoginDTO;
import com.jiwon.ilovepet.user.vo.UserVO;

public interface UserDAO {
	
	// 현재 시각 얻기
	public String getTime() throws Exception;  
	
	// 회원 가입(C) 
	public void insertUser(UserVO userVO) throws Exception;
	
	// 회원 조회(R)
	public UserVO readUser(String userId) throws Exception;
	
	// 회원 수정(U)
	public void updateUser(UserVO userVO) throws Exception;
	
	// 회원 비밀번호 수정(U)
	public void updateUserPw(String userId, String userNewPw) throws Exception;
	
	// 회원 탈퇴(D)
	public void deleteUser(UserVO userVO) throws Exception;
	
	// 전체회원 목록 (페이징처리 x) 
	public List<UserVO> listUsers() throws Exception;
	
	// 로그인 처리
	public UserVO login(LoginDTO loginDTO) throws Exception;
	
	// 로그인 유지 처리
	public void keepLogin(String userId, String sessionId, Date sessionLimit) throws Exception;
	
	// 세션키 검증
	public UserVO checkSessionKeyOfUser(String value) throws Exception;
	
	// 회원 비밀번호
	public String getUserPw(String userId) throws Exception;
	
	// 중복 아이디 검사
	public int idCheck(String userId) throws Exception;
	
	// 회원자신이 쓴 자유게시판 글내역 조회
	public List<BoardVO> listBbsById(PagingCriteria pCriteria, String userId) throws Exception;
	
	// 회원자신이 쓴 무료분양 글내역 조회
	public List<AdoptionVO> listAdoptionById(PagingCriteria pCriteria, String userId) throws Exception;
	

}
