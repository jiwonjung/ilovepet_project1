package com.jiwon.ilovepet.user.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jiwon.ilovepet.board.controller.BoardController;
import com.jiwon.ilovepet.board.service.BoardService;
import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.user.service.UserService;
import com.jiwon.ilovepet.user.vo.UserVO;

@Controller
@RequestMapping("/user")
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	HttpSession session;
	@Inject
	private UserService userService;
	
	// 회원가입 폼
	@RequestMapping(value="/register", method=RequestMethod.GET)
	public void registerGET() throws Exception {
		logger.info("registerGET() 호출.........."); 
	}
	
	// 회원가입 처리
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String registerPOST(UserVO userVO, RedirectAttributes reAttributes) throws Exception {
		logger.info("registerPOST() 호출.........."); 
		logger.info("회원가입 한 내용: " + userVO.toString());
		
		String hashedPw = BCrypt.hashpw(userVO.getUserPw(), BCrypt.gensalt());
		
		userVO.setUserPw(hashedPw);
		userService.register(userVO);
		
		reAttributes.addFlashAttribute("result", "registerSuccess");
		
		return "redirect:/user/login";
	}
	
	// 마이페이지
	@RequestMapping(value="/mypage")
	public void mypage(@ModelAttribute("userVO") UserVO userVO, Model model) {
		model.addAttribute("userVO", userVO);
	}
	
	/*아래는 AJAX로 호출할 메소드*/
	
	// 회원정보 읽기
	@RequestMapping(value="/mypage/myinfo", method=RequestMethod.GET)
	public void myinfoGET(@RequestParam("userId") String userId, Model model) throws Exception {
		logger.info("회원정보 읽기 호출..........");
		model.addAttribute("userVO", userService.read(userId));
	}	
	
	// 회원정보 수정페이지
	@RequestMapping(value="/mypage/myinfoUpdate", method=RequestMethod.GET)
	public void myinfoUpdateGET(@RequestParam("userId") String userId, Model model) throws Exception {
		logger.info("회원정보 update페이지 호출............");
		
		model.addAttribute(userService.read(userId));
	}
	
	// 회원정보 수정처리
	@RequestMapping(value = "/mypage/myinfoUpdate", method = RequestMethod.POST)
	public String myinfoPOST(UserVO userVO, HttpSession session, Model model, RedirectAttributes reAttributes) throws Exception {
		logger.info("회원정보 update 처리 호출............");
		
		userService.update(userVO);
		
		session.setAttribute("login", userVO);
		
		String userId = userVO.getUserId();
		System.out.println("userId: " + userId);
		model.addAttribute(userService.read(userId));
		
		reAttributes.addFlashAttribute("result", "modifyUserSuccess");

		return "redirect:/user/mypage/myinfo?userId=" + userId;
	}
	
	// 회원 비밀번호 변경
	@RequestMapping(value="/mypage/password/update", method=RequestMethod.POST)
	public String userPwUpdate(String userPw, String userPwCk, String oldUserPw, String userId, RedirectAttributes reAttributes) throws Exception {
		
		System.out.println("기존 비밀번호 : " + oldUserPw);
		System.out.println("새로운 비밀번호 : " + userPw);
		System.out.println("새로운 비밀번호 확인 : " + userPwCk);

		System.out.println("로그인 한 사용자 아이디 : " + userId);
		/*
		// 새비밀번호&새빌민번호 확인란 일치 확인
		if(userPw != userPwCk) {
			System.out.println("새비밀번호와 새비밀번호 확인이 일치하지 않습니다.");
			reAttributes.addFlashAttribute("result", "wrong userPw");
			return "redirect:/user/mypage";
		}
		
		 */
		// 새로운 비밀번호 == 현재 비밀번호
		if(userPw == oldUserPw) {
			System.out.println("새로운 비밀번호 == 현재 비밀번호");
			reAttributes.addFlashAttribute("result", "same userPw");
			return "redirect:/user/mypage";
		}
		System.out.println("비밀번호를 새로 변경처리하겠습니다.");
		
		// 새로운 비밀번호 암호화 처리, 변경
		String hashedPw = BCrypt.hashpw(userPw, BCrypt.gensalt());
		userService.updateUserPw(userId, hashedPw); // 로그인한 사용자의 비번을 hashedPw로 만들어 update
		reAttributes.addFlashAttribute("result", "updated userPw");
		/*
		*/
		return "redirect:/user/mypage/myinfo?userId=" + userId;
	}
	
	// 회원 탈퇴
	@RequestMapping(value="/mypage/delete", method=RequestMethod.POST)
	public String deleteUser(UserVO userVO, HttpSession session, RedirectAttributes reAttributes) throws Exception {
		logger.info("deleteUser()호출..........");
		
		UserVO user = (UserVO)session.getAttribute("login");
		
		  String dbPW = user.getUserPw(); // 현재 로그인한 회원의 비밀번호
		  String inputPW = userVO.getUserPw(); // 회원탈퇴 시 인풋값에 넣은 비밀번호
		  
		  System.out.println("dbPW: " + dbPW); 
		  System.out.println("inputPW: " + inputPW); 
		  
		  if(!(dbPW.equals(inputPW))) { 
			  reAttributes.addFlashAttribute("result", "deleteUserFail"); 
			  return "redirect:/user/myinfo"; 
		  }
		  
		  userService.delete(userVO); // 탈퇴처리 
		  reAttributes.addFlashAttribute("result", "deleteUserSuccess");
		 
		return "redirect:/";
	}
	
	// 중복 아이디 검사
	@ResponseBody
	@RequestMapping(value="/idCheck", method=RequestMethod.GET)
	public int idCheck(@RequestParam("userId") String userId) throws Exception {
		
		logger.info("중복 아이디 검사...........");
		
		int idCheck = userService.idCheck(userId); // 1이면 중복아이디 존재, 0은 사용가능 아이디
		
		
		return idCheck;
	}
}
