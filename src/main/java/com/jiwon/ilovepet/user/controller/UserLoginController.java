package com.jiwon.ilovepet.user.controller;

import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import com.jiwon.ilovepet.user.service.UserService;
import com.jiwon.ilovepet.user.vo.LoginDTO;
import com.jiwon.ilovepet.user.vo.UserVO;

@Controller
@RequestMapping("/user")
public class UserLoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserLoginController.class);
	
	@Inject
	private UserService userService;
	
	HttpSession session;
	
	// 로그인 페이지
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public void loginGET(@ModelAttribute("loginDTO") LoginDTO loginDTO) throws Exception {
		logger.info("loginGET() 호출.........."); 
	}
	
	// 로그인 처리
	@RequestMapping(value="/loginPost", method=RequestMethod.POST)
	public void loginPOST(LoginDTO loginDTO, HttpServletRequest request, Model model) throws Exception {
		logger.info("loginPOST() 호출.........."); 
		
		UserVO userVO = userService.login(loginDTO);  // 로그인 처리
		
		// 존재하지 않는 회원인 경우
		if(userVO == null) {
			model.addAttribute("result", "notExist");
			return;
		} else { // 존재하는 회원
			//if(userVO.getUserPw().equals(loginDTO.getUserPw())) {  // 디비에 저장된 pw와 입력한 pw가 동일하면(즉, 로그인 성공)
			if(BCrypt.checkpw(loginDTO.getUserPw(), userVO.getUserPw())) {
				logger.info(BCrypt.checkpw(loginDTO.getUserPw(), userVO.getUserPw()) + "!! 로그인에 성공하였습니다.");
				
				System.out.println("DB PW: " + userVO.getUserPw());
				System.out.println("입력 PW: " + loginDTO.getUserPw());
				// 비밀번호가 일치면 user란 이름의 변수에 저장함
				model.addAttribute("result", "loginSuccess");
				model.addAttribute("user", userVO);
				
			} else { // 비번 불일치
				logger.info("비밀번호가 일치하지 않습니다.");
				model.addAttribute("result", "wrongPw");
				System.out.println("DB PW: " + userVO.getUserPw());
				System.out.println("입력 PW: " + loginDTO.getUserPw());
			}
		}
		
		// 로그인 유지를 선택할 경우
		if(loginDTO.isUseCookie()) {
			int amount = 60 * 60 * 24 * 7;  // 7일
			Date sessionsLimit = new Date(System.currentTimeMillis() + (1000 * amount)); // 로그인 유지기간 설정
			userService.keepLogin(userVO.getUserId(), session.getId(), sessionsLimit);
		}
		
	}
	
	// 로그아웃
	@RequestMapping(value="/logout")
	 public String logout(HttpServletRequest request, HttpServletResponse response,
             HttpSession httpSession, RedirectAttributes reAttributes) throws Exception {
		logger.info("회원이 로그아웃합니다.");
		
		Object object = httpSession.getAttribute("login");
		if (object != null) {
			UserVO userVO = (UserVO) object;
			httpSession.removeAttribute("login");
			httpSession.invalidate();
			Cookie loginCookie = WebUtils.getCookie(request, "loginCookie");
			
			if (loginCookie != null) {
			    loginCookie.setPath("/");
			    loginCookie.setMaxAge(0);
			    response.addCookie(loginCookie);
			    userService.keepLogin(userVO.getUserId(), "none", new Date());
			}
		}
		
		reAttributes.addFlashAttribute("result", "logoutSuccess");
		return "redirect:/";
	}
	
	
}
