package com.jiwon.ilovepet.user.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.jiwon.ilovepet.user.vo.LoginDTO;
import com.jiwon.ilovepet.user.vo.UserVO;

public interface UserService {

	// 회원 가입(C)
	public void register(UserVO userVO) throws Exception;

	// 회원 조회(R)
	public UserVO read(String userId) throws Exception;

	// 회원 수정(U)
	public void update(UserVO userVO) throws Exception;
	
	// 회원 비밀번호 수정(U)
	public void updateUserPw(String userId, String userNewPw) throws Exception;

	// 회원 탈퇴(D)
	public void delete(UserVO userVO) throws Exception;

	// 전체회원 목록 (페이징처리 x)
	public List<UserVO> list() throws Exception;
	
	// 로그인 처리
	public UserVO login(LoginDTO loginDTO) throws Exception;

	// 로그아웃
	public void logout(HttpSession session) throws Exception;
	
	// 로그인 유지 처리
	public void keepLogin(String userId, String sessionId, Date sessionLimit) throws Exception;
	
	// 세션키 검증
	public UserVO checkLoginBefore(String value) throws Exception;
	
	public boolean isValidUserPw(String userId, String userPw) throws Exception;
	
	// 중복 아이디 검사
	public int idCheck(String userId) throws Exception;
		
}
