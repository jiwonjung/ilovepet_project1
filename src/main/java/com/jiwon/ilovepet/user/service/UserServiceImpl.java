package com.jiwon.ilovepet.user.service;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.jiwon.ilovepet.user.dao.UserDAO;
import com.jiwon.ilovepet.user.vo.LoginDTO;
import com.jiwon.ilovepet.user.vo.UserVO;

@Service
public class UserServiceImpl implements UserService {
	
	@Inject
	UserDAO userDAO;
	
	@Override
	public void register(UserVO userVO) throws Exception {
		userDAO.insertUser(userVO);
	}

	@Override
	public UserVO read(String userId) throws Exception {
		return userDAO.readUser(userId);
	}

	@Override
	public void update(UserVO userVO) throws Exception {
		userDAO.updateUser(userVO);
	}
	
	@Override
	public void updateUserPw(String userId, String userNewPw) throws Exception {
		userDAO.updateUserPw(userId, userNewPw);
		
	}

	@Override
	public void delete(UserVO userVO) throws Exception {
		userDAO.deleteUser(userVO);
	}

	@Override
	public List<UserVO> list() throws Exception {
		return userDAO.listUsers();
	}

	@Override
	public UserVO login(LoginDTO loginDTO) throws Exception {
		return userDAO.login(loginDTO);
	}
	
	@Override
	public void logout(HttpSession session) throws Exception {
		session.invalidate();
	}

	// 로그인 유지 처리
	@Override
	public void keepLogin(String userId, String sessionId, Date sessionLimit) throws Exception {
		userDAO.keepLogin(userId, sessionId, sessionLimit);
	}
	
	// 세션키 검증
	@Override
	public UserVO checkLoginBefore(String value) throws Exception {
		return userDAO.checkSessionKeyOfUser(value);
	}

	@Override
	public boolean isValidUserPw(String userId, String userPw) throws Exception {
		String hashedUserPw = userDAO.getUserPw(userId);
		return BCrypt.checkpw(userPw, hashedUserPw);
	}

	@Override
	public int idCheck(String userId) throws Exception {
		return userDAO.idCheck(userId);
	}

}
