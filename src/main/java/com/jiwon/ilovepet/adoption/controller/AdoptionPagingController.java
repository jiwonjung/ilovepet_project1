package com.jiwon.ilovepet.adoption.controller;

import java.io.File;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jiwon.ilovepet.adoption.service.AdoptionService;
import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.commons.paging.PagingMaker;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.commons.utils.UploadFileUtils;

@Controller
@RequestMapping("/adoption/search/*")
public class AdoptionPagingController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdoptionPagingController.class);
	
	@Inject
	private AdoptionService adoptionService;
	
	@Resource(name="uploadPath")
	private String uploadPath;
	
	// 분양게시판 글 리스트 보기
	@RequestMapping(value="/list", method = {RequestMethod.GET,RequestMethod.POST})
	public void listGET(@ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model) throws Exception {
		logger.info("분양페이지의 listGET()호출..........");
		logger.info(sCriteria.toString());
		
		model.addAttribute("list", adoptionService.listAdoptionCriteria(sCriteria));
		
		PagingMaker pagingMaker = new PagingMaker();
		pagingMaker.setCri(sCriteria);
		pagingMaker.setTotalData(adoptionService.countAdoptionData(sCriteria));
	
		model.addAttribute("pagingMaker", pagingMaker);
		
	}
	
	// 분양게시판 글 조회하기
	@RequestMapping(value="/read", method=RequestMethod.GET)
	public void readGET(@RequestParam("adoptionNo") int adoptionNo, 
			@ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model) throws Exception {
		logger.info("분양페이지의 readGET()호출..........");
		model.addAttribute(adoptionService.read(adoptionNo));
	}
	
	// 분양게시판 삭제처리
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String delete(@RequestParam("adoptionNo") int adoptionNo, SearchingCriteria sCriteria, RedirectAttributes reAttributes) throws Exception {
		
		logger.info("분양페이지의 delete() 호출..........");
		
		adoptionService.delete(adoptionNo);
		
		reAttributes.addAttribute("page", sCriteria.getPage());
		reAttributes.addAttribute("numPerPage", sCriteria.getNumPerPage());
		reAttributes.addAttribute("searchingCategory", sCriteria.getSearchingCategory());
		reAttributes.addAttribute("keyword", sCriteria.getKeyword());
		
		reAttributes.addFlashAttribute("result", "deleteSuccess");
		
		return "redirect:/adoption/search/list";
	}
	
	// 분양게시판 수정 페이지 이동
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public void updateGET(int adoptionNo, @ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model) throws Exception {
		
		logger.info("분양페이지의 updateGET() 호출..........");
		logger.info(sCriteria.toString());
		
		model.addAttribute(adoptionService.read(adoptionNo));
	}
	
	// 분양게시판 수정 처리 
	@RequestMapping(value="/update", method=RequestMethod.POST) 
	public String updateyPOST(AdoptionVO adoptionVO, SearchingCriteria sCriteria, 
			RedirectAttributes reAttributes, HttpServletRequest req, MultipartFile file) throws Exception {
		
		logger.info("분양페이지의 updateyPOST() 호출..........");
		 // 새로운 파일이 등록되었는지 확인
		 if(file.getOriginalFilename() != null && file.getOriginalFilename() != "") {
		  // 기존 파일을 삭제
		  new File(uploadPath + req.getParameter("petImg")).delete();
		  new File(uploadPath + req.getParameter("petThumbImg")).delete();
		  
		  // 새로 첨부한 파일을 등록
		  String imgUploadPath = uploadPath + File.separator + "imgUpload";
		  String ymdPath = UploadFileUtils.calcPath(imgUploadPath);
		  String fileName = UploadFileUtils.fileUpload(imgUploadPath, file.getOriginalFilename(), file.getBytes(), ymdPath);
		  
		  adoptionVO.setPetImg(File.separator + "imgUpload" + ymdPath + File.separator + fileName);
		  adoptionVO.setPetThumbImg(File.separator + "imgUpload" + ymdPath + File.separator + "s" + File.separator + "s_" + fileName);
		  
		 } else {  // 새로운 파일이 등록되지 않았다면
		  // 기존 이미지를 그대로 사용
		 adoptionVO.setPetImg(req.getParameter("petImg"));
		 adoptionVO.setPetThumbImg(req.getParameter("petThumbImg"));
		  
		 }
		 
		
		adoptionService.update(adoptionVO);  // 수정 처리
		
		reAttributes.addAttribute("page", sCriteria.getPage());
		reAttributes.addAttribute("numPerPage", sCriteria.getNumPerPage());
		reAttributes.addAttribute("searchingCategory", sCriteria.getSearchingCategory());
		
		reAttributes.addAttribute("keyword", sCriteria.getKeyword());
		
		reAttributes.addFlashAttribute("result", "updateSuccess");
		
		logger.info(reAttributes.toString());
		
		return "redirect:/adoption/search/list";
	}
	
	// 글쓰기 페이지 
	@RequestMapping(value="/write", method=RequestMethod.GET)
	public void writeGET() throws Exception {
		
		logger.info("분양페이지의 writeGET() 호출..........");
	}
	
	// 글쓰기 처리
	@RequestMapping(value="/write", method=RequestMethod.POST)
	public String writePOST(AdoptionVO adoptionVO, RedirectAttributes reAttr, MultipartFile file, HttpServletRequest request) throws Exception {
		logger.info("분양페이지의 writePOST() 호출..........");
		logger.info("=====adoptionVO.toString()한 글내용: " + adoptionVO.toString());
		
		// 절대경로
		//String imgUploadPath = uploadPath + File.separator + "imgUpload";
		
		// 상대경로
		String imgUploadPath = request.getServletContext().getRealPath("resources/imgUpload/");
		System.out.println("imgUploadPath: " + imgUploadPath);
				
		String ymdPath = UploadFileUtils.calcPath(imgUploadPath);
		String fileName = null;

		if(file.getOriginalFilename() != null && file.getOriginalFilename() != "") {
		 fileName =  UploadFileUtils.fileUpload(imgUploadPath, file.getOriginalFilename(), file.getBytes(), ymdPath); 
		} else {
		 //fileName = uploadPath + File.separator + "images" + File.separator + "none.png";
		}

		adoptionVO.setPetImg(File.separator + "imgUpload" + ymdPath + File.separator + fileName);
		adoptionVO.setPetThumbImg(File.separator + "imgUpload" + ymdPath + File.separator + "s" + File.separator + "s_" + fileName);
		
		adoptionService.create(adoptionVO);
		
		reAttr.addFlashAttribute("result", "writeSuccess");
		
		return "redirect:/adoption/search/list";
	}
}
