package com.jiwon.ilovepet.adoption.controller;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jiwon.ilovepet.adoption.service.AdoptionService;
import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.board.controller.BoardController;

@Controller
@RequestMapping("/adoption")
public class AdoptionController {
	
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
	
	@Inject
	private AdoptionService adoptionService;
	
	// 분양게시판 글리스트(페이징처리x)
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public void list(Model model) throws Exception {
		logger.info("분양게시판 list()호출 ..........");
		
		model.addAttribute("list", adoptionService.list());
	}
	
	// 글쓰기 페이지
	@RequestMapping(value="/write", method= RequestMethod.GET)
	public void writeGET(AdoptionVO adoptionVO, Model model) throws Exception {
		logger.info("분양게시판 writeGET() 호출..........");
	}
	
	// 글쓰기 처리
	@RequestMapping(value="/write", method=RequestMethod.POST)
	public String writePOST(AdoptionVO adoptionVO, RedirectAttributes reAttributes) throws Exception {
		logger.info("분양게시판 writePOST() 호출 .........."); 
		logger.info("adoptionVO.toStriong()한 글 내용: " + adoptionVO.toString());
		
		adoptionService.create(adoptionVO);
		reAttributes.addFlashAttribute("result", "writeSuccess");
		
		return "redirect:/adoption/list";
	}
	
	// 글조회 처리
	@RequestMapping(value="read", method=RequestMethod.GET)
	public void read(@RequestParam("adoptionNo") int adoptionNo, Model model) throws Exception {
		logger.info("분양게시판 read()호출..........");
		
		model.addAttribute("adoptionVO", adoptionService.read(adoptionNo));
	}
	
	// 글삭제 처리
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String deletePOST(@RequestParam("adoptionNo") int adoptionNo, RedirectAttributes reAttributes) throws Exception {
		logger.info("분양게시판 deletePOST()호출..........");
		
		adoptionService.delete(adoptionNo);
		reAttributes.addFlashAttribute("result", "deleteSuccess");
		
		return "redirect:/adoption/list";
	}
	
	// 글수정 페이지
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public void updateGET(@RequestParam("adoptionNo") int adoptionNo, Model model) throws Exception {
		logger.info("분양게시판 updateGET()호출..........");
		
		model.addAttribute(adoptionService.read(adoptionNo));
	}
	
	// 글수정 처리
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String updatePOST(AdoptionVO adoptionVO, RedirectAttributes reAttributes) throws Exception {
		logger.info("분양게시판 updatePOST()호출..........");
		
		reAttributes.addFlashAttribute("result", "modifySuccess");
		
		return "redirect:/adoption/list";
	}
}
