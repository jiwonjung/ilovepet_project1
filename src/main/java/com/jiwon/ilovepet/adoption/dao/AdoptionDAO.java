package com.jiwon.ilovepet.adoption.dao;

import java.util.List;

import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;

public interface AdoptionDAO {
	
	// 분양글 쓰기
	public void writeAdoption(AdoptionVO adoptionVO) throws Exception;
	
	// 분양글 읽기
	public AdoptionVO readAdoption(Integer adoptionNo) throws Exception;
	
	// 분양글 수정
	public void updateAdoption(AdoptionVO adoptionVO) throws Exception;
	
	// 분양글 삭제
	public void deleteAdoption(Integer adoptionNo) throws Exception;
	
	// 분양글 리스트
	public List<AdoptionVO> listAdoption() throws Exception;
	
	// 페이징처리 + 검색처리 
	public List<AdoptionVO> listAdoptionCriteria(SearchingCriteria sCriteria) throws Exception;
	public int countAdoptionData(SearchingCriteria sCriteria) throws Exception;
}
