package com.jiwon.ilovepet.adoption.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;

@Repository
public class AdoptionDAOImpl implements AdoptionDAO {
	
	@Inject 
	private SqlSession sqlSession;
	
	// 분양글 쓰기
	@Override
	public void writeAdoption(AdoptionVO adoptionVO) throws Exception {
		sqlSession.insert("writeAdoption", adoptionVO);
	}
	
	// 분양글 읽기
	@Override
	public AdoptionVO readAdoption(Integer adoptionNo) throws Exception {
		return sqlSession.selectOne("readAdoption", adoptionNo);
	}
	
	// 분양글 수정
	@Override
	public void updateAdoption(AdoptionVO adoptionVO) throws Exception {
		sqlSession.update("updateAdoption", adoptionVO);
	}
	
	// 분양글 삭제
	@Override
	public void deleteAdoption(Integer adoptionNo) throws Exception {
		sqlSession.delete("deleteAdoption", adoptionNo);
	}
	
	// 분양글 리스트
	@Override
	public List<AdoptionVO> listAdoption() throws Exception {
		return sqlSession.selectList("listAdoption");
	}
	
	// 페이징 리스트 + 검색기능
	@Override
	public List<AdoptionVO> listAdoptionCriteria(SearchingCriteria sCriteria) throws Exception {
		return sqlSession.selectList("listAdoptionCriteria", sCriteria);
	}

	@Override
	public int countAdoptionData(SearchingCriteria sCriteria) throws Exception {
		return sqlSession.selectOne("countAdoptionData", sCriteria);
	}
}
