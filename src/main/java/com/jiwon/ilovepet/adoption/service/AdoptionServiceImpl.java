package com.jiwon.ilovepet.adoption.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.jiwon.ilovepet.adoption.dao.AdoptionDAO;
import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;

@Service
public class AdoptionServiceImpl implements AdoptionService {
	
	@Inject
	AdoptionDAO adoptionDAO;
	
	@Override
	public void create(AdoptionVO adoptionVO) throws Exception {
		adoptionDAO.writeAdoption(adoptionVO);

	}

	@Override
	public AdoptionVO read(Integer adoptionNo) throws Exception {
		return adoptionDAO.readAdoption(adoptionNo);
	}

	@Override
	public void update(AdoptionVO adoptionVO) throws Exception {
		adoptionDAO.updateAdoption(adoptionVO);
	}

	@Override
	public void delete(Integer adoptionNo) throws Exception {
		adoptionDAO.deleteAdoption(adoptionNo);
	}

	@Override
	public List<AdoptionVO> list() throws Exception {
		return adoptionDAO.listAdoption();
	}
	 
	@Override
	public List<AdoptionVO> listAdoptionCriteria(SearchingCriteria sCriteria) throws Exception {
		return adoptionDAO.listAdoptionCriteria(sCriteria);
	}
	@Override
	public int countAdoptionData(SearchingCriteria sCriteria) throws Exception {
		return adoptionDAO.countAdoptionData(sCriteria);
	}


}
