package com.jiwon.ilovepet.adoption.service;

import java.util.List;

import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;

public interface AdoptionService {
	
	// 분양글 쓰기 
	public void create(AdoptionVO adoptionVO) throws Exception;

	// 분양글 읽기
	public AdoptionVO read(Integer adoptionNo) throws Exception;
	
	// 분양글 수정
	public void update(AdoptionVO adoptionVO) throws Exception;
	
	// 분양글 삭제
	public void delete(Integer adoptionNo) throws Exception;
	
	// 분양글 리스트
	public List<AdoptionVO> list() throws Exception;
	
	// 페이징처리 + 검색처리 
	public List<AdoptionVO> listAdoptionCriteria(SearchingCriteria sCriteria) throws Exception;
	public int countAdoptionData(SearchingCriteria sCriteria) throws Exception;

}
