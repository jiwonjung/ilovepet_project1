package com.jiwon.ilovepet.adoption.vo;

import java.util.Date;

public class AdoptionVO {
/*
 create table adoption (
	adoption_no int not null auto_increment,
	user_id varchar(50) not null,
	title varchar(200) not null,
	state varchar(5) default '분양중',
	adoptor_name varchar(50) not null,
	adoptor_phone varchar(50) not null,
	adoptor_email varchar(100) not null,
	pet_location varchar(100) not null,
	pet_category varchar(50) not null,
	pet_amount varchar(50) not null,
	pet_sex varchar(10) default '모름',
	pet_content text(1000),
	regdate timestamp not null default now(),
	moddate timestamp not null default now(),
	primary key(adoption_no)
); */
	
	private int adoptionNo;
	private String userId;
	private String title;
	private String state; // 분양상태
	private String adoptorName;
	private String adoptorPhone;
	private String adoptorEmail;
	private String petLocation;
	private String petCategory;
	private String petAmount;
	private String petSex;
	private String petContent;
	private String petImg;
	private String petThumbImg;
	private Date regdate;
	private Date moddate;
	
	
	
	public String getPetImg() {
		return petImg;
	}
	public void setPetImg(String petImg) {
		this.petImg = petImg;
	}
	public String getPetThumbImg() {
		return petThumbImg;
	}
	public void setPetThumbImg(String petThumbImg) {
		this.petThumbImg = petThumbImg;
	}
	public int getAdoptionNo() {
		return adoptionNo;
	}
	public void setAdoptionNo(int adoptionNo) {
		this.adoptionNo = adoptionNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAdoptorName() {
		return adoptorName;
	}
	public void setAdoptorName(String adoptorName) {
		this.adoptorName = adoptorName;
	}
	public String getAdoptorPhone() {
		return adoptorPhone;
	}
	public void setAdoptorPhone(String adoptorPhone) {
		this.adoptorPhone = adoptorPhone;
	}
	public String getAdoptorEmail() {
		return adoptorEmail;
	}
	public void setAdoptorEmail(String adoptorEmail) {
		this.adoptorEmail = adoptorEmail;
	}
	public String getPetLocation() {
		return petLocation;
	}
	public void setPetLocation(String petLocation) {
		this.petLocation = petLocation;
	}
	public String getPetCategory() {
		return petCategory;
	}
	public void setPetCategory(String petCategory) {
		this.petCategory = petCategory;
	}
	public String getPetSex() {
		return petSex;
	}
	public void setPetSex(String petSex) {
		this.petSex = petSex;
	}
	public String getPetContent() {
		return petContent;
	}
	public void setPetContent(String petContent) {
		this.petContent = petContent;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModdate() {
		return moddate;
	}
	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPetAmount() {
		return petAmount;
	}
	public void setPetAmount(String petAmount) {
		this.petAmount = petAmount;
	}
	@Override
	public String toString() {
		return "AdoptionVO [adoptionNo=" + adoptionNo + ", userId=" + userId + ", title=" + title + ", state=" + state
				+ ", adoptorName=" + adoptorName + ", adoptorPhone=" + adoptorPhone + ", adoptorEmail=" + adoptorEmail
				+ ", petLocation=" + petLocation + ", petCategory=" + petCategory + ", petAmount=" + petAmount
				+ ", petSex=" + petSex + ", petContent=" + petContent + ", petImg=" + petImg + ", petThumbImg="
				+ petThumbImg + ", regdate=" + regdate + ", moddate=" + moddate + "]";
	}
	
}
