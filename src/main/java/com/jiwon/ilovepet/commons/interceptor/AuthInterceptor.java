package com.jiwon.ilovepet.commons.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import com.jiwon.ilovepet.user.service.UserService;
import com.jiwon.ilovepet.user.vo.UserVO;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);
    
    @Inject
    UserService userService;

    // 페이지 요청정보 저장 하여 자동페이지 이동처리
    private void saveDestination(HttpServletRequest request) {
        String uri = request.getRequestURI();
        String query = request.getQueryString();
        if (query == null || query.equals("null")) {
            query = "";
        } else {
            query = "?" + query;
        }

        if (request.getMethod().equals("GET")) {
            logger.info("destination : " + (uri + query));
            request.getSession().setAttribute("destination", uri + query);
        }
    }
    
    // 특정 경로에 접근할 경우 현재 사용자의 로그인 여부를 체크
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession httpSession = request.getSession();

        if (httpSession.getAttribute("login") == null) {
            logger.info("현재 사용자는 로그인되어있지 않으므로 로그인 후 이용해주세요.");
            
            // 자동페이지 이동처리
            saveDestination(request);

            Cookie loginCookie = WebUtils.getCookie(request, "loginCookie");
            if (loginCookie != null) {
               UserVO userVO = userService.checkLoginBefore(loginCookie.getValue());
                if (userVO != null) {
                    httpSession.setAttribute("login", userVO);
                    return true;
                }
            }

            response.sendRedirect("/user/login");
            return false;
        }
        return true;
    }
}
