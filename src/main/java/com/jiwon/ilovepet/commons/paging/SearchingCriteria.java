package com.jiwon.ilovepet.commons.paging;

public class SearchingCriteria extends PagingCriteria {
	
	private String searchingCategory; // 검색조건
	private String keyword; // 검색 키워드
	
	public String getSearchingCategory() {
		return searchingCategory;
	}
	public void setSearchingCategory(String searchingCategory) {
		this.searchingCategory = searchingCategory;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	@Override
	public String toString() {
		return "SearchingCriteria [searchingCategory=" + searchingCategory + ", keyword=" + keyword + "]";
	}
}
