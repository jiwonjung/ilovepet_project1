package com.jiwon.ilovepet.commons.paging;

public class PagingCriteria {
	
	private int page; // 해당 현재 페이지번호
	private int numPerPage; // 한 페이지당 출력되는 게시글 갯수
	
	public PagingCriteria() {
		this.page = 1;  // 디폴트      (page - 1) * 10;
		this.numPerPage = 10; // 디폴트로  10개씩    
	}
	
	public void setPage(int page) {
		if(page <= 0) {
			this.page = 1;
			return;
		}
		
		this.page = page;
	}
	
	public void setNumPerPage(int numPerPage) {
		if(numPerPage <= 0 || numPerPage > 100) { 
			this.numPerPage = 10; 
			return;
		}
		
		this.numPerPage = numPerPage;
	}
	
	public int getPage() {
		return page;
	}
	
	public int getNumPerPage() {
		return this.numPerPage;
	}
	
	// 추가 메소드
	public int getStartPage() {  // limit {startPage}{numPerPage}에서 startPage를 구하는 것
		return(this.page-1)*numPerPage;
	}

	@Override
	public String toString() {
		return "PagingCriteria [page=" + page + ", numPerPage=" + numPerPage + "]";
	}
	
}
