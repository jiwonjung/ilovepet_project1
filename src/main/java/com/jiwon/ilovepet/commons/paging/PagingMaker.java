package com.jiwon.ilovepet.commons.paging;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

public class PagingMaker {
	
	private int totalData;  // 데이터베이스에 있는 전체 데이터 갯수
	private int startPage; // 페이지 목록의 시작번호
	private int endPage; // 페이지 목록의 끝번호
	private boolean prev; // 이전 버튼을 나타내는 불린값
	private boolean next; // 다음 버튼을 나타내는 불린값
	
	private int displayPageNum = 10;  // 하단의 페이지 번호의 갯수
	
	private PagingCriteria pCriteria; 
	
	private String cateCode;
	private String level;
	
	private String boardCategory;
	
	public void setCateCodeLevel(String cateCode, String level) {
		this.cateCode = cateCode;
		this.level = level;
	}
	
	public void setBoardCategory(String boardCategory) {
		this.boardCategory = boardCategory;
	}
	public void setCri(PagingCriteria pCriteria) {
		this.pCriteria = pCriteria;
	}
	
	public void setTotalData(int totalData) {
		this.totalData = totalData;
		
		calculatePagingData();
	}
	
	/* 왜 double로 캐스팅? 원하는 값으로 얻기 위해. 안그러면  원하는 값 못가져옴 
	 *  i = 3/100   sysout(i) == > 0
	 * 
	 *  i = (int)3.0/100 sysout(i) ==> 33
	 *  
	 *  i = 3.0/100.0; sysout(i) ==> 0.03
	 *  i = 3 / 100.0; sysout(i) ==> 0.03
	 *  i = 100/(double)3; sysout(i) ==>33.3333333
	 * */
	
	// 추가 메소드
	private void calculatePagingData(){
		endPage = (int)(Math.ceil(pCriteria.getPage()/(double)displayPageNum) * displayPageNum);
		
		startPage = (endPage - displayPageNum) + 1;
		
		int finalEndPage = (int)(Math.ceil(totalData/(double)pCriteria.getNumPerPage()));
		
		if(endPage > finalEndPage){
			endPage = finalEndPage;
		}
		
		prev = startPage == 1? false: true;
		
		next = endPage*pCriteria.getNumPerPage() >= totalData ? false : true;
	}
	
	public PagingCriteria getCri() {
		return pCriteria;
	}
	
	public int getTotalData() {
		return totalData;
	}
	
	// 추가 메소드
	// URI를 자동으로 생성해주는 메소드
	public String makeUriComponents(int page) { 
		UriComponents uriComponents = UriComponentsBuilder.newInstance() 
				.queryParam("page", page)
				.queryParam("numPerPage", pCriteria.getNumPerPage()) 
				.build();
	  
	  return uriComponents.toUriString(); // ex) /page=1&numPerPage=10
	}
	
	public String makeSearchingURI(int page) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.queryParam("page", page)
				.queryParam("numPerPage", pCriteria.getNumPerPage())
				.queryParam("searchingCategory", ((SearchingCriteria)pCriteria).getSearchingCategory())
				.queryParam("keyword", ((SearchingCriteria)pCriteria).getKeyword())
				.build();
		
		return uriComponents.toUriString();
		// ex) /page=1&numPerPage=10&searchingCategory=S&keyword=테스트  
	}
	
	// 카테고리별 상품 리스트 URI만들기
	public String makeSearchingURIbyCategory(int page) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.queryParam("c", cateCode)
				.queryParam("l", level)
				.queryParam("page", page)
				.queryParam("numPerPage", pCriteria.getNumPerPage())
				.queryParam("searchingCategory", ((SearchingCriteria)pCriteria).getSearchingCategory())
				.queryParam("keyword", ((SearchingCriteria)pCriteria).getKeyword())
				.build();
		
		return uriComponents.toUriString();
		// ex) /c=101&l=2&page=1&numPerPage=10&searchingCategory=S&keyword=테스트  
	}
	
	// 카테고리별 게시판 리스트 URI만들기
	public String makeBbsSearchingURIbyCategory(int page) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.queryParam("boardCategory", boardCategory)
				.queryParam("page", page)
				.queryParam("numPerPage", pCriteria.getNumPerPage())
				.queryParam("searchingCategory", ((SearchingCriteria)pCriteria).getSearchingCategory())
				.queryParam("keyword", ((SearchingCriteria)pCriteria).getKeyword())
				.build();
		
		return uriComponents.toUriString();
		// ex) /c=101&l=2&page=1&numPerPage=10&searchingCategory=S&keyword=테스트  
	}
	
	// getter setter
	public int getStartPage() {
		return startPage;
	}

	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}

	public int getEndPage() {
		return endPage;
	}

	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}

	public boolean isPrev() {
		return prev;
	}

	public void setPrev(boolean prev) {
		this.prev = prev;
	}

	public boolean isNext() {
		return next;
	}

	public void setNext(boolean next) {
		this.next = next;
	}

	public int getDisplayPageNum() {
		return displayPageNum;
	}

	public void setDisplayPageNum(int displayPageNum) {
		this.displayPageNum = displayPageNum;
	}
}
