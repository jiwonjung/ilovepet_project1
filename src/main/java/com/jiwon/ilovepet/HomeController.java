package com.jiwon.ilovepet;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.jiwon.ilovepet.admin.service.HomeService;
import com.jiwon.ilovepet.adoption.service.AdoptionService;
import com.jiwon.ilovepet.adoption.vo.AdoptionVO;
import com.jiwon.ilovepet.commons.paging.PagingMaker;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Inject
	private HomeService homeService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, @ModelAttribute("sCriteria") SearchingCriteria sCriteria) throws Exception {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		logger.info("무료분양 최신글 가져오기 호출...........");
		model.addAttribute("listMainAdoption", homeService.getMainAdoption());
		
		logger.info("상품 최신글 가져오기 호출...........");
		model.addAttribute("listMainProudct", homeService.getMainProduct());
		
		return "home";
	}
	
	@RequestMapping("/test")
	public void test() {
		
	}
	
	/*
	 * @RequestMapping(value = "/fileupload", method = RequestMethod.POST) public
	 * String create(@ModelAttribute ProductVO vo, @RequestParam MultipartFile file,
	 * HttpServletRequest request) { String fileUrl = FileHelper.upload("/uploads",
	 * file, request); vo.setProductUrl(fileUrl); bookMapper.create(book); return
	 * "redirect:/test2"; }
	 */
    
    //uuid생성
    public static String getUuid() { 
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

	@RequestMapping("/ajaxTest")
	public void ajaxTest() {
		
	}
	@RequestMapping("/home2")
	public void test2() {
		
	}
}
