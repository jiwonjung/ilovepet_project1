package com.jiwon.ilovepet.petmall.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.petmall.dao.ProductDAO;
import com.jiwon.ilovepet.petmall.dao.ProductReviewDAO;
import com.jiwon.ilovepet.petmall.vo.ProductReviewVO;

@Service
public class ProductReviewServiceImpl implements ProductReviewService {
	
	@Inject
	ProductReviewDAO reviewDao;
	
	@Inject
	ProductDAO productDao;
	
	@Override
	public List<ProductReviewVO> list(Integer productNo) throws Exception {
		return reviewDao.listProductReview(productNo);
	}
	
	// 댓글이 추가되면 상품후기 갯수를 1씩 증가
	@Transactional
	@Override
	public void write(ProductReviewVO reviewVO) throws Exception {
		reviewDao.writeProductReview(reviewVO); // 상품후기 등록
		productDao.updateProductReviewCnt(reviewVO.getProductNo(), 1); // 상품평 갯수 증가
	}

	@Override
	public void update(ProductReviewVO reviewVO) throws Exception {
		reviewDao.updateProductReview(reviewVO);
	}
	
	// 댓글이 삭제되면 상품후기 갯수를 1씩 감소
	@Transactional
	@Override
	public void delete(Integer reviewNo) throws Exception {
		int productNo = reviewDao.getProductNo(reviewNo); // 상품후기의 상품 번호 조회
		reviewDao.deleteProductReview(reviewNo); // 상품평 삭제
		productDao.updateProductReviewCnt(productNo, -1); // 상품평 갯수 감소
	}

	@Override
	public List<ProductReviewVO> listPagingProductReview(Integer productNo, PagingCriteria pCriteria) throws Exception {
		return reviewDao.listPagingProductReview(productNo, pCriteria);
	}

	@Override
	public int countReviewData(Integer productNo) throws Exception {
		return reviewDao.countReviewData(productNo);
	}

}
