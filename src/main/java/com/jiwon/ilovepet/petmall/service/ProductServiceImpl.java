package com.jiwon.ilovepet.petmall.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.petmall.dao.ProductDAO;
import com.jiwon.ilovepet.petmall.vo.ProductCategoryVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;
import com.jiwon.ilovepet.petmall.vo.ProductViewVO;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Inject
	ProductDAO dao;
	
	@Override
	public List<ProductVO> list() throws Exception {
		return dao.listProduct();
	}

	@Override
	public void insert(ProductVO productVO) throws Exception {
		dao.insertProduct(productVO);
	}

	@Override
	public ProductVO read(Integer productNo) throws Exception {
		return dao.readProductDetails(productNo);
	}

	@Override
	public void update(ProductVO productVO) throws Exception {
		dao.updateProduct(productVO);
	}

	@Override
	public void delete(Integer productNo) throws Exception {
		dao.deleteProduct(productNo);
	}

	@Override
	public List<ProductVO> listProductCriteria(SearchingCriteria sCriteria) throws Exception {
		return dao.listProductCriteria(sCriteria);
	}

	@Override
	public int countProductData(SearchingCriteria sCriteria) throws Exception {
		return dao.countProductData(sCriteria);
	}

	@Override
	public List<ProductCategoryVO> listCategory() throws Exception {
		return dao.listCategory();
	}

	@Override
	public ProductViewVO productView(Integer productNo) throws Exception {
		return dao.productView(productNo);
	}

	@Override
	public List<ProductViewVO> listProductByCategory(int cateCode, SearchingCriteria sCriteria) throws Exception {
		return dao.listProductByCategory(cateCode, sCriteria);
	}

	@Override
	public int countProductDataByCategory(int cateCode, SearchingCriteria sCriteria) throws Exception {
		return dao.countProductDataByCategory(cateCode, sCriteria);
	}

}
