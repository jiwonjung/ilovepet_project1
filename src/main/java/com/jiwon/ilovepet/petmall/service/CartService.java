package com.jiwon.ilovepet.petmall.service;

import java.util.List;

import com.jiwon.ilovepet.petmall.vo.CartListVO;
import com.jiwon.ilovepet.petmall.vo.CartVO;

public interface CartService {
	
	// 장바구니에 담기
	public void insert(CartListVO cartListVO) throws Exception;
	
	// 장바구니 목록
	public List<CartListVO> list(String userId) throws Exception;
	
	// 장바구니 삭제
	public void delete(CartVO cartVO) throws Exception;
	
	// 구매완료 후 장바구니 전체 비우기
	public void deleteAllCart(String userId) throws Exception;
		
	// 장바구니 수정
	public void update(CartVO cartVO) throws Exception;
	
	// 장바구니 금액 합계 (장바구니 상품 전체금액 select조회한 결과 리턴)
	public int sumCartMoney(String userId) throws Exception;
	
	// 장바구니 동일한 상품 레코드 확인 (장바구니에 동일상품 잇는지 select조회한 결과를 리턴)
	public int countCart(int productNo, String userId) throws Exception;
	
	// 장바구니 상품수량 변경 (동일상품일 경우 수량을 합산하여 update)
	public void updateCartCnt(CartVO cartVO) throws Exception;
		
}
