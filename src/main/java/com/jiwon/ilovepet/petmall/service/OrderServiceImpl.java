package com.jiwon.ilovepet.petmall.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.jiwon.ilovepet.petmall.dao.OrderDAO;
import com.jiwon.ilovepet.petmall.vo.OrderDetailVO;
import com.jiwon.ilovepet.petmall.vo.OrderListVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

@Service
public class OrderServiceImpl implements OrderService {
	
	@Inject
	OrderDAO dao;
	
	// 주문 정보
	@Override
	public void orderInfo(OrderVO orderVO) throws Exception {
		dao.orderInfo(orderVO);
	}
	
	// 주문 상세 정보
	@Override
	public void orderInfo_Details(OrderDetailVO orderDetailVO) throws Exception {
		dao.orderInfo_Details(orderDetailVO);
	}
	
	// 주문 목록
	@Override
	public List<OrderVO> listOrder(OrderVO orderVO) throws Exception {
		return dao.listOrder(orderVO);
	}
	
	// 특정 주문 목록
	@Override
	public List<OrderListVO> orderView(OrderVO orderVO) throws Exception {
		return dao.orderView(orderVO);
	}
	
}
