package com.jiwon.ilovepet.petmall.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.jiwon.ilovepet.petmall.dao.CartDAO;
import com.jiwon.ilovepet.petmall.vo.CartListVO;
import com.jiwon.ilovepet.petmall.vo.CartVO;

@Service
public class CartServiceImpl implements CartService {
	
	@Inject
	CartDAO dao;
	
	@Override
	public void insert(CartListVO cartListVO) throws Exception {
		dao.insertCart(cartListVO);
	}

	@Override
	public List<CartListVO> list(String userId) throws Exception {
		return dao.listCart(userId);
	}

	@Override
	public void delete(CartVO cartVO) throws Exception {
		dao.deleteCart(cartVO);
	}
	
	// 구매완료 후 장바구니 전체 비우기
	@Override
	public void deleteAllCart(String userId) throws Exception {
		dao.deleteAllCart(userId);
	}
	
	@Override
	public void update(CartVO cartVO) throws Exception {
		dao.updateCart(cartVO);
	}

	@Override
	public int sumCartMoney(String userId) throws Exception {
		return dao.sumCartMoney(userId);
	}

	@Override
	public int countCart(int productNo, String userId) throws Exception {
		return dao.countCart(productNo, userId);
	}

	@Override
	public void updateCartCnt(CartVO cartVO) throws Exception {
		dao.updateCartCnt(cartVO);
	}

}
