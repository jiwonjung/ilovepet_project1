package com.jiwon.ilovepet.petmall.service;

import java.util.List;

import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.petmall.vo.ProductReviewVO;

public interface ProductReviewService {
	
	public List<ProductReviewVO> list(Integer productNo) throws Exception;
	
	public void write(ProductReviewVO reviewVO) throws Exception;
	
	public void update(ProductReviewVO reviewVO) throws Exception;
	
	public void delete(Integer reviewNo) throws Exception;
	
	// 리뷰 페이징 처리 
	public List<ProductReviewVO> listPagingProductReview(Integer productNo, PagingCriteria pCriteria) throws Exception;
	public int countReviewData(Integer productNo) throws Exception;
	
}
