package com.jiwon.ilovepet.petmall.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.petmall.vo.ProductReviewVO;

@Repository
public class ProductReviewDAOImpl implements ProductReviewDAO {
	
	@Inject
	private SqlSession sqlSession;
	
	@Override
	public List<ProductReviewVO> listProductReview(Integer productNo) throws Exception {
		return sqlSession.selectList("listProductReview", productNo);
	}

	@Override
	public void writeProductReview(ProductReviewVO reviewVO) throws Exception {
		sqlSession.insert("writeProductReview", reviewVO);
	}

	@Override
	public void updateProductReview(ProductReviewVO reviewVO) throws Exception {
		sqlSession.update("updateProductReview", reviewVO);
	}

	@Override
	public void deleteProductReview(Integer reviewNo) throws Exception {
		sqlSession.delete("deleteProductReview", reviewNo);
	}
	
	// 페이징처리
	@Override
	public List<ProductReviewVO> listPagingProductReview(Integer productNo, PagingCriteria pCriteria) throws Exception {
		
		Map<String, Object> map = new HashMap<>();
		map.put("productNo", productNo);
		map.put("pCriteria", pCriteria);
		return sqlSession.selectList("listPagingProductReview", map);
	}

	@Override
	public int countReviewData(Integer productNo) throws Exception {
		return sqlSession.selectOne("countReviewData", productNo);
	}
	
	// 리뷰의 상품글 번호 조회
	@Override
	public int getProductNo(Integer reviewNo) throws Exception {
		return sqlSession.selectOne("getProductNo", reviewNo);
	}

}
