package com.jiwon.ilovepet.petmall.dao;

import java.util.List;

import com.jiwon.ilovepet.petmall.vo.OrderDetailVO;
import com.jiwon.ilovepet.petmall.vo.OrderListVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

public interface OrderDAO {
	
	// 주문 정보
	public void orderInfo(OrderVO orderVO) throws Exception;
	
	// 주문 상세 정보
	public void orderInfo_Details(OrderDetailVO orderDetailVO) throws Exception;
	
	// 특정 유저의 주문 목록
	public List<OrderVO> listOrder(OrderVO orderVO) throws Exception;
	
	// 특정 주문 목록
	public List<OrderListVO> orderView(OrderVO orderVO) throws Exception;


}
