package com.jiwon.ilovepet.petmall.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.petmall.vo.OrderDetailVO;
import com.jiwon.ilovepet.petmall.vo.OrderListVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

@Repository
public class OrderDAOImpl implements OrderDAO {
	
	@Inject
	SqlSession sqlSession;
	
	// 주문 정보
	@Override
	public void orderInfo(OrderVO orderVO) throws Exception {
		sqlSession.insert("orderInfo", orderVO);
	}
	// 주문 상세 정보
	@Override
	public void orderInfo_Details(OrderDetailVO orderDetailVO) throws Exception {
		sqlSession.insert("orderInfo_Details", orderDetailVO);
	}
	
	// 주문 목록
	@Override
	public List<OrderVO> listOrder(OrderVO orderVO) throws Exception {
		return sqlSession.selectList("listOrder", orderVO);
	}
	
	// 특정  주문 목록
	@Override
	public List<OrderListVO> orderView(OrderVO orderVO) throws Exception {
		return sqlSession.selectList("orderView", orderVO);
	}

}
