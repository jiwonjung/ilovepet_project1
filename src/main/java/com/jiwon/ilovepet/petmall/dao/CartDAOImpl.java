package com.jiwon.ilovepet.petmall.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.petmall.vo.CartListVO;
import com.jiwon.ilovepet.petmall.vo.CartVO;

@Repository
public class CartDAOImpl implements CartDAO {
	
	@Inject
	SqlSession sqlSession;
	
	// 장바구니에 담기
	@Override
	public void insertCart(CartListVO cartListVO) throws Exception {
		sqlSession.insert("insertCart", cartListVO);
	}

	// 장바구니 목록
	@Override
	public List<CartListVO> listCart(String userId) throws Exception {
		return sqlSession.selectList("listCart", userId);
	}

	// 장바구니 삭제
	@Override
	public void deleteCart(CartVO cartVO) throws Exception {
		sqlSession.delete("deleteCart", cartVO);
	}

	// 구매완료 후 장바구니 전체 비우기
	@Override
	public void deleteAllCart(String userId) throws Exception {
		sqlSession.delete("deleteAllCart", userId);
	}

	
	
	@Override
	public void updateCart(CartVO cartVO) throws Exception {
		sqlSession.update("updateCart", cartVO);
	}
	
	// 장바구니 전체 금액 합계 (장바구니 상품 전체금액 select조회한 결과 리턴)
	@Override
	public int sumCartMoney(String userId) throws Exception {
		return sqlSession.selectOne("sumCartMoney", userId);
	}
	
	// 장바구니 동일한 상품 레코드 확인 (장바구니에 동일상품 잇는지 select조회한 결과를 리턴)
	@Override
	public int countCart(int productNo, String userId) throws Exception {
		Map<Object, Object> map = new HashMap<>();
		map.put("productNo", productNo);
		map.put("userId", userId);
		return sqlSession.selectOne("countCart", map);
	}
	
	// 장바구니 상품수량 변경 (동일상품일 경우 수량을 합산하여 update)
	@Override
	public void updateCartCnt(CartVO cartVO) throws Exception {
		sqlSession.update("updateCartCnt", cartVO);
	}

}
