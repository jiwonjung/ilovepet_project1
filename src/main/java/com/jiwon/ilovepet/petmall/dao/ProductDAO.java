package com.jiwon.ilovepet.petmall.dao;

import java.util.List;

import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.petmall.vo.ProductCategoryVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;
import com.jiwon.ilovepet.petmall.vo.ProductViewVO;

public interface ProductDAO {
	
	public List<ProductVO> listProduct() throws Exception;
	
	public void insertProduct(ProductVO productVO) throws Exception;
	
	public ProductVO readProductDetails(Integer productNo) throws Exception;
	
	public void updateProduct(ProductVO productVO) throws Exception;
	
	public void deleteProduct(Integer productNo) throws Exception;
	
	// 전체리스트 (페이징 + 검색처리)
	public List<ProductVO> listProductCriteria(SearchingCriteria sCriteria) throws Exception;
	public int countProductData(SearchingCriteria sCriteria) throws Exception;
	
	//해당상품의 리뷰수 변경 
	public void updateProductReviewCnt(Integer productNo, int amount) throws Exception;
	
	// 카테고리 
	public List<ProductCategoryVO> listCategory() throws Exception;
	
	// 상품 조회 + 카테고리 조인
	public ProductViewVO productView(Integer productNo) throws Exception;
	
	// 전체리스트(카테고리별로 볼 수 있도록)
	public List<ProductViewVO> listProductByCategory(int cateCode, SearchingCriteria sCriteria) throws Exception;
	public int countProductDataByCategory(int cateCode, SearchingCriteria sCriteria) throws Exception;
	
}
