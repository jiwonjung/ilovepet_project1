package com.jiwon.ilovepet.petmall.dao;

import java.util.List;

import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.petmall.vo.ProductReviewVO;

public interface ProductReviewDAO {

	public List<ProductReviewVO> listProductReview(Integer productNo) throws Exception;
	
	public void writeProductReview(ProductReviewVO reviewVO) throws Exception;
	
	public void updateProductReview(ProductReviewVO reviewVO) throws Exception;
	
	public void deleteProductReview(Integer reviewNo) throws Exception;
	
	// 리뷰 페이징 처리 
	public List<ProductReviewVO> listPagingProductReview(Integer productNo, PagingCriteria pCriteria) throws Exception;
	public int countReviewData(Integer productNo) throws Exception;
	
	// 리뷰의 게시물 번호 조회
	int getProductNo(Integer reviewNo) throws Exception;
	
}
