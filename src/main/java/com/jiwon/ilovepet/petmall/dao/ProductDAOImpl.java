package com.jiwon.ilovepet.petmall.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.petmall.vo.ProductCategoryVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;
import com.jiwon.ilovepet.petmall.vo.ProductViewVO;

@Repository
public class ProductDAOImpl implements ProductDAO {
	
	@Inject 
	private SqlSession sqlSession;
	
	@Override
	public List<ProductVO> listProduct() throws Exception {
		return sqlSession.selectList("listProduct");
	}
	
	@Override
	public void insertProduct(ProductVO productVO) throws Exception {
		sqlSession.insert("insertProduct", productVO);
	}

	@Override
	public ProductVO readProductDetails(Integer productNo) throws Exception {
		return sqlSession.selectOne("readProductDetails", productNo);
	}

	@Override
	public void updateProduct(ProductVO productVO) throws Exception {
		sqlSession.update("updateProduct", productVO);
	}

	@Override
	public void deleteProduct(Integer productNo) throws Exception {
		sqlSession.delete("deleteProduct", productNo);
	}
	
	// 전체리스트 (페이징 + 검색처리)
	@Override
	public List<ProductVO> listProductCriteria(SearchingCriteria sCriteria) throws Exception {
		return sqlSession.selectList("listProductCriteria", sCriteria);
	}

	@Override
	public int countProductData(SearchingCriteria sCriteria) throws Exception {
		return sqlSession.selectOne("countProductData", sCriteria);
	}
	
	//해당상품의 리뷰수 변경 
	@Override
	public void updateProductReviewCnt(Integer productNo, int amount) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("productNo", productNo);
		map.put("amount", amount);
		
		sqlSession.update("updateProductReviewCnt", map);
	}
	// 카테고리 가져오기
	@Override
	public List<ProductCategoryVO> listCategory() throws Exception {
		return sqlSession.selectList("listCategory");
	}
	
	// 상품 조회 + 카테고리 조인
	@Override
	public ProductViewVO productView(Integer productNo) throws Exception {
		return sqlSession.selectOne("productView", productNo);
	}
	
	// 전체리스트(카테고리별로 볼 수 있도록)
	@Override
	public List<ProductViewVO> listProductByCategory(int cateCode, SearchingCriteria sCriteria) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("cateCode", cateCode);
		map.put("sCriteria", sCriteria);
		return sqlSession.selectList("listProductByCategory", map);
	}

	@Override
	public int countProductDataByCategory(int cateCode, SearchingCriteria sCriteria) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("cateCode", cateCode);
		map.put("sCriteria", sCriteria);
		return sqlSession.selectOne("countProductDataByCategory", map);
		
	}
	

}
