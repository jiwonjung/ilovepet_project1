package com.jiwon.ilovepet.petmall.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jiwon.ilovepet.boardReply.vo.BoardReplyVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.PagingMaker;
import com.jiwon.ilovepet.petmall.service.ProductReviewService;
import com.jiwon.ilovepet.petmall.vo.ProductReviewVO;


@RestController
@RequestMapping("/review")
public class ProductReviewController {
	private static final Logger logger = LoggerFactory.getLogger(ProductReviewController.class);
	
	@Inject
	private ProductReviewService reviewService;
	
	// POST는 상품후기 등록할 때
	@RequestMapping(value="", method=RequestMethod.POST)  // http://localhost:8080/replies/JSON DATA
	public ResponseEntity<String> write(@RequestBody ProductReviewVO reviewVO) { // @RequestBody는 JSON 객체로 변환시킴
		
		ResponseEntity<String> responseEntity = null; // ResponseEntity는 Http Status(상태코드)로 반환하겠다는 것임
		
		try {
			reviewService.write(reviewVO);
			
			responseEntity = new ResponseEntity<String>("writeSuccess", HttpStatus.OK); // OK는 200번 
		} catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST); // BAD_REQUEST는 400
		}
		
		return responseEntity;
	}
	
	// GET은 상품후기 목록 가져오기
		@RequestMapping(value="/selectAll/{productNo}", method=RequestMethod.GET)  // http://localhost:8080/selectAll/1
		public ResponseEntity<List<ProductReviewVO>> list(@PathVariable("productNo") Integer productNo) {
			
			ResponseEntity<List<ProductReviewVO>> responseEntity = null;
			
			try {
				responseEntity = new ResponseEntity<>(reviewService.list(productNo),HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				responseEntity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			return responseEntity;
		}
	
	// PUT/PATCH은 상품후기 수정
	@RequestMapping(value="/{reviewNo}", method= {RequestMethod.PUT, RequestMethod.PATCH}) // 
	public ResponseEntity<String> update(@PathVariable("reviewNo") Integer reviewNo, @RequestBody ProductReviewVO reviewVO) {// @RequestBody는 JSON 객체로 변환시킴
		
		ResponseEntity<String> responseEntity = null;
		
		try {
			reviewVO.setReviewNo(reviewNo);
			reviewService.update(reviewVO);
			
			responseEntity = new ResponseEntity<String>("updateSuccess", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		return responseEntity;
	}
	
	// DELETE은 상품후기 삭제
	@RequestMapping(value = "/{reviewNo}", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(@PathVariable("reviewNo") Integer reviewNo) {

		ResponseEntity<String> responseEntity = null;

		try {
			reviewService.delete(reviewNo);
			
			responseEntity = new ResponseEntity<String>("deleteSuccess", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
	
	// 상품후기에 대한 페이징 처리
	@RequestMapping(value="/{productNo}/{page}", method=RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> reviewListPage(
			@PathVariable("productNo") Integer productNo,
			@PathVariable("page") Integer page) {
		
		ResponseEntity<Map<String, Object>> resEntity = null;
		try {
			PagingCriteria pCriteria = new PagingCriteria();
			pCriteria.setPage(page);
			
			List<ProductReviewVO> reList = reviewService.listPagingProductReview(productNo, pCriteria);
			int reCount = reviewService.countReviewData(productNo);
			PagingMaker pagingMaker = new PagingMaker();
			pagingMaker.setCri(pCriteria);
			pagingMaker.setTotalData(reCount);
			
			Map<String, Object> reMap = new HashMap<>();
			reMap.put("reList", reList);
			reMap.put("pagingMaker", pagingMaker);
			
			resEntity = new ResponseEntity<Map<String,Object>>(reMap, HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			resEntity = new ResponseEntity<Map<String,Object>>(HttpStatus.BAD_REQUEST);
		}
		
		return resEntity;
	}	
}
