package com.jiwon.ilovepet.petmall.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jiwon.ilovepet.commons.paging.PagingMaker;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.commons.utils.UploadFileUtils;
import com.jiwon.ilovepet.petmall.service.ProductService;
import com.jiwon.ilovepet.petmall.vo.ProductCategoryVO;
import com.jiwon.ilovepet.petmall.vo.ProductVO;
import com.jiwon.ilovepet.petmall.vo.ProductViewVO;

import net.sf.json.JSONArray;

@Controller
@RequestMapping("/petmall/search/*")
public class PetmallPagingController {
	
	private static final Logger logger = LoggerFactory.getLogger(PetmallPagingController.class);
	
	@Inject
	private ProductService productService;
	
	@Resource(name="uploadPath")
	private String uploadPath;
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public void listGET(@ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model) throws Exception {
		
		logger.info("펫쇼핑몰의 list()호출..........");
		logger.info(sCriteria.toString());
		
		// 리스트 넘기기
		model.addAttribute("list", productService.listProductCriteria(sCriteria));																																											
		
		PagingMaker pagingMaker = new PagingMaker();
		pagingMaker.setCri(sCriteria);
		pagingMaker.setTotalData(productService.countProductData(sCriteria));
		
		model.addAttribute("pagingMaker", pagingMaker);
	}
	
	// 상품 등록페이지
	@RequestMapping(value="/insert", method= RequestMethod.GET)  
	public void insertGET(ProductVO productVO, Model model) throws Exception {
		logger.info("펫쇼핑몰의 insertGET() 호출............");
		
		// 블로그 보고 아래 세 줄 추가
		List<ProductCategoryVO> productCategory = null;
		productCategory = productService.listCategory();
		model.addAttribute("category", JSONArray.fromObject(productCategory)); // productCategory를 JSON타입으로 변경후 category 
	}
	
	
	// 상품 등록처리
	@RequestMapping(value="/insert", method= RequestMethod.POST)
	public String insertPOST(ProductVO productVO, RedirectAttributes reAttributes,
			MultipartFile file) throws Exception {
		
		logger.info("펫쇼핑몰의 insertPOST() 호출..........");
		logger.info("productVO.toString한 글 내용: " + productVO.toString());
		
		String imgUploadPath = uploadPath + File.separator + "imgUpload";
		String ymdPath = UploadFileUtils.calcPath(imgUploadPath);
		String fileName = null;

		if(file != null) {
		 fileName =  UploadFileUtils.fileUpload(imgUploadPath, file.getOriginalFilename(), file.getBytes(), ymdPath); 
		} else {
		 fileName = uploadPath + File.separator + "images" + File.separator + "none.png";
		}

		productVO.setProductUrl(File.separator + "imgUpload" + ymdPath + File.separator + fileName);
		productVO.setProductThumbImg(File.separator + "imgUpload" + ymdPath + File.separator + "s" + File.separator + "s_" + fileName);
	
		productService.insert(productVO); // 상품 등록 처리

		reAttributes.addFlashAttribute("result", "insertSuccess");
		
		return "redirect:/petmall/search/list";
	}
	
	// 상품조회하기
	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public void read(@RequestParam("productNo") int productNo, @ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model) throws Exception {
		
		logger.info("펫쇼핑몰의 read() 호출..........");
		
		//model.addAttribute("productVO", productService.read(productNo)); 
		ProductViewVO productViewVO = productService.productView(productNo);
		model.addAttribute("productVO", productViewVO);
	}
	
	// 상품 수정 페이지
	@RequestMapping(value = "/update", method = RequestMethod.GET) 
	public void updateGET(@RequestParam("productNo")int productNo, @ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model) throws Exception { 
		logger.info("펫쇼핑몰의 updateGET() 호출............");
		logger.info(sCriteria.toString());
		
		// model.addAttribute(productService.read(productNo));
		ProductViewVO productViewVO = productService.productView(productNo);
		model.addAttribute("productVO", productViewVO);
		
		List<ProductCategoryVO> productCategory = null;
		productCategory = productService.listCategory();
		model.addAttribute("category", JSONArray.fromObject(productCategory));
	}

	// 상품 수정 처리
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updatePOST(ProductVO productVO, SearchingCriteria sCriteria, RedirectAttributes reAttributes,
						MultipartFile file, HttpServletRequest request) throws Exception {
		logger.info("펫쇼핑몰의 updatePOST() 호출............");
		
		 // 새로운 파일이 등록되었는지 확인
		 if(file.getOriginalFilename() != null && file.getOriginalFilename() != "") {
			  // 기존 파일을 삭제
			  new File(uploadPath + request.getParameter("productUrl")).delete();
			  new File(uploadPath + request.getParameter("productThumbImg")).delete();
			  
			  // 새로 첨부한 파일을 등록
			  String imgUploadPath = uploadPath + File.separator + "imgUpload";
			  String ymdPath = UploadFileUtils.calcPath(imgUploadPath);
			  String fileName = UploadFileUtils.fileUpload(imgUploadPath, file.getOriginalFilename(), file.getBytes(), ymdPath);
			  
			  productVO.setProductUrl(File.separator + "imgUpload" + ymdPath + File.separator + fileName);
			  productVO.setProductThumbImg(File.separator + "imgUpload" + ymdPath + File.separator + "s" + File.separator + "s_" + fileName);
				
		 } else {  // 새로운 파일이 등록되지 않았다면
			  // 기존 이미지를 그대로 사용
			 productVO.setProductUrl(request.getParameter("productUrl"));
			 productVO.setProductThumbImg(request.getParameter("productThumbImg"));
			  
		 }
		
		
		productService.update(productVO);  // 수정 처리

		reAttributes.addAttribute("page", sCriteria.getPage());
		reAttributes.addAttribute("numPerPage", sCriteria.getNumPerPage());
		reAttributes.addAttribute("searchingCategory", sCriteria.getSearchingCategory());
		
		reAttributes.addAttribute("keyword", sCriteria.getKeyword());
		
		reAttributes.addFlashAttribute("result", "updateSuccess");
		
		logger.info(reAttributes.toString());

		return "redirect:/petmall/search/list";
	}
	
	// 상품 삭제하기 
	@RequestMapping(value = "/delete", method = RequestMethod.POST) // read.jsp에서 POST방식으로 보냈기떄문에
	public String deletePOST(@RequestParam("productNo") int productNo, SearchingCriteria sCriteria, RedirectAttributes reAttributes) throws Exception { 
		
		logger.info("펫쇼핑몰의 deletePOST()호출..........");
		
		productService.delete(productNo);
		
		reAttributes.addAttribute("page", sCriteria.getPage());
		reAttributes.addAttribute("numPerPage", sCriteria.getNumPerPage());
		reAttributes.addAttribute("searchingCategory", sCriteria.getSearchingCategory());
		reAttributes.addAttribute("keyword", sCriteria.getKeyword());
		
		reAttributes.addFlashAttribute("result", "deleteSuccess");
		
		return "redirect:/petmall/search/list";
	}
	
/* 카테고리별 상품 CRUD*/	
	
	// 카테고리별 상품 리스트
	@RequestMapping(value="listByCategory", method=RequestMethod.GET)
	public void getListByCategory(@RequestParam("c") int cateCode, @RequestParam("l") int level, 
			Model model, @ModelAttribute("sCriteria") SearchingCriteria sCriteria, HttpServletRequest request) throws Exception {
		logger.info("카테고리별 전체상품리스트 호출..........");
		
		model.addAttribute("list", productService.listProductByCategory(cateCode, sCriteria));
		
		PagingMaker pagingMaker = new PagingMaker();
		
		String c = request.getParameter("c");
		String l = request.getParameter("l");

		pagingMaker.setCri(sCriteria);
		pagingMaker.setCateCodeLevel(c, l);
		pagingMaker.setTotalData(productService.countProductDataByCategory(cateCode, sCriteria));
		
		model.addAttribute("pagingMaker", pagingMaker);
	}
	
	// 상품조회하기
	@RequestMapping(value = "/readByCategory", method = RequestMethod.GET)
	public void readByCategory(@RequestParam("productNo") int productNo, @ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model,
			@RequestParam("c") int cateCode, @RequestParam("l") int level, HttpServletRequest request) throws Exception {
		
		logger.info("펫쇼핑몰의 read() 호출..........");
		
		ProductViewVO productViewVO = productService.productView(productNo);
		model.addAttribute("productVO", productViewVO);
		
		String c = request.getParameter("c");
		String l = request.getParameter("l");
		
		model.addAttribute("cateCode", c);
		model.addAttribute("level", l);
	}
		
}
