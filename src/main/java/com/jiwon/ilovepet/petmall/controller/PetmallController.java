package com.jiwon.ilovepet.petmall.controller;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.petmall.service.ProductService;
import com.jiwon.ilovepet.petmall.vo.ProductVO;

@Controller
@RequestMapping("/petmall/*")
public class PetmallController {
	
	private static final Logger logger = LoggerFactory.getLogger(PetmallController.class);
	
	@Inject
	private ProductService productService;
	
	// 상품목록 가져오기 (페이징처리X)
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public void list(Model model) throws Exception {
		logger.info("펫쇼핑몰의 list()호출..........");
		model.addAttribute("list", productService.list());
	}
	
	// 상품 등록페이지
	@RequestMapping(value="/insert", method= RequestMethod.GET)  
	public void insertGET(ProductVO productVO, Model model) {
		logger.info("펫쇼핑몰의 insertGET() 호출............");
	}
	
	// 상품 등록페처리
	@RequestMapping(value="/insert", method= RequestMethod.POST)
	public String insertPOST(ProductVO productVO, RedirectAttributes reAttributes) throws Exception {
		
		logger.info("펫쇼핑몰의 insertPOST() 호출..........");
		logger.info("productVO.toString한 글 내용: " + productVO.toString());
		
		productService.insert(productVO);

		reAttributes.addFlashAttribute("result", "insertSuccess");
		
		return "redirect:/petmall/list";
	}
	
	// 상품조회하기
	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public void read(@RequestParam("productNo") int productNo, Model model) throws Exception {
		
		logger.info("펫쇼핑몰의 read() 호출..........");
		
		model.addAttribute("productVo", productService.read(productNo)); 
	}
	
	// 상품 수정 페이지
	@RequestMapping(value = "/update", method = RequestMethod.GET) 
	public void updateGET(@RequestParam("productNo")int productNo, Model model) throws Exception { 
		logger.info("펫쇼핑몰의 updateGET() 호출............");
		
		model.addAttribute(productService.read(productNo));
	}

	// 상품 수정 처리
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updatePOST(ProductVO productVO, RedirectAttributes reAttributes) throws Exception {
		logger.info("펫쇼핑몰의 updatePOST() 호출............");
		
		productService.update(productVO);
		
		reAttributes.addFlashAttribute("result", "modifySuccess");

		return "redirect:/petmall/list";
	}
	
	// 상품 삭제하기 
	@RequestMapping(value = "/delete", method = RequestMethod.POST) // read.jsp에서 POST방식으로 보냈기떄문에
	public String deletePOST(@RequestParam("productNo") int productNo, RedirectAttributes reAttributes) throws Exception { 
		
		logger.info("펫쇼핑몰의 deletePOST()호출..........");
		
		productService.delete(productNo);
		
		reAttributes.addFlashAttribute("result", "deleteSuccess");
		
		return "redirect:/petmall/list";
	}
	
}
