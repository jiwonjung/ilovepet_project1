package com.jiwon.ilovepet.petmall.controller;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jiwon.ilovepet.petmall.service.CartService;
import com.jiwon.ilovepet.petmall.service.OrderService;
import com.jiwon.ilovepet.petmall.vo.OrderDetailVO;
import com.jiwon.ilovepet.petmall.vo.OrderListVO;
import com.jiwon.ilovepet.petmall.vo.OrderVO;
import com.jiwon.ilovepet.user.vo.UserVO;

@Controller
@RequestMapping("/petmall/cart/*")
public class OrderController {
	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
	
	@Inject
	private OrderService orderService;
	
	@Inject
	private CartService cartServcie;
	
	// 주문 하기
	@Transactional
	@RequestMapping(value="/order", method=RequestMethod.POST)
	public String order(HttpSession session, OrderVO orderVO, OrderDetailVO orderDetailVO, RedirectAttributes reAttributes) throws Exception {
		logger.info("order() 호출...........");
		UserVO user = (UserVO)session.getAttribute("login");
		String userId = user.getUserId();
		
		// 캘린더 호출
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);  // 연도 추출
		String ym = year + new DecimalFormat("00").format(cal.get(Calendar.MONTH) + 1);  // 월 추출
		String ymd = ym +  new DecimalFormat("00").format(cal.get(Calendar.DATE));  // 일 추출
		String subNum = "";  // 랜덤 숫자를 저장할 문자열 변수
		
		for(int i = 1; i <= 6; i ++) {  // 6회 반복
			subNum += (int)(Math.random() * 10);  // 0~9까지의 숫자를 생성하여 subNum에 저장
		}
		
		String orderNo = ymd + "_" + subNum;
		
		orderVO.setOrderNo(orderNo);
		orderVO.setUserId(userId);
		
		orderService.orderInfo(orderVO); // 주문하기
		
		orderDetailVO.setOrderNo(orderNo);
		orderDetailVO.setUserId(userId);
		
		orderService.orderInfo_Details(orderDetailVO);  // 이게 이상함!!!! 
		
		cartServcie.deleteAllCart(userId); // 주문 테이블, 주문 상세 테이블에 데이터를 전송하고, 카트 비우기
		
		reAttributes.addFlashAttribute("result", "orderSuccess");
		
		return "redirect:/petmall/cart/orderList";
	}
	
	// 특정 유저의 주문 목록
	@RequestMapping(value="/orderList", method=RequestMethod.GET)
	public void getOrderList(HttpSession session, OrderVO orderVO, Model model) throws Exception {
		logger.info("주묵 목록getOrderList()호출.........."); 
		
		UserVO user = (UserVO)session.getAttribute("login");
		String userId = user.getUserId();
		
		orderVO.setUserId(userId);
		
		List<OrderVO> orderList = orderService.listOrder(orderVO);
		
		model.addAttribute("orderList", orderList);
	}
	
	// 주문 상세 목록
	@RequestMapping(value="/orderView", method=RequestMethod.GET)
	public void getOrderList(HttpSession session,
			@RequestParam("orderNo") String orderNo, OrderVO orderVO, Model model) throws Exception {
		logger.info("주묵 상세 목록 호출.........."); 
		
		UserVO user = (UserVO)session.getAttribute("login");
		String userId = user.getUserId();
		
		orderVO.setUserId(userId);
		orderVO.setOrderNo(orderNo);
		
		List<OrderListVO> orderView = orderService.orderView(orderVO);
		
		model.addAttribute("orderView", orderView);
	}
}
