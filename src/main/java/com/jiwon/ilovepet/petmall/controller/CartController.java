package com.jiwon.ilovepet.petmall.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jiwon.ilovepet.petmall.service.CartService;
import com.jiwon.ilovepet.petmall.service.ProductService;
import com.jiwon.ilovepet.petmall.vo.CartListVO;
import com.jiwon.ilovepet.petmall.vo.CartVO;
import com.jiwon.ilovepet.user.vo.UserVO;

@Controller
@RequestMapping("/petmall/cart/*")
public class CartController {
	
	private static final Logger logger = LoggerFactory.getLogger(CartController.class);
	
	@Inject
	private CartService cartService;
	
	// 장바구니 추가
	@ResponseBody
	@RequestMapping(value="/insert", method = RequestMethod.POST)
	public int insert(CartListVO cartListVO, HttpSession session) throws Exception{
		
		int result = 0; // 로그인 안되었을 경우
		
		UserVO user = (UserVO)session.getAttribute("login");
		System.out.println("user: " + user);
		
		if(user != null) {
			cartListVO.setUserId(user.getUserId());
			cartService.insert(cartListVO);
			result = 1; // 로그인 했을 경우
		}
		
		return result;
		/*
		String userId = (String) session.getAttribute("login");
		cartVO.setUserId(userId);
		
		// 장바구니에 기존 상품이 있는지 검사
		int count = cartService.countCart(cartVO.getProductNo(), userId);
		
		if(count == 0){		
			// 없으면 insert
			cartService.insert(cartVO);
		} else {
			// 있으면 update
			cartService.updateCartCnt(cartVO);
		}
		return "redirect:/petmall/cart/list.do"; */
	}
	
	// 장바구니 목록
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public void cartList(HttpSession session, Model model) throws Exception {
		logger.info("장바구니 리스트를 가져옵니다.");
		UserVO user = (UserVO)session.getAttribute("login");
		String userId = user.getUserId();
		
		List<CartListVO> list = cartService.list(userId);
		model.addAttribute("list", list);
	}
	/*@RequestMapping(value="list", method = RequestMethod.GET)
	public ModelAndView list(HttpSession session, ModelAndView mav) throws Exception {
		Map<String, Object> map = new HashMap<>();
		String userId = (String) session.getAttribute("userId"); // session에 저장된 userId
		
		List<CartVO> list = cartService.list(userId); // 장바구니 정보 int sumCartMoney =
		System.out.println("list.size()!!!: " + list.toString());
		int sumMoney = cartService.sumCartMoney(userId); // 장바구니 전체 금액 호출
		  
		  // 장바구니 전체 금액에 따라 배송비 구분 (배송비 (3만원 => 무료, 미만 =>2500) int deliveryFee =
		  int deliveryFee = sumMoney >= 50000 ? 0 : 2500;
		  map.put("list", list); // 장바구니 정보 저장
		  map.put("count", list.size()); // 장바구니 상품의 유무 
		  map.put("sumMoney",sumMoney); // 장바구니전체 금액 
		  map.put("deliveryFee", deliveryFee); // 배송금액
		  map.put("totalSum", sumMoney+deliveryFee); // 주문 상품 전체 금액

		  mav.setViewName("petmall/cart/list"); 
		  mav.addObject("map", map);
		 
		return mav;
	}*/
	
	
	
	// 장바구니 삭제
	@ResponseBody
	@RequestMapping(value="delete", method=RequestMethod.POST)
	public int delete(HttpSession session, CartVO cartVO,
			@RequestParam(value="checkbox[]") List<String> chArr) throws Exception {
		logger.info("장바구니 삭제");
		
		UserVO user = (UserVO)session.getAttribute("login");
		String userId = user.getUserId();
		
		int result = 0; 
		int cartNo = 0;
		
		// 로그인 여부
		if(user != null) {
			cartVO.setUserId(userId);
			
			for(String i : chArr) { // 에이젝스에서 받은 chArr의 갯수만큼 반복
				cartNo = Integer.parseInt(i);  // i번째 데이터를 cartNum에 저장
				cartVO.setCartNo(cartNo);
				cartService.delete(cartVO);
			}
			result = 1;
		}
		return result;
	}
	
	// 장바구니 수정
	@RequestMapping("update.do")
	public String update(@RequestParam int[] amount, @RequestParam int[] productNo, HttpSession session) throws Exception {
		
		String userId = (String) session.getAttribute("userId"); // session의 id
		
		for (int i = 0; i < productNo.length; i++) {
			CartVO cartVO = new CartVO();
			cartVO.setUserId(userId);
			cartVO.setAmount(amount[i]);
			cartVO.setProductNo(productNo[i]);
			
			cartService.update(cartVO);
		}
		return "redirect:/petmall/cart/list.do";
	}
}
