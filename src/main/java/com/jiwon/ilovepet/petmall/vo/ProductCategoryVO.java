package com.jiwon.ilovepet.petmall.vo;

public class ProductCategoryVO {
	/*create table product_category (
	cate_name varchar(20) not null,
	cate_code varchar(30) not null,
	cate_code_ref varchar(30) null,
	primary key(cate_code),
	foreign key(cate_code_ref) references product_category(cate_code));*/
	
	private String cateName;
	private String cateCode;
	private String cateCodeRef;
	
	private int level;
	
	
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getCateName() {
		return cateName;
	}
	public void setCateName(String cateName) {
		this.cateName = cateName;
	}
	public String getCateCode() {
		return cateCode;
	}
	public void setCateCode(String cateCode) {
		this.cateCode = cateCode;
	}
	public String getCateCodeRef() {
		return cateCodeRef;
	}
	public void setCateCodeRef(String cateCodeRef) {
		this.cateCodeRef = cateCodeRef;
	}
	@Override
	public String toString() {
		return "ProductCategoryVO [cateName=" + cateName + ", cateCode=" + cateCode + ", cateCodeRef=" + cateCodeRef
				+ ", level=" + level + "]";
	}
	
}
