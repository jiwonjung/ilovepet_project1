package com.jiwon.ilovepet.petmall.vo;

import java.util.Date;

/*Product 테이블과 Product_Category 테이블을 조인하여 가져오는 값을 
 현재 VO로 만듬 */
public class ProductViewVO {
	
	private Integer productNo;  		// 상품번호
	private String productName; 		// 상품이름
	private String cateCode;			// 상품 코드 (100[101,102,103], 200[201,202, etc], 300, 400
	private Integer productPrice; 		// 상품가격
	private Integer productStock;		// 상품 제고
	private String productDetails; 		// 상품 상세정보
	private String productUrl;			// 상품이미지 경로
	private int reviewCnt;				// 상품평 댓글 수
	private Date regdate;			    // 등록일
	private Date moddate;				// 수정일
	
	private String cateCodeRef;
	private String cateName;
	
	private String productThumbImg; // 썸네일 이미지경로
	
	public String getProductThumbImg() {
		return productThumbImg;
	}
	public void setProductThumbImg(String productThumbImg) {
		this.productThumbImg = productThumbImg;
	}
	public Integer getProductNo() {
		return productNo;
	}
	public void setProductNo(Integer productNo) {
		this.productNo = productNo;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getCateCode() {
		return cateCode;
	}
	public void setCateCode(String cateCode) {
		this.cateCode = cateCode;
	}
	public Integer getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}
	public Integer getProductStock() {
		return productStock;
	}
	public void setProductStock(Integer productStock) {
		this.productStock = productStock;
	}
	public String getProductDetails() {
		return productDetails;
	}
	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}
	public String getProductUrl() {
		return productUrl;
	}
	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}
	public int getReviewCnt() {
		return reviewCnt;
	}
	public void setReviewCnt(int reviewCnt) {
		this.reviewCnt = reviewCnt;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModdate() {
		return moddate;
	}
	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}
	public String getCateCodeRef() {
		return cateCodeRef;
	}
	public void setCateCodeRef(String cateCodeRef) {
		this.cateCodeRef = cateCodeRef;
	}
	public String getCateName() {
		return cateName;
	}
	public void setCateName(String cateName) {
		this.cateName = cateName;
	}
	
	@Override
	public String toString() {
		return "ProductViewVO [productNo=" + productNo + ", productName=" + productName + ", cateCode=" + cateCode
				+ ", productPrice=" + productPrice + ", productStock=" + productStock + ", productDetails="
				+ productDetails + ", productUrl=" + productUrl + ", reviewCnt=" + reviewCnt + ", regdate=" + regdate
				+ ", moddate=" + moddate + ", cateCodeRef=" + cateCodeRef + ", cateName=" + cateName + "]";
	}
}
