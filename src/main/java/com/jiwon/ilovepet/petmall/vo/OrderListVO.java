package com.jiwon.ilovepet.petmall.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

// order테이블, order상세 테이블, product태이블 3개를 조인하여 모든 정보가 표시될 수 있도록 합친 class작성
public class OrderListVO {
	private String orderNo;
	private String userId;
	private String orderReceiver;
	private String userAddcode;
	private String userAddress1;
	private String userAddress2;
	private String orderPhone;
	private int orderAmount;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd:ss")
	private Date orderdate;
	private String delivery;
	
	private int orderDetailsNo;
	private Integer productNo;
	private int amount;  // 카트에 담은 해당 상품 수량
	
	private String productName; 		// 상품이름
	private String productThumbImg;
	private Integer productPrice; 		// 상품가격
	
	
	public String getDelivery() {
		return delivery;
	}
	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOrderReceiver() {
		return orderReceiver;
	}
	public void setOrderReceiver(String orderReceiver) {
		this.orderReceiver = orderReceiver;
	}
	public String getUserAddcode() {
		return userAddcode;
	}
	public void setUserAddcode(String userAddcode) {
		this.userAddcode = userAddcode;
	}
	public String getUserAddress1() {
		return userAddress1;
	}
	public void setUserAddress1(String userAddress1) {
		this.userAddress1 = userAddress1;
	}
	public String getUserAddress2() {
		return userAddress2;
	}
	public void setUserAddress2(String userAddress2) {
		this.userAddress2 = userAddress2;
	}
	public String getOrderPhone() {
		return orderPhone;
	}
	public void setOrderPhone(String orderPhone) {
		this.orderPhone = orderPhone;
	}
	public int getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
	}
	public Date getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}
	public int getOrderDetailsNo() {
		return orderDetailsNo;
	}
	public void setOrderDetailsNo(int orderDetailsNo) {
		this.orderDetailsNo = orderDetailsNo;
	}
	
	public Integer getProductNo() {
		return productNo;
	}
	public void setProductNo(Integer productNo) {
		this.productNo = productNo;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductThumbImg() {
		return productThumbImg;
	}
	public void setProductThumbImg(String productThumbImg) {
		this.productThumbImg = productThumbImg;
	}
	public Integer getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}
	@Override
	public String toString() {
		return "OrderListVO [orderNo=" + orderNo + ", userId=" + userId + ", orderReceiver=" + orderReceiver
				+ ", userAddcode=" + userAddcode + ", userAddress1=" + userAddress1 + ", userAddress2=" + userAddress2
				+ ", orderPhone=" + orderPhone + ", orderAmount=" + orderAmount + ", orderdate=" + orderdate
				+ ", orderDetailsNo=" + orderDetailsNo + ", productNo=" + productNo + ", amount=" + amount
				+ ", productName=" + productName + ", productThumbImg=" + productThumbImg + ", productPrice="
				+ productPrice + ", delivery=" + delivery + "]";
	}
	
}
