package com.jiwon.ilovepet.petmall.vo;

import java.util.Date;

public class CartListVO {
	/*create table cart (
	cart_no int not null auto_increment,
	user_id varchar(50) not null,
	product_no int not null,
	amount int default 0,
	add_date TIMESTAMP not null default now(),
	primary key(cart_no, user_id)
);*/
	
	private Integer cartNo; // 장바구니 번호 
	private String userId;  // 사용자 아이디
	private Integer productNo; // 상품 번호
	private int amount; // 장바구니에 담긴 수량 (cartStock)
	private Date addDate;
	
	//private String userName;  // 사용자 이름
	private int num; 
	private String productName; // 상품 이름 
	private Integer productPrice; // 상품 가격
	private String productUrl; // 상품 가격
	
	public Integer getCartNo() {
		return cartNo;
	}
	public void setCartNo(Integer cartNo) {
		this.cartNo = cartNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getProductNo() {
		return productNo;
	}
	public void setProductNo(Integer productNo) {
		this.productNo = productNo;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Date getAddDate() {
		return addDate;
	}
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}
	public String getProductUrl() {
		return productUrl;
	}
	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}
	@Override
	public String toString() {
		return "CartListVO [cartNo=" + cartNo + ", userId=" + userId + ", productNo=" + productNo + ", amount=" + amount
				+ ", addDate=" + addDate + ", num=" + num + ", productName=" + productName + ", productPrice="
				+ productPrice + ", productUrl=" + productUrl + "]";
	}
}
