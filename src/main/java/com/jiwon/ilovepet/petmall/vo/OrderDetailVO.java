package com.jiwon.ilovepet.petmall.vo;

public class OrderDetailVO {
/*create table tbl_order_details (
    orderDetails_no int not null auto_increment,
    order_no varchar(50) not null,
    product_no int not null,
    amount int not null,
    primary key(orderDetails_no)
);*/
	
	private int orderDetailsNo;
	private String orderNo;
	private Integer productNo;
	private int amount;
	private String userId;
	
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getOrderDetailsNo() {
		return orderDetailsNo;
	}
	public void setOrderDetailsNo(int orderDetailsNo) {
		this.orderDetailsNo = orderDetailsNo;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	public Integer getProductNo() {
		return productNo;
	}
	public void setProductNo(Integer productNo) {
		this.productNo = productNo;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "OrderDetailVO [orderDetailsNo=" + orderDetailsNo + ", orderNo=" + orderNo + ", productNo=" + productNo
				+ ", amount=" + amount + ", userId=" + userId + "]";
	}
	
}
