package com.jiwon.ilovepet.petmall.vo;

import java.util.Date;

public class OrderVO {
	/*create table tbl_order (
	order_no varchar(50) not null,
	user_id varchar(50) not null,
	order_receiver varchar(50) not null,
	user_addcode varchar(50) not null,
	user_address1 varchar(100) not null,
	user_address2 varchar(50) not null,
	order_phone varchar(30) not null,
	order_amount int not null,
	orderdate timestamp not null default now(),
	primary key(order_no) );*/

	private String orderNo;
	private String userId;
	private String orderReceiver;
	private String userAddcode;
	private String userAddress1;
	private String userAddress2;
	private String orderPhone;
	private int orderAmount;
	private Date orderdate;
	
	private String delivery;
	
	
	public String getDelivery() {
		return delivery;
	}
	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOrderReceiver() {
		return orderReceiver;
	}
	public void setOrderReceiver(String orderReceiver) {
		this.orderReceiver = orderReceiver;
	}
	public String getUserAddcode() {
		return userAddcode;
	}
	public void setUserAddcode(String userAddcode) {
		this.userAddcode = userAddcode;
	}
	public String getUserAddress1() {
		return userAddress1;
	}
	public void setUserAddress1(String userAddress1) {
		this.userAddress1 = userAddress1;
	}
	public String getUserAddress2() {
		return userAddress2;
	}
	public void setUserAddress2(String userAddress2) {
		this.userAddress2 = userAddress2;
	}
	public String getOrderPhone() {
		return orderPhone;
	}
	public void setOrderPhone(String orderPhone) {
		this.orderPhone = orderPhone;
	}
	public int getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
	}
	public Date getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}
	
	@Override
	public String toString() {
		return "OrderVO [orderNo=" + orderNo + ", userId=" + userId + ", orderReceiver=" + orderReceiver
				+ ", userAddcode=" + userAddcode + ", userAddress1=" + userAddress1 + ", userAddress2=" + userAddress2
				+ ", orderPhone=" + orderPhone + ", orderAmount=" + orderAmount + ", orderdate=" + orderdate
				+ ", delivery=" + delivery + "]";
	}
}
