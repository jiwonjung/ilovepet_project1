package com.jiwon.ilovepet.petmall.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ProductReviewVO {
	/*create table product_review (
	review_no int not null auto_increment,
	product_no int not null,
	user_id varchar(50) not null,
	review_content varchar(1000) not null,
	review_score INT(11) DEFAULT '0',
	regdate TIMESTAMP not null default now(),
	moddate TIMESTAMP not null default now(),
	primary key(review_no, product_no)
);*/
	private Integer reviewNo;
	private Integer productNo;
	private String userId;
	private String reviewContent;
	private Integer reviewScore;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd:ss")
	private Date regdate;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd:ss")
	private Date moddate;
	
	public Integer getReviewNo() {
		return reviewNo;
	}
	public void setReviewNo(Integer reviewNo) {
		this.reviewNo = reviewNo;
	}
	public Integer getProductNo() {
		return productNo;
	}
	public void setProductNo(Integer productNo) {
		this.productNo = productNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getReviewContent() {
		return reviewContent;
	}
	public void setReviewContent(String reviewContent) {
		this.reviewContent = reviewContent;
	}
	public Integer getReviewScore() {
		return reviewScore;
	}
	public void setReviewScore(Integer reviewScore) {
		this.reviewScore = reviewScore;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModdate() {
		return moddate;
	}
	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}
	@Override
	public String toString() {
		return "ProductReviewVO [reviewNo=" + reviewNo + ", productNo=" + productNo + ", userId=" + userId
				+ ", reviewContent=" + reviewContent + ", reviewScore=" + reviewScore + ", regdate=" + regdate
				+ ", moddate=" + moddate + "]";
	}
}
