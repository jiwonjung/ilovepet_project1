package com.jiwon.ilovepet.petmall.vo;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class ProductVO {
/*create table product (
	product_no int not null auto_increment,
	product_name varchar(60),
	cateCode varchar(30),
	product_price integer default 0,
	product_stock int(11),
	product_details varchar(500),
	product_url varchar(500),
	product_thumbImg varchar(200),
	reviewCnt int default 0,
	regdate TIMESTAMP not null default now(),
	moddate TIMESTAMP not null default now(),
	primary key(product_no)
);*/
	
	private Integer productNo;  		// 상품번호
	private String productName; 		// 상품이름
	private String cateCode;			// 상품 코드 (100[101,102,103], 200[201,202, etc], 300, 400
	private Integer productPrice; 		// 상품가격
	private Integer productStock;		// 상품 제고
	private String productDetails; 		// 상품 상세정보
	private String productUrl;			// 상품이미지 경로
	private String productThumbImg;
	private int reviewCnt;				// 상품평 댓글 수
	private Date regdate;			    // 등록일
	private Date moddate;				// 수정일
	
	private MultipartFile productPhoto;	// 상품이미지 파일
	
	
	public String getProductThumbImg() {
		return productThumbImg;
	}
	public void setProductThumbImg(String productThumbImg) {
		this.productThumbImg = productThumbImg;
	}
	public Integer getProductNo() {
		return productNo;
	}
	public void setProductNo(Integer productNo) {
		this.productNo = productNo;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}
	public String getProductDetails() {
		return productDetails;
	}
	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}
	public String getProductUrl() {
		return productUrl;
	}
	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}
	public MultipartFile getProductPhoto() {
		return productPhoto;
	}
	public void setProductPhoto(MultipartFile productPhoto) {
		this.productPhoto = productPhoto;
	}
	public int getReviewCnt() {
		return reviewCnt;
	}
	public void setReviewCnt(int reviewCnt) {
		this.reviewCnt = reviewCnt;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	
	public Date getModdate() {
		return moddate;
	}
	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}
	public String getCateCode() {
		return cateCode;
	}
	public void setCateCode(String cateCode) {
		this.cateCode = cateCode;
	}
	public Integer getProductStock() {
		return productStock;
	}
	public void setProductStock(Integer productStock) {
		this.productStock = productStock;
	}
	@Override
	public String toString() {
		return "ProductVO [productNo=" + productNo + ", productName=" + productName + ", cateCode=" + cateCode
				+ ", productPrice=" + productPrice + ", productStock=" + productStock + ", productDetails="
				+ productDetails + ", productUrl=" + productUrl + ", productThumbImg=" + productThumbImg
				+ ", reviewCnt=" + reviewCnt + ", regdate=" + regdate + ", moddate=" + moddate + ", productPhoto="
				+ productPhoto + "]";
	}
}
