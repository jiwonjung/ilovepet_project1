package com.jiwon.ilovepet.petmall.vo;

import java.util.Date;

public class CartVO {
	/*create table cart (
	cart_no int not null auto_increment,
	user_id varchar(50) not null,
	product_no int not null,
	amount int default 0,
	add_date TIMESTAMP not null default now(),
	primary key(cart_no, user_id)
);*/
	
	private Integer cartNo; // 장바구니 번호 
	private String userId;  // 사용자 아이디
	private Integer productNo; // 상품 번호
	private int amount; // 구매 수량
	private Date addDate;
	
	public Integer getCartNo() {
		return cartNo;
	}
	public void setCartNo(Integer cartNo) {
		this.cartNo = cartNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getProductNo() {
		return productNo;
	}
	public void setProductNo(Integer productNo) {
		this.productNo = productNo;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Date getAddDate() {
		return addDate;
	}
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}
	@Override
	public String toString() {
		return "CartVO [cartNo=" + cartNo + ", userId=" + userId + ", productNo=" + productNo + ", amount=" + amount
				+ ", addDate=" + addDate + "]";
	}
}
