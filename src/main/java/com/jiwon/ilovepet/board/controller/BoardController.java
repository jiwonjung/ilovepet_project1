package com.jiwon.ilovepet.board.controller;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jiwon.ilovepet.board.service.BoardService;
import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.PagingMaker;

@Controller
@RequestMapping("/bbs/*")
public class BoardController {
	
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
	
	@Inject
	private BoardService boardService;
	
	// 글 입력페이지
	@RequestMapping(value="/write", method= RequestMethod.GET)  // localhost:8080/bbs/write
	public void writeGET(BoardVO boardVO, Model model) {
		logger.info("writeGET 호출............");
	}
	
	// 글 입력처리
	@RequestMapping(value="/write", method= RequestMethod.POST)
	public String writePOST(BoardVO boardVO, RedirectAttributes reAttributes) throws Exception {
		
		logger.info("writePOST 호출..........");
		logger.info("boardVO.toString한 글 내용: " + boardVO.toString());
		
		boardService.write(boardVO);

		reAttributes.addFlashAttribute("result", "writeSuccess");
		
		return "redirect:/bbs/list";
		//return "/bbs/resultOK";
	}
	
	// 글 조회하기
	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public void read(@RequestParam("boardNo") int boardNo, Model model) throws Exception {
		
		logger.info("read() 호출..........");
		
		model.addAttribute("boardVO", boardService.read(boardNo)); 
	}
	
	// 글 조회하기 (+ 페이징 처리)
	@RequestMapping(value = "/readPaging", method = RequestMethod.GET)
	public void readPaging(@RequestParam("boardNo") int boardNo, 
			@ModelAttribute("pCriteria") PagingCriteria pCriteria, Model model) throws Exception {
		
		logger.info("readPaging() 호출..........");
		
		model.addAttribute("boardVO", boardService.read(boardNo)); 
	}
	
	// 글 삭제처리
	@RequestMapping(value = "/delete", method = RequestMethod.POST) // read.jsp에서 POST방식으로 보냈기떄문에
	public String deletePOST(@RequestParam("boardNo") int boardNo, RedirectAttributes reAttributes) throws Exception { 
		
		logger.info("deletePOST()호출..........");
		
		boardService.delete(boardNo);
		
		reAttributes.addFlashAttribute("result", "deleteSuccess");
		return "redirect:/bbs/list";
	}
	
	// 글 삭제처리 (+ 페이징 처리)
	@RequestMapping(value = "/deletePaging", method = RequestMethod.POST) // read.jsp에서 POST방식으로 보냈기떄문에
	public String deletePagingPOST(@RequestParam("boardNo") int boardNo, 
			PagingCriteria pCriteria, RedirectAttributes reAttributes) throws Exception { 
		
		logger.info("deletePagingPOST() 호출..........");
		
		boardService.delete(boardNo);
		
		reAttributes.addAttribute("page", pCriteria.getPage());
		reAttributes.addAttribute("numPerPage", pCriteria.getNumPerPage());
		
		reAttributes.addFlashAttribute("result", "deleteSuccess");
		return "redirect:/bbs/pagingList";
	}
	
	// 글 수정페이지
	@RequestMapping(value = "/update", method = RequestMethod.GET) 
	public void updateGET(@RequestParam("boardNo")int boardNo, Model model) throws Exception { 
		
		logger.info("updateGET() 호출............");
		
		model.addAttribute(boardService.read(boardNo));
	}

	// 글 수정처리
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updatePOST(BoardVO boardVO, RedirectAttributes reAttributes) throws Exception {
		logger.info("updatePOST() 호출............");
		
		boardService.update(boardVO);
		
		reAttributes.addFlashAttribute("result", "modifySuccess");

		return "redirect:/bbs/list";
	}
	
	// 글 수정페이지 (+ 페이징 처리)
	@RequestMapping(value = "/updatePaging", method = RequestMethod.GET) 
	public void updatePagingGET(@RequestParam("boardNo")int boardNo, 
			@ModelAttribute("pCriteria") PagingCriteria pCriteria, Model model) throws Exception { 
		
		logger.info("updatePagingGET() 호출............");
		
		model.addAttribute(boardService.read(boardNo));
	}
	
	// 글 수정처리 (+ 페이징 처리)
	@RequestMapping(value = "/updatePaging", method = RequestMethod.POST)
	public String updatePagingPOST(BoardVO boardVO, 
			PagingCriteria pCriteria, RedirectAttributes reAttributes) throws Exception {
		
		logger.info("updatePagingPOST() 호출............");
		
		boardService.update(boardVO);
		
		reAttributes.addAttribute("page", pCriteria.getPage());
		reAttributes.addAttribute("numPerPage", pCriteria.getNumPerPage());
		
		reAttributes.addFlashAttribute("result", "modifySuccess");
		
		return "redirect:/bbs/pagingList";
	}
	
	// 글목록 가져오기 (페이징처리X)
	@RequestMapping(value="/list", method= RequestMethod.GET)
	public void list(Model model) throws Exception {
		
		logger.info("글목록 (list)가져오기 호출..........");
		
		model.addAttribute("list", boardService.list());
	}	
	
	// 글목록 가져오기 (+ 페이징 처리 테스트) 
	@RequestMapping(value="/pagingListTest", method= {RequestMethod.GET, RequestMethod.POST})
	public void pagingListTest(PagingCriteria pCriteria, Model model) throws Exception { // PageCriteria 타입을 보고 자동으로 저 객체를 생성해냄
		logger.info("pagingListTest() 호출............");
		
		model.addAttribute("list", boardService.listPagingCriteria(pCriteria)); 
	}
	
	// 글목록 가져오기 (+ 페이징 처리) 
	@RequestMapping(value="/pagingList", method=RequestMethod.GET)
	public void pagingList(PagingCriteria pCriteria, Model model) throws Exception { // PageCriteria 타입을 보고 자동으로 저 객체를 생성해냄
		logger.info("pagingList() 호출............");
		logger.info(pCriteria.toString());
		
		// 리스트 넘기기
		model.addAttribute("list", boardService.listPagingCriteria(pCriteria)); 
		
		// 페이징 처리한 결과 넘기기
		PagingMaker pagingMaker = new PagingMaker();
		pagingMaker.setCri(pCriteria);
		pagingMaker.setTotalData(boardService.countPagingData(pCriteria));
		
		model.addAttribute("pagingMaker", pagingMaker);
	}
}
