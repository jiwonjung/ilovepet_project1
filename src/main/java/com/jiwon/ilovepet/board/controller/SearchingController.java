package com.jiwon.ilovepet.board.controller;

import java.io.File;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jiwon.ilovepet.board.service.BoardService;
import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingMaker;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;
import com.jiwon.ilovepet.commons.utils.UploadFileUtils;

@Controller
@RequestMapping("/bbs/search/*")
public class SearchingController {
	
	private static final Logger logger = LoggerFactory.getLogger(SearchingController.class);
	
	@Inject
	private BoardService boardService;
	
	@Resource(name="uploadPath")
	private String uploadPath;
	
	// 전체 목록보기
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public void listGET(@ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model) throws Exception {
		
		logger.info("listGET() 호출..........");
		logger.info(sCriteria.toString());
		
		// 리스트 넘기기
		model.addAttribute("list", boardService.listSearchingCriteria(sCriteria));																																											
		
		PagingMaker pagingMaker = new PagingMaker();
		pagingMaker.setCri(sCriteria);
		pagingMaker.setTotalData(boardService.countSearchingData(sCriteria));
		
		model.addAttribute("pagingMaker", pagingMaker);
	}

	// 조회하기
	@RequestMapping(value="/read", method=RequestMethod.GET)
	public void read(@RequestParam("boardNo") int boardNo, @ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model) throws Exception {
		
		logger.info("read() 호출..........");
		model.addAttribute(boardService.read(boardNo));
	}
	
	// 삭제처리
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String delete(@RequestParam("boardNo") int boardNo, SearchingCriteria sCriteria, RedirectAttributes reAttributes) throws Exception {
		
		logger.info("delete() 호출..........");
		
		boardService.delete(boardNo);
		
		reAttributes.addAttribute("page", sCriteria.getPage());
		reAttributes.addAttribute("numPerPage", sCriteria.getNumPerPage());
		reAttributes.addAttribute("searchingCategory", sCriteria.getSearchingCategory());
		reAttributes.addAttribute("keyword", sCriteria.getKeyword());
		
		reAttributes.addFlashAttribute("result", "deleteSuccess");
		
		return "redirect:/bbs/search/list";
	}
	
	// 수정 페이지 이동
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public void updateGET(int boardNo, @ModelAttribute("sCriteria") SearchingCriteria sCriteria, Model model) throws Exception {
		
		logger.info("updateGET() 호출..........");
		logger.info(sCriteria.toString());
		model.addAttribute(boardService.read(boardNo));
	}
	
	// 수정 처리 
	@RequestMapping(value="/update", method=RequestMethod.POST) 
	public String updateyPOST(BoardVO boardVO, SearchingCriteria sCriteria, RedirectAttributes reAttributes,
				HttpServletRequest req, MultipartFile file) throws Exception {
		
		logger.info("updateyPOST() 호출..........");
		// 새로운 파일이 등록되었는지 확인
		 if(file.getOriginalFilename() != null && file.getOriginalFilename() != "") {
		  // 기존 파일을 삭제
		  new File(uploadPath + req.getParameter("boardImg")).delete();
		  new File(uploadPath + req.getParameter("boardThumbImg")).delete();
		  
		  // 새로 첨부한 파일을 등록
		  String imgUploadPath = uploadPath + File.separator + "imgUpload";
		  String ymdPath = UploadFileUtils.calcPath(imgUploadPath);
		  String fileName = UploadFileUtils.fileUpload(imgUploadPath, file.getOriginalFilename(), file.getBytes(), ymdPath);
		  
		  boardVO.setBoardImg(File.separator + "imgUpload" + ymdPath + File.separator + fileName);
		  boardVO.setBoardThumbImg(File.separator + "imgUpload" + ymdPath + File.separator + "s" + File.separator + "s_" + fileName);
			
		 } else {  // 새로운 파일이 등록되지 않았다면
		  // 기존 이미지를 그대로 사용
		 boardVO.setBoardImg(req.getParameter("boardImg"));
		 boardVO.setBoardThumbImg(req.getParameter("boardThumbImg"));
			
		 }
		
		boardService.update(boardVO);
		
		reAttributes.addAttribute("page", sCriteria.getPage());
		reAttributes.addAttribute("numPerPage", sCriteria.getNumPerPage());
		reAttributes.addAttribute("searchingCategory", sCriteria.getSearchingCategory());
		
		reAttributes.addAttribute("keyword", sCriteria.getKeyword());
		
		reAttributes.addFlashAttribute("result", "updateSuccess");
		
		logger.info(reAttributes.toString());
		
		return "redirect:/bbs/search/list";
	}
	
	// 글쓰기 페이지 
	@RequestMapping(value="/write", method=RequestMethod.GET)
	public void writeGET() throws Exception {
		
		logger.info("writeGET() 호출..........");
	}
	
	// 글쓰기 처리
	@RequestMapping(value="/write", method=RequestMethod.POST)
	public String writePOST(BoardVO boardVO, RedirectAttributes reAttr, MultipartFile file, HttpServletRequest request) throws Exception {
		logger.info("writePOST() 호출..........");
		logger.info("=====boardVO.toString()한 글내용: " + boardVO.toString());
		
		// 절대경로
		//String imgUploadPath = uploadPath + File.separator + "imgUpload";
		
		// 상대경로
		String imgUploadPath = request.getServletContext().getRealPath("resources/imgUpload/");
		System.out.println("imgUploadPath: " + imgUploadPath);
		
		String ymdPath = UploadFileUtils.calcPath(imgUploadPath);
		String fileName = null;

		if(file.getOriginalFilename() != null && file.getOriginalFilename() != "") {
		 fileName =  UploadFileUtils.fileUpload(imgUploadPath, file.getOriginalFilename(), file.getBytes(), ymdPath); 
		 boardVO.setBoardImg(File.separator + "imgUpload" + ymdPath + File.separator + fileName);
		 boardVO.setBoardThumbImg(File.separator + "imgUpload" + ymdPath + File.separator + "s" + File.separator + "s_" + fileName);
		 
		} else { // 첨부된 파일이 없다면
		 //fileName = uploadPath + File.separator + "images" + File.separator + "none.png";
		}

		
		boardService.write(boardVO); // 게시글 입력
		
		reAttr.addFlashAttribute("result", "writeSuccess");
		
		return "redirect:/bbs/search/list";
	}
	
}
