package com.jiwon.ilovepet.board.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

@Repository
public class BoardFileDAOImpl implements BoardFileDAO {
	
	@Inject
	private SqlSession sqlSession;

	@Override
	public void addBoardFile(String fullName) throws Exception {
		sqlSession.insert("addBoardFile", fullName);
	}

	@Override
	public List<String> getBoardFiles(Integer boardNo) throws Exception {
		return sqlSession.selectList("getBoardFiles", boardNo);
	}

	@Override
	public void deleteBoardFile(String fileName) throws Exception {
		sqlSession.delete("deleteBoardFile", fileName);
	}

	@Override
	public void deleteAllBoardFiles(Integer boardNo) throws Exception {
		sqlSession.delete("deleteAllBoardFiles", boardNo);
	}

	@Override
	public void modifyBoardFile(String fileName, Integer boardNo) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("fileName", fileName);
		map.put("boardNo", boardNo);
		sqlSession.insert("modifyBoardFile", map);
	}

	@Override
	public void updateBoardFileCnt(Integer boardNo) throws Exception {
		sqlSession.update("updateBoardFileCnt", boardNo);
	}
}
