package com.jiwon.ilovepet.board.dao;

import java.util.List;

import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;

public interface BoardDAO {
	
	public void writeBoard(BoardVO boardVO) throws Exception;
	
	public BoardVO readBoard(Integer boardNo) throws Exception;
	
	public void updateBoard(BoardVO boardVO) throws Exception;
	
	public void deleteBoard(Integer boardNo) throws Exception;
	
	public List<BoardVO> list() throws Exception; // 전체 리스트
	
	// 페이징 처리
	public List<BoardVO> listPaging(int page) throws Exception; 
	public List<BoardVO> listPagingCriteria(PagingCriteria pCriteria) throws Exception;
	public int countPagingData(PagingCriteria pCriteria) throws Exception;
	
	// 검색 처리
	public List<BoardVO> listSearchingCriteria(SearchingCriteria sCriteria) throws Exception;
	public int countSearchingData(SearchingCriteria sCriteria) throws Exception;
	
	// 게시글 조회수 변경
	public void updateHitCnt(Integer boardNo) throws Exception;
	// 게시글 댓글수 변경
	public void updateReplyCnt(Integer boardNo, int amount) throws Exception;
	
	public void updateReplyCnt2(Integer boardNo, int amount) throws Exception;

}
