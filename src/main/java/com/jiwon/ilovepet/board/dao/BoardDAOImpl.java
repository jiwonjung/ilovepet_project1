package com.jiwon.ilovepet.board.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;

@Repository
public class BoardDAOImpl implements BoardDAO {

	@Inject 
	private SqlSession sqlSession;
	
	@Override
	public void writeBoard(BoardVO boardVO) throws Exception {
		sqlSession.insert("writeBoard", boardVO);
	}

	@Override
	public BoardVO readBoard(Integer boardNo) throws Exception {
		return sqlSession.selectOne("readBoard", boardNo);
	}

	@Override
	public void updateBoard(BoardVO boardVO) throws Exception {
		sqlSession.update("updateBoard", boardVO);
	}

	@Override
	public void deleteBoard(Integer boardNo) throws Exception {
		sqlSession.delete("deleteBoard", boardNo);
	}
	
	@Override
	public List<BoardVO> list() throws Exception {
		return sqlSession.selectList("list");
	}
	
	// 페이징
	@Override
	public List<BoardVO> listPaging(int page) throws Exception {
		if (page <= 0) {
			page = 1;
		}
		page = (page - 1) * 10;
		
		return sqlSession.selectList("listPaging", page);
	}

	@Override
	public List<BoardVO> listPagingCriteria(PagingCriteria pCriteria) throws Exception {
		return sqlSession.selectList("listPagingCriteria", pCriteria);
	}
	
	@Override
	public int countPagingData(PagingCriteria pCriteria) throws Exception {
		return sqlSession.selectOne("countPagingData", pCriteria);
	}
	
	// 검색처리 + 페이징처리
	@Override
	public List<BoardVO> listSearchingCriteria(SearchingCriteria sCriteria) throws Exception {
		return sqlSession.selectList("listSearchingCriteria", sCriteria);
	}

	@Override
	public int countSearchingData(SearchingCriteria sCriteria) throws Exception {
		return sqlSession.selectOne("countSearchingData", sCriteria);
	}

	// 게시물 조회수
	@Override
	public void updateHitCnt(Integer boardNo) throws Exception {
		sqlSession.update("updateHitCnt", boardNo);
	}
	
	// 게시물 댓글수
	@Override
	public void updateReplyCnt(Integer boardNo, int amount) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("boardNo", boardNo);
		map.put("amount", amount);
		
		sqlSession.update("updateReplyCnt", map);
	}
	
	@Override
	public void updateReplyCnt2(Integer boardNo, int amount) throws Exception {
		Map<Object, Object> map = new HashMap<>();
		map.put("boardNo", boardNo);
		map.put("amount", amount);
		sqlSession.update("updateReplyCnt2", map);
		
	}

}
