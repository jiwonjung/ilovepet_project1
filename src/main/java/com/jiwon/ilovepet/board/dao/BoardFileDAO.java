package com.jiwon.ilovepet.board.dao;

import java.util.List;

public interface BoardFileDAO {
	
	// 파일추가
	public void addBoardFile(String fullName) throws Exception;
	
	// 파일 목록 
	public List<String> getBoardFiles(Integer boardNo) throws Exception;
	
	// 파일 삭제
	public void deleteBoardFile(String fileName) throws Exception;
	
	// 파일 전체 삭제
	public void deleteAllBoardFiles(Integer boardNo) throws Exception;
	
	// 파일 수정
	public void modifyBoardFile(String fileName, Integer boardNo) throws Exception;
	
	// 파일 갯수 갱신
	public void updateBoardFileCnt(Integer boardNo) throws Exception;
}
