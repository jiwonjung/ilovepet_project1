	package com.jiwon.ilovepet.board.vo;

import java.util.Arrays;
import java.util.Date;

public class BoardVO {
	/*
	 create table board (
	board_no int not null auto_increment,
	user_id varchar(50) not null, 
	board_title varchar(200) not null,
	board_content text null,
	board_category varchar(30) not null,
	hit int default 0,
	replycnt int default 0,
	filecnt int default 0,
	regdate timestamp not null default now(),
	moddate timestamp not null default now(),
	primary key(board_no)
);	  */
	
	private Integer boardNo;
	private String userId;
	private String boardTitle;
	private String boardContent;
	private String boardCategory;
	private int hit;
	private int replyCnt;
	private String boardImg;
	private String boardThumbImg;
	private Date regdate;
	private Date moddate;
	
	private String[] files; // 다수의 첨부파일 이름을 저장하기 위한 변수
	private int fileCnt;  // 첨부파일 파일의 갯수
	
	
	
	
	public String getBoardImg() {
		return boardImg;
	}
	public void setBoardImg(String boardImg) {
		this.boardImg = boardImg;
	}
	public String getBoardThumbImg() {
		return boardThumbImg;
	}
	public void setBoardThumbImg(String boardThumbImg) {
		this.boardThumbImg = boardThumbImg;
	}
	public void setFiles(String[] files) {
		this.files = files;
		setFileCnt(files.length); // 외부에서 따로 첨부파일 개수를 넣어주지 않아도 됨
	}
	public String[] getFiles() {
		return files;
	}
	public Integer getBoardNo() {
		return boardNo;
	}
	public void setBoardNo(Integer boardNo) {
		this.boardNo = boardNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getBoardTitle() {
		return boardTitle;
	}
	public void setBoardTitle(String boardTitle) {
		this.boardTitle = boardTitle;
	}
	public String getBoardContent() {
		return boardContent;
	}
	public void setBoardContent(String boardContent) {
		this.boardContent = boardContent;
	}
	public String getBoardCategory() {
		return boardCategory;
	}
	public void setBoardCategory(String boardCategory) {
		this.boardCategory = boardCategory;
	}
	public int getHit() {
		return hit;
	}
	public void setHit(int hit) {
		this.hit = hit;
	}
	public int getReplyCnt() {
		return replyCnt;
	}
	public void setReplyCnt(int replyCnt) {
		this.replyCnt = replyCnt;
	}
	public int getFileCnt() {
		return fileCnt;
	}
	public void setFileCnt(int fileCnt) {
		this.fileCnt = fileCnt;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModdate() {
		return moddate;
	}
	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}
	@Override
	public String toString() {
		return "BoardVO [boardNo=" + boardNo + ", userId=" + userId + ", boardTitle=" + boardTitle + ", boardContent="
				+ boardContent + ", boardCategory=" + boardCategory + ", hit=" + hit + ", replyCnt=" + replyCnt
				+ ", boardImg=" + boardImg + ", boardThumbImg=" + boardThumbImg + ", regdate=" + regdate + ", moddate="
				+ moddate + ", files=" + Arrays.toString(files) + ", fileCnt=" + fileCnt + "]";
	}
	
}
