package com.jiwon.ilovepet.board.service;

import java.util.List;

public interface BoardFileService {
	
	// 파일 목록 
	List<String> getArticleFiles(Integer boardNo) throws Exception;
	
	// 파일 삭제
	public void deleteFile(String fileName, Integer boardNo) throws Exception;
}
