package com.jiwon.ilovepet.board.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.jiwon.ilovepet.board.dao.BoardFileDAO;

@Service
public class BoardFileServiceImpl implements BoardFileService {
	
	@Inject
	BoardFileDAO boardFileDAO;
	
	@Override
	public List<String> getArticleFiles(Integer boardNo) throws Exception {
		return boardFileDAO.getBoardFiles(boardNo);
	}

	@Override
	public void deleteFile(String fileName, Integer boardNo) throws Exception {
		boardFileDAO.deleteBoardFile(fileName);
		boardFileDAO.updateBoardFileCnt(boardNo);
	}

}
