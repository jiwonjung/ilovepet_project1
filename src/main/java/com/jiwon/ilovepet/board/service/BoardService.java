package com.jiwon.ilovepet.board.service;

import java.util.List;

import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;

public interface BoardService {
	
public void write(BoardVO boardVO) throws Exception;
	
	public BoardVO read(Integer boardNo) throws Exception;
	
	public void update(BoardVO boardVO) throws Exception;
	
	public void delete(Integer boardNo) throws Exception;
	
	public List<BoardVO> list() throws Exception;
	
	// 페이징 처리
	public List<BoardVO> listPagingCriteria(PagingCriteria pCriteria) throws Exception;
	public int countPagingData(PagingCriteria pCriteria) throws Exception;
	
	// 검색 처리
	public List<BoardVO> listSearchingCriteria(SearchingCriteria sCriteria) throws Exception;
	public int countSearchingData(SearchingCriteria sCriteria) throws Exception;
	
}
