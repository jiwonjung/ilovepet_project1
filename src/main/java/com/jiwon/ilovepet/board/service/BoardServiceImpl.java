package com.jiwon.ilovepet.board.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.jiwon.ilovepet.board.dao.BoardDAO;
import com.jiwon.ilovepet.board.dao.BoardFileDAO;
import com.jiwon.ilovepet.board.vo.BoardVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.SearchingCriteria;

@Service
public class BoardServiceImpl implements BoardService {
	
	@Inject
	BoardDAO dao;
	
	@Inject
	BoardFileDAO boardFileDAO;
	
	@Transactional
	@Override
	public void write(BoardVO boardVO) throws Exception {
		dao.writeBoard(boardVO);
		String[] files = boardVO.getFiles();
		
		if(files == null) {
			return;
		}
		
		// 게시글 첨부파일 입력처리
		for(String fileName : files) {
			boardFileDAO.addBoardFile(fileName);
		}
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	@Override
	public BoardVO read(Integer boardNo) throws Exception {
		dao.updateHitCnt(boardNo);
		return dao.readBoard(boardNo);
	}

	@Override
	public void update(BoardVO boardVO) throws Exception {
		dao.updateBoard(boardVO);
	}

	@Override
	public void delete(Integer boardNo) throws Exception {
		dao.deleteBoard(boardNo);
	}

	@Override
	public List<BoardVO> list() throws Exception {
		return dao.list();
	}
	
	// 페이징 처리
	@Override
	public List<BoardVO> listPagingCriteria(PagingCriteria pCriteria) throws Exception {
		return dao.listPagingCriteria(pCriteria);
	}

	@Override
	public int countPagingData(PagingCriteria pCriteria) throws Exception {
		return dao.countPagingData(pCriteria);
	}
	
	// 페이징 처리 + 검색 처리
	@Override
	public List<BoardVO> listSearchingCriteria(SearchingCriteria sCriteria) throws Exception {
		return dao.listSearchingCriteria(sCriteria);
	}

	@Override
	public int countSearchingData(SearchingCriteria sCriteria) throws Exception {
		return dao.countSearchingData(sCriteria);
	}

}
