package com.jiwon.ilovepet.boardReply.service;

import java.util.List;

import com.jiwon.ilovepet.boardReply.vo.BoardReplyVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;

public interface BoardReplyService {
	
	public List<BoardReplyVO> list(Integer boardNo) throws Exception;
	
	public void create(BoardReplyVO replyVO) throws Exception;
	
	public void update(BoardReplyVO replyVO) throws Exception;
	
	public void delete(Integer reBoardNo) throws Exception;
	
	// 댓글 페이징 처리 
	public List<BoardReplyVO> listPaging(Integer boardNo, PagingCriteria pCriteria) throws Exception;
	public int replyCountData(Integer boardNo) throws Exception;
	
}
