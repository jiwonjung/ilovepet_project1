package com.jiwon.ilovepet.boardReply.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jiwon.ilovepet.board.dao.BoardDAO;
import com.jiwon.ilovepet.boardReply.dao.BoardReplyDAO;
import com.jiwon.ilovepet.boardReply.vo.BoardReplyVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;

@Service
public class BoardReplyServiceImpl implements BoardReplyService {
	
	@Inject
	BoardReplyDAO dao;
	
	@Inject
	BoardDAO boardDao;
	
	@Override
	public List<BoardReplyVO> list(Integer boardNo) throws Exception {
		return dao.replyList(boardNo);
	}
	
	// 댓글이 추가되면 댓글 갯수를 1씩 증가
	@Transactional
	@Override
	public void create(BoardReplyVO replyVO) throws Exception {
		dao.writeReply(replyVO); // 댓글 등록
		boardDao.updateReplyCnt(replyVO.getBoardNo(), 1); // 댓글 갯수 증가
	}
	
	
	@Override
	public void update(BoardReplyVO replyVO) throws Exception {
		dao.updateReply(replyVO);
	}
	
	// 댓글이 삭제되면 댓글 갯수를 1씩 감소
	@Transactional
	@Override
	public void delete(Integer reBoardNo) throws Exception {
		int boardNo = dao.getBoardNo(reBoardNo); // 댓글의 게시물 번호 조회
		dao.deleteReply(reBoardNo); // 댓글 삭제
		boardDao.updateReplyCnt(boardNo, -1); // 댓글 갯수 감소
	}

	@Override
	public List<BoardReplyVO> listPaging(Integer boardNo, PagingCriteria pCriteria) throws Exception {
		return dao.replyListPaging(boardNo, pCriteria);
	}

	@Override
	public int replyCountData(Integer boardNo) throws Exception {
		return dao.replyCountData(boardNo);
	}

}
