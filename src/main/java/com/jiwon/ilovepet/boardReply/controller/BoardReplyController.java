package com.jiwon.ilovepet.boardReply.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.jiwon.ilovepet.boardReply.service.BoardReplyService;
import com.jiwon.ilovepet.boardReply.vo.BoardReplyVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;
import com.jiwon.ilovepet.commons.paging.PagingMaker;

@RestController
@RequestMapping("/replies")
public class BoardReplyController {
	
	private static final Logger logger = LoggerFactory.getLogger(BoardReplyController.class);
	
	@Inject
	private BoardReplyService replyService;
	
	// POST는 댓글 등록할 때
	@RequestMapping(value="", method=RequestMethod.POST)  // http://localhost:8080/replies/JSON DATA
	public ResponseEntity<String> write(@RequestBody BoardReplyVO replyVO) { // @RequestBody는 JSON 객체로 변환시킴
		
		ResponseEntity<String> responseEntity = null; // ResponseEntity는 Http Status(상태코드)로 반환하겠다는 것임
		
		try {
			replyService.create(replyVO);
			
			responseEntity = new ResponseEntity<String>("writeSuccess", HttpStatus.OK); // OK는 200번 
		} catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST); // BAD_REQUEST는 400
		}
		
		return responseEntity;
	}
	// GET은 댓글 목록 가져오기
	@RequestMapping(value="/selectAll/{boardNo}", method=RequestMethod.GET)  // http://localhost:8080/selectAll/1
	public ResponseEntity<List<BoardReplyVO>> list(@PathVariable("boardNo") Integer boardNo) {
		
		ResponseEntity<List<BoardReplyVO>> responseEntity = null;
		
		try {
			responseEntity = new ResponseEntity<>(replyService.list(boardNo),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
	
	// PUT/PATCH은 댓글 수정
	@RequestMapping(value="/{reBoardNo}", method= {RequestMethod.PUT, RequestMethod.PATCH}) // 
	public ResponseEntity<String> update(@PathVariable("reBoardNo") Integer reBoardNo, @RequestBody BoardReplyVO replyVO) {// @RequestBody는 JSON 객체로 변환시킴
		
		ResponseEntity<String> responseEntity = null;
		
		try {
			replyVO.setReBoardNo(reBoardNo);
			replyService.update(replyVO);
			
			responseEntity = new ResponseEntity<String>("updateSuccess", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		return responseEntity;
	}
	
	// DELETE은 댓글 삭제
	@RequestMapping(value = "/{reBoardNo}", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(@PathVariable("reBoardNo") Integer reBoardNo) {

		ResponseEntity<String> responseEntity = null;

		try {
			replyService.delete(reBoardNo);
			
			responseEntity = new ResponseEntity<String>("deleteSuccess", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
	
	// 댓글에 대한 페이징 처리
	@RequestMapping(value="/{boardNo}/{page}", method=RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> reListPage(
			@PathVariable("boardNo") Integer boardNo,
			@PathVariable("page") Integer page) {
		
		ResponseEntity<Map<String, Object>> resEntity = null;
		try {
			PagingCriteria pCriteria = new PagingCriteria();
			pCriteria.setPage(page);
			
			List<BoardReplyVO> reList = replyService.listPaging(boardNo, pCriteria);
			int reCount = replyService.replyCountData(boardNo);
			
			PagingMaker pagingMaker = new PagingMaker();
			pagingMaker.setCri(pCriteria);
			pagingMaker.setTotalData(reCount);
			
			Map<String, Object> reMap = new HashMap<>();
			reMap.put("reList", reList);
			reMap.put("pagingMaker", pagingMaker);
			
			resEntity = new ResponseEntity<Map<String,Object>>(reMap, HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			resEntity = new ResponseEntity<Map<String,Object>>(HttpStatus.BAD_REQUEST);
		}
		
		return resEntity;
	}
}
