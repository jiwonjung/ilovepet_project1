package com.jiwon.ilovepet.boardReply.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class BoardReplyVO {
	/*
	 create table board_reply (
	reboard_no int not null auto_increment,
	board_no int not null default '0',
	user_id varchar(50) not null, 
	reboard_content text null,
	regdate timestamp not null default now(),
	moddate timestamp not null default now(),
	primary key(reboard_no)
	 * */
	
	private Integer reBoardNo;
	private Integer boardNo;
	private String userId;
	private String reBoardContent;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm")
	private Date regdate;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm")
	private Date moddate;
	
	public Integer getReBoardNo() {
		return reBoardNo;
	}
	public void setReBoardNo(Integer reBoardNo) {
		this.reBoardNo = reBoardNo;
	}
	public Integer getBoardNo() {
		return boardNo;
	}
	public void setBoardNo(Integer boardNo) {
		this.boardNo = boardNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getReBoardContent() {
		return reBoardContent;
	}
	public void setReBoardContent(String reBoardContent) {
		this.reBoardContent = reBoardContent;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModdate() {
		return moddate;
	}
	public void setModdate(Date moddate) {
		this.moddate = moddate;
	}
	@Override
	public String toString() {
		return "BoardReplyVO [reBoardNo=" + reBoardNo + ", boardNo=" + boardNo + ", userId=" + userId
				+ ", reBoardContent=" + reBoardContent + ", regdate=" + regdate + ", moddate=" + moddate + "]";
	}
}
