package com.jiwon.ilovepet.boardReply.dao;

import java.util.List;

import com.jiwon.ilovepet.boardReply.vo.BoardReplyVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;

public interface BoardReplyDAO {
	
	public List<BoardReplyVO> replyList(Integer boardNo) throws Exception;
	
	public void writeReply(BoardReplyVO replyVO) throws Exception;
	
	public void updateReply(BoardReplyVO replyVO) throws Exception;
	
	public void deleteReply(Integer reBoardNo) throws Exception;
	
	// 댓글 페이징 처리 
	public List<BoardReplyVO> replyListPaging(Integer boardNo, PagingCriteria pCriteria) throws Exception;
	public int replyCountData(Integer boardNo) throws Exception;
	
	// 댓글의 게시물 번호 조회 (게시글의 번호를 가지고 댓글 갯수를 갱시하는 기능)
	int getBoardNo(Integer reBoardNo) throws Exception;
	
}
