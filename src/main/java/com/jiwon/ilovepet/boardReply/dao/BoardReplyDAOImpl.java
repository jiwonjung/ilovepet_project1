package com.jiwon.ilovepet.boardReply.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.jiwon.ilovepet.boardReply.vo.BoardReplyVO;
import com.jiwon.ilovepet.commons.paging.PagingCriteria;

@Repository
public class BoardReplyDAOImpl implements BoardReplyDAO {
	
	@Inject 
	private SqlSession sqlSession;
	
	@Override
	public List<BoardReplyVO> replyList(Integer boardNo) throws Exception {
		return sqlSession.selectList("replyList", boardNo);
	}

	@Override
	public void writeReply(BoardReplyVO replyVO) throws Exception {
		sqlSession.insert("writeReply", replyVO);
	}

	@Override
	public void updateReply(BoardReplyVO replyVO) throws Exception {
		sqlSession.update("updateReply", replyVO);
	}

	@Override
	public void deleteReply(Integer reBoardNo) throws Exception {
		sqlSession.delete("deleteReply", reBoardNo);
	}
	
	// 페이징 처리
	@Override
	public List<BoardReplyVO> replyListPaging(Integer boardNo, PagingCriteria pCriteria) throws Exception {
		
		Map<String, Object> map = new HashMap<>();
		map.put("boardNo", boardNo);
		map.put("pCriteria", pCriteria);
		
		return sqlSession.selectList("replyListPaging", map);
	}	

	@Override
	public int replyCountData(Integer boardNo) throws Exception {
		return sqlSession.selectOne("replyCountData", boardNo);
	}
	
	// 해당 댓글번호를 매개변수로 넣으면 게시물 번호 조회
	@Override
	public int getBoardNo(Integer reBoardNo) throws Exception {
		return sqlSession.selectOne("getBoardNo", reBoardNo);
	}

}
